<?php

namespace App\Tests\Feature\Config;

class AuthConfig {
    const ADMIN_HEADERS = [
        'Authorization' => 'Basic YWRtaW46dnU4ZWFhamlhdw==',
    ];
    
    const PUBLIC_HEADERS = [
        'Authorization' => 'Basic YWdlbnRfMTp2dThlYWFqaWF3',
    ];
}
