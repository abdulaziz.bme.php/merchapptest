<?php

namespace App\Search;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class Search
{
    const FILTER_TYPE_EQUAL = 'filterTypeEqual';
    const FILTER_TYPE_EQUAL_INSENSITIVE = 'filterTypeEqualInsensitive';
    const FILTER_TYPE_LIKE = 'filterTypeLike';
    const FILTER_TYPE_IN = 'filterTypeIn';
    const FILTER_TYPE_DATE = 'filterTypeDate';
    const FILTER_TYPE_DATETIME = 'filterTypeDatetime';
    const FILTER_TYPE_LOCALIZED = 'filterTypeLocalized';

    const COMBINED_TYPE_AND = 'where';
    const COMBINED_TYPE_OR = 'orWhere';

    const SORT_TYPE_SIMPLE = 'sortTypeSimple';
    const SORT_TYPE_LOCALIZED = 'sortTypeLocalized';

    public Builder $queryBuilder;
    protected array $relations = [];
    protected array $filterable = [];
    protected array $combinedFilterable = [];
    protected array $sortable = [];

    protected array $defaultSort = ['-id'];

    public function setQueryBuilder(Builder $queryBuilder): self
    {
        $this->queryBuilder = $queryBuilder;
        return $this;
    }

    public function join(array $params): self
    {
        $params = Arr::flatten($params);
        $params = array_intersect($params, $this->relations);

        $this->queryBuilder->with($params);

        return $this;
    }

    public function filter(array $params, string $combinedType, ?Builder $subQuery = null): self
    {
        $subQuery = $subQuery ?? $this->queryBuilder;

        foreach ($params as $param => $value) {
            $type = $this->filterable[$param] ?? null;

            if (!$type) continue;

            $param = explode('.', $param);

            $value = is_array($value) ? Arr::flatten($value) : $value;

            if (count($param) == 1) {
                $this->applyFilter(
                    query: $subQuery,
                    param: $param[0],
                    type: $type,
                    value: $value,
                    combinedType: $combinedType,
                );
            } else {
                $lastParam = array_pop($param);

                $subQuery->{$combinedType . 'Has'}(
                    implode('.', $param),
                    function ($query) use ($lastParam, $type, $value, $combinedType) {
                        $query->select('id');
                        $this->applyFilter(
                            query: $query,
                            param: $lastParam,
                            type: $type,
                            value: $value,
                            combinedType: self::COMBINED_TYPE_AND,
                        );
                    }
                );
            }
        }

        return $this;
    }

    public function combinedFilter(array $params): self
    {
        foreach ($params as $param => $value) {
            $options = $this->combinedFilterable[$param] ?? null;

            if (!$options) continue;

            $fields = array_map(fn ($v) => $value, $options['fields']);

            $this->filterable += $options['fields'];

            $this->queryBuilder->where(function ($subQuery) use ($fields, $options) {
                $this->filter($fields, $options['type'], $subQuery);
            });
        }

        return $this;
    }

    private function applyFilter(Builder &$query, string $param, string $type, mixed $value, string $combinedType)
    {
        if ($type != self::FILTER_TYPE_IN) {
            $value = is_array($value) ? implode('', $value) : $value;
        }

        switch ($type) {
            case self::FILTER_TYPE_EQUAL:
                $query->{$combinedType}($param, '=', $value);
                break;
            case self::FILTER_TYPE_EQUAL_INSENSITIVE:
                $query->{$combinedType}(DB::raw("LOWER($param)"), '=', mb_strtolower($value));
                break;
            case self::FILTER_TYPE_LIKE:
                $query->{$combinedType}($param, 'LIKE', "%$value%");
                break;
            case self::FILTER_TYPE_IN:
                $query->{$combinedType . 'In'}($param, (array)$value);
                break;
            case self::FILTER_TYPE_DATE:
                $query->{$combinedType}($param, '=', date('Y-m-d', strtotime($value)));
                break;
            case self::FILTER_TYPE_DATETIME:
                $query->{$combinedType}($param, '=', date('Y-m-d H:i:s', strtotime($value)));
                break;
            case self::FILTER_TYPE_LOCALIZED:
                $locale = app()->getLocale();
                $query->{$combinedType}("$param->$locale", 'LIKE', "%$value%");
                break;
        }
    }

    public function sort(array $params): self
    {
        $params = $params ?: $this->defaultSort;
        $params = Arr::flatten($params);

        foreach ($params as $param) {
            if (str_starts_with($param, '-')) {
                $sort_direction = 'DESC';
                $param = substr($param, 1);
            } else {
                $sort_direction = 'ASC';
            }

            $type = $this->sortable[$param] ?? null;

            if (!$type) continue;

            if (str_contains($param, '.')) {
                $this->queryBuilder->joinRelation(pathinfo($param, PATHINFO_FILENAME));
            } else {
                $param = $this->queryBuilder->from . ".$param";
            }

            switch ($type) {
                case self::SORT_TYPE_SIMPLE:
                    $this->queryBuilder->orderBy($param, $sort_direction);
                    break;
                case self::SORT_TYPE_LOCALIZED:
                    $locale = app()->getLocale();
                    $this->queryBuilder->orderBy("$param->$locale", $sort_direction);
                    break;
            }
        }

        return $this;
    }

    public function show(array $params): self
    {
        $params = Arr::flatten($params);

        $model = $this->queryBuilder->getModel();
        $hasSoftDelete = in_array(SoftDeletes::class, class_uses_recursive($model));

        foreach ($params as $param) {
            switch ($param) {
                case 'with-deleted':
                    if ($hasSoftDelete) $this->queryBuilder->withTrashed();
                    break;
                case 'only-deleted':
                    if ($hasSoftDelete) $this->queryBuilder->onlyTrashed();
                    break;
            }
        }

        return $this;
    }
}
