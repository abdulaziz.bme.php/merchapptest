<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Modules\Task\Models\Task;

class TaskDailyChangeAgentStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:daily_task_change_agent_status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change agent_status in Task (daily)';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Task::query()
            ->where('agent_status', '!=', 'completed')
            ->where('deadline', '<', date('Y-m-d'))
            ->update(['agent_status' => 'overdue']);

        $this->info('Success');
    }
}
