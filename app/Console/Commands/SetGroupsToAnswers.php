<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Modules\Question\Models\QuestionAnswer;

class SetGroupsToAnswers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'answers:set_groups';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        QuestionAnswer::with(['question' => fn($q) => $q->withTrashed()])->chunk(100, function ($answers) {
            /** @var QuestionAnswer $answer */
            foreach ($answers as $answer) {
                $answer->update([
                    'question_group_id' => $answer->question->group_id ?? 0,
                ]);
            }
        });
        return Command::SUCCESS;
    }
}
