<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Modules\Product\Models\Product;
use PhpOffice\PhpSpreadsheet\IOFactory;

class SyncProductsFromExcel extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync_products:from_excel {path}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $file_path = $this->argument('path');
        $spreadsheet = IOFactory::load(base_path($file_path));
        $sheet = $spreadsheet->getSheet(0);

        $rows = $sheet->toArray();
        array_shift($rows);

        foreach ($rows as $row) {
            if (!$row) continue;

            $row = array_map('trim', $row);
            $product_id = intval($row[0]);

            $product = Product::query()->where('id', $product_id)->first();

            if (!$product)
                continue;

            $date_eol = $row ? Carbon::createFromFormat('m/d/Y', $row[9])->format('Y-m-d') : null;
            $product->update([
                'date_eol' => $date_eol,
                'extra_id' => $row[10] ?? "",
            ]);
            $this->info($row[0] . ' - ' . $row[7] . ' UPDATED');
        }

        return 0;
    }
}
