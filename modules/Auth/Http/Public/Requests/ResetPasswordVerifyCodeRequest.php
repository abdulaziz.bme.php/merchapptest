<?php

namespace Modules\Auth\Http\Public\Requests;

use App\Http\Requests\FormRequest;
use Illuminate\Validation\Rule;

class ResetPasswordVerifyCodeRequest extends FormRequest
{
    public function rules()
    {
        return [
            'email' => [
                'required',
                'email',
                'max:100',
                Rule::exists('user')->where('deleted_at', null),
            ],
            'reset_password_code' => [
                'required',
                'string',
                'size:4',
                Rule::exists('user')->where('deleted_at', null)->where('email', $this->email),
            ],
        ];
    }
}
