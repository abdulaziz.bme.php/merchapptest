<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('alter table question_answer_groups drop column question_group_id;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('question_answer_groups', function (Blueprint $table) {
            $table->foreignId('question_group_id')->nullable()->constrained('question_groups')->cascadeOnDelete();
        });
    }
};
