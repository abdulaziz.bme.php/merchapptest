<?php

namespace Modules\Question\Enums;

enum AnswerType: string
{
    case TEXT    = "T";
    case VARIANT = "V";
}
