<?php

namespace Modules\Question\Enums;

enum QuestionType: string
{
    case TEXT     = "T";
    case VARIANT  = "V";
    case MULTIPLE = "M";
}
