<?php
/**
 *
 * MerchApp-Web
 * User: Bakhodir @iambakhodir
 * Date: 25/03/23
 * Time: 2:37 PM
 */

use Illuminate\Support\Facades\Route;
use Modules\Question\Http\Public\Controllers\QuestionAnswersController;
use Modules\Question\Http\Public\Controllers\QuestionGroupsController;

Route::prefix('questions')
    ->where([
        'group' => '[0-9]+',
    ])
    ->group(function () {
        Route::apiResource('groups', QuestionGroupsController::class)->only(['index', 'show']);
        Route::post('groups/answer', [QuestionGroupsController::class, 'answer'])->whereNumber('group');

        Route::apiResource('answers', QuestionAnswersController::class)->parameters([
            'answers' => 'answers',
        ])->only(['show', 'update', 'store']);
    });
