<?php
/**
 *
 * MerchApp-Web
 * User: Bakhodir @iambakhodir
 * Date: 25/03/23
 * Time: 3:03 PM
 */

namespace Modules\Question\Http\Public\Controllers;

use App\Http\Controllers\ApiResourceController;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Modules\Question\Enums\AnswerType;
use Modules\Question\Enums\QuestionType;
use Modules\Question\Http\Public\Requests\QuestionAnswerRequest;
use Modules\Question\Models\Question;
use Modules\Question\Models\QuestionAnswer;
use Modules\Question\Models\QuestionAnswerGroup;
use Modules\Question\Models\QuestionGroup;
use Modules\Question\Resources\QuestionAnswerGroupResource;
use Modules\Question\Resources\QuestionGroupResource;
use Modules\Question\Search\QuestionGroupSearch;

class QuestionGroupsController extends ApiResourceController
{
    public function __construct()
    {
        return parent::__construct(
            model: new QuestionGroup(),
            search: new QuestionGroupSearch(only_active: true),
            resourceClass: QuestionGroupResource::class
        );
    }

    public function show($id)
    {
        $model = $this->search->queryBuilder->findOrFail(intval($id));
        $data = $this->resourceClass::make($model);

        return response()->json($data);
    }

    public function answer(QuestionAnswerRequest $request)
    {
        DB::beginTransaction();
        try {
            $shop_id = $request->get('shop_id', 0);

            //@TODO reusable niyam filter qilish
            //@TODO question_group_id ni answerni ichiga tiqish


            $answer_group = QuestionAnswerGroup::query()->create([
                'report_id' => $request->get('report_id', 0),
                'shop_id'   => $shop_id,
            ]);

            foreach ($request->get('answers', []) as $answer) {
                /** @var Question $q */
                $q = Question::query()->where('id', $answer['question_id'])->first();

                if ($q) {
                    /** @var QuestionGroup $question_group */
                    $question_group = QuestionGroup::query()->where('id', $q->group_id)->firstOrFail();

                    if (!$question_group->reusable) {
                        $is_used = QuestionAnswerGroup::query()
                            ->leftJoin('question_answers', 'question_answers.answer_group_id', '=', 'question_answer_groups.id')
                            ->where('question_answers.question_id', $q->id)
                            ->where('question_answer_groups.shop_id', $shop_id)
                            ->exists();
                        abort_if($is_used, 500, 'На эти вопросы уже были даны ответы');
                    }

                    if ($question_group->for_selected_shops) {
                        $is_used = QuestionGroup::query()
                            ->whereHas('shops', function ($q) use ($shop_id) {
                                $q->where('id', $shop_id);
                            })
                            ->where('id', $q->group_id)
                            ->exists();

                        abort_if(!$is_used, 500, "На этот вопрос можно ответить только определенные магазины");
                    }

                    abort_if(($q->question_type == QuestionType::VARIANT && $answer['answer_type'] !== AnswerType::VARIANT->value), 422, 'Тип ответа неверен.');
                    abort_if(($q->question_type == QuestionType::TEXT && $answer['answer_type'] !== AnswerType::TEXT->value), 422, 'Тип ответа неверен.');
                    abort_if(($answer['answer_type'] === AnswerType::VARIANT->value && !is_numeric($answer['answer'])), 422, 'Тип ответа неверен.');

                    QuestionAnswer::query()->create([
                        'question_group_id' => $q->group_id,
                        'question_id'       => $answer['question_id'],
                        'answer_group_id'   => $answer_group->id,
                        'answer'            => $answer['answer'],
                        'answer_type'       => $answer['answer_type'],
                    ]);
                }
            }

            DB::commit();

            $data = QuestionAnswerGroup::query()->where('id', $answer_group->id)->with('answers.question', 'group', 'shop')->first();
            return response()->json(
                QuestionAnswerGroupResource::make($data),
                Response::HTTP_CREATED
            );
        } catch (\Throwable $exception) {
            DB::rollBack();
            return response()->json(
                [
                    'message' => $exception->getMessage(),
                ],
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }
}
