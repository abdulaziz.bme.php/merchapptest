<?php
/**
 *
 * MerchApp-Web
 * User: Bakhodir @iambakhodir
 * Date: 28/03/23
 * Time: 5:36 PM
 */

namespace Modules\Question\Http\Admin\Controllers;

use App\Http\Controllers\ApiResourceController;
use Modules\Question\Models\QuestionAnswerGroup;
use Modules\Question\Resources\QuestionAnswerGroupResource;
use Modules\Question\Search\QuestionAnswerGroupSearch;

class QuestionAnswerGroupsController  extends ApiResourceController
{
    public function __construct()
    {
        return parent::__construct(
            model        : new QuestionAnswerGroup(),
            search       : new QuestionAnswerGroupSearch(),
            resourceClass: QuestionAnswerGroupResource::class
        );
    }
}
