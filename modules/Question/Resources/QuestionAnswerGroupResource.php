<?php
/**
 *
 * MerchApp-Web
 * User: Bakhodir @iambakhodir
 * Date: 25/03/23
 * Time: 3:18 PM
 */

namespace Modules\Question\Resources;

use App\Resources\JsonResource;
use Modules\Shop\Resources\ShopResource;

class QuestionAnswerGroupResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id'         => $this->id,
            'shop'       => ShopResource::make($this->shop),
            'answers'    => QuestionAnswerResource::collection($this->whenLoaded('answers')),
            'created_at' => $this->created_at->format("Y-m-d H:i"),
            'updated_at' => $this->updated_at->format("Y-m-d H:i"),
        ];
    }
}
