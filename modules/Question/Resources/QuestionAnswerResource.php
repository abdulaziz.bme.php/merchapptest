<?php
/**
 *
 * MerchApp-Web
 * User: Bakhodir @iambakhodir
 * Date: 25/03/23
 * Time: 3:19 PM
 */

namespace Modules\Question\Resources;

use App\Resources\JsonResource;

class QuestionAnswerResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id'          => $this->id,
            'answer'      => $this->answer,
            'answer_type'      => $this->answer_type,
            'question_id' => $this->question_id,
            'question'    => $this->question->question,
            'created_at'  => $this->created_at->format("Y-m-d H:i"),
            'updated_at'  => $this->updated_at->format("Y-m-d H:i"),
        ];
    }
}
