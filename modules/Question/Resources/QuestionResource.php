<?php
/**
 *
 * MerchApp-Web
 * User: Bakhodir @iambakhodir
 * Date: 25/03/23
 * Time: 3:19 PM
 */

namespace Modules\Question\Resources;

use App\Resources\JsonResource;

class QuestionResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id'          => $this->id,
            'question'    => $this->question,
            'question_en' => $this->question_en,
            'question_type' => $this->question_type,
            'variants'    => QuestionVariantResource::collection($this->variants),
            'created_at'  => $this->created_at->format("Y-m-d H:i"),
            'updated_at'  => $this->updated_at->format("Y-m-d H:i"),
        ];
    }
}
