<?php
/**
 *
 * MerchApp-Web
 * User: Bakhodir @iambakhodir
 * Date: 25/03/23
 * Time: 3:04 PM
 */

namespace Modules\Question\Models;

use App\Models\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Modules\Question\Enums\VariantType;

/**
 * @property int                $id
 * @property int                $question_id
 * @property string             $title
 * @property string             $title_en
 * @property string|Carbon      $created_at
 * @property string|Carbon      $updated_at
 * @property string|Carbon|null $deleted_at
 * @property string|VariantType $variant_type
 *
 */
class QuestionVariant extends Model
{
    use SoftDeletes;

    protected $table = 'question_variants';

    protected $fillable = [
        'question_id',
        'title',
        'title_en',
        'variant_type',
    ];

    protected $casts = [
        'variant_type' => VariantType::class,
    ];
}
