<?php
/**
 *
 * MerchApp-Web
 * User: Bakhodir @iambakhodir
 * Date: 25/03/23
 * Time: 3:04 PM
 */

namespace Modules\Question\Models;

use App\Models\Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Modules\Product\Models\ProductBrand;
use Modules\Region\Models\RegionCity;
use Modules\Shop\Models\Shop;

/**
 * @property int                   $id
 * @property string                $title
 * @property bool                  $is_active
 * @property string|Carbon         $created_at
 * @property string|Carbon         $updated_at
 * @property int                   $brand_id
 * @property string                $title_en
 * @property bool                  $for_selected_shops
 * @property bool                  $reusable
 * @property bool                  $shop_type
 *
 * @property Question[]|Collection $questions
 * @property ProductBrand          $brand
 */
class QuestionGroup extends Model
{
    use SoftDeletes;

    protected $table = 'question_groups';

    protected $fillable = [
        'title',
        'title_en',
        'is_active',
        'brand_id',
        'for_selected_shops',
        'reusable',
        'shop_type',
    ];

    public function shops()
    {
        return $this->belongsToMany(
            Shop::class,
            'question_group_shops',
            'group_id',
            'shop_id'
        )->withTrashed();
    }

    public function questions()
    {
        return $this->hasMany(Question::class, 'group_id', 'id');
    }

    public function brand()
    {
        return $this->hasOne(ProductBrand::class, 'id', 'brand_id');
    }

    protected static function boot()
    {
        parent::boot();

        self::deleted(function (self $model) {
            $model->questions()->delete();
        });
        self::restored(function (self $model) {
            Question::query()->onlyTrashed()->where('group_id', $model->id)->restore();
        });
    }
}
