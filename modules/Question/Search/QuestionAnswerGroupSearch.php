<?php

namespace Modules\Question\Search;

use App\Search\Search;

class QuestionAnswerGroupSearch extends Search
{
    protected array $relations = [
        'answers',
        'shop',
        'report',
    ];

    protected array $filterable = [
        'id'       => Search::FILTER_TYPE_EQUAL,
        'shop.id'  => Search::FILTER_TYPE_EQUAL,
    ];

    protected array $sortable = [
        'id'         => Search::SORT_TYPE_SIMPLE,
        'shop.id'    => Search::SORT_TYPE_SIMPLE,
        'created_at' => Search::SORT_TYPE_SIMPLE,
    ];
}
