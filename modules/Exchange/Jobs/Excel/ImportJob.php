<?php

namespace Modules\Exchange\Jobs\Excel;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use Illuminate\Support\Facades\DB;
use App\Services\LocalizationService;
use Illuminate\Support\Arr;

use Modules\Product\Models\Product;
use Modules\Product\Models\ProductBrand;
use Modules\Product\Models\ProductSpecificationOption;
use Modules\Product\Models\ProductVariation;
use Modules\Region\Models\Region;
use Modules\Region\Models\RegionCity;
use Modules\Shop\Models\Shop;
use Modules\Shop\Models\ShopContact;
use Modules\User\Models\User;

class ImportJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    const TYPE_PRODUCT = 'processProduct';
    const TYPE_SHOP = 'processShop';

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(
        private string $type,
        private object $record,
        private int $creatorId,
    ) {
        if (!in_array($type, [
            self::TYPE_PRODUCT,
            self::TYPE_SHOP,
        ])) throw new \Exception('Wrong type');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // DB::beginTransaction();

        // try {
        //     $this->{$this->type}();
        //     DB::commit();
        // } catch (\Throwable $e) {
        //     print_r($e);
        //     DB::rollBack();
        //     throw new \Exception($e->getMessage());
        // }

        $this->{$this->type}();
    }

    private function processProduct()
    {
        $languages = LocalizationService::getInstance()->activeLanguages->pluck('code')->toArray();

        // Processing Product

        $brand = ProductBrand::query()->where('name', $this->record->brandName)->withTrashed()->firstOrFail();
        $this->record->brand_id = $brand->id;

        $productPath = public_path("storage/exchange/product/{$this->record->brandName}/{$this->record->model_number}");

        $product = Product::query()
            ->where('brand_id', $this->record->brand_id)
            ->where('model_number', $this->record->model_number)
            ->withTrashed()
            ->firstOrNew();

        $images = glob("$productPath/*.{jpg,jpeg,png}", GLOB_BRACE);
        $images = array_map(function ($value) {
            $value = str_replace(public_path(), '', $value);
            return str_replace('\\', '/', $value);
        }, $images);

        $product->images_list = $images;
        $product->creator_id ??= $this->creatorId;
        $product->is_active ??= true;
        $product->fill((array)$this->record)->touch();

        // Processing ProductVariation

        $variationRecords = explode("\n", $this->record->variations);

        foreach ($variationRecords as $variationRecord) {
            $variationRecord = explode('|', $variationRecord);
            $variationRecord = array_map('trim', $variationRecord);
            $variationSku = Arr::pull($variationRecord, 0);

            $options = ProductSpecificationOption::query();

            foreach ($languages as $language) {
                $options->orWhereIn("name->$language", $variationRecord);
            }

            $options = $options->get()->pluck('id');

            $variation = ProductVariation::query()
                ->where('product_id', $product->id)
                ->where('sku', $variationSku)
                ->withTrashed()
                ->firstOrNew();

            $variation->product_id = $product->id;
            $variation->sku = $variationSku;

            $images = glob("$productPath/$variationSku/*.{jpg,jpeg,png}", GLOB_BRACE);
            $images = array_map(function ($value) {
                $value = str_replace(public_path(), '', $value);
                return str_replace('\\', '/', $value);
            }, $images);

            $variation->images_list = $images;

            $variation->fillableRelations = [
                $variation::RELATION_TYPE_MANY_MANY => [
                    'options' => $options,
                ],
            ];

            $variation->touch();
        }
    }

    private function processShop()
    {
        $languages = LocalizationService::getInstance()->activeLanguages->pluck('code')->toArray();

        // Preparing Shop

        $agent = User::query()->where('full_name', $this->record->agentFullName)->withTrashed()->firstOrFail();

        $region = Region::query();

        $region->where(function ($query) use ($languages) {
            foreach ($languages as $language) {
                $query->orWhere("name->$language", $this->record->regionName);
            }
        });

        $region = $region->withTrashed()->firstOrFail();

        $city = RegionCity::query()->where('region_id', $region->id);

        $city->where(function ($query) use ($languages) {
            foreach ($languages as $language) {
                $query->orWhere("name->$language", $this->record->cityName);
            }
        });

        $city = $city->withTrashed()->firstOrFail();

        $this->record->agent_id = $agent->id;
        $this->record->city_id = $city->id;

        $shop = Shop::query()->where('number', $this->record->number)->withTrashed()->firstOrNew();

        // Processing ShopContact

        $contactsFullNames = explode("\n", $this->record->contactsFullNames);
        $contactsFullNames = array_map('trim', $contactsFullNames);
        $contactsPhones = explode("\n", $this->record->contactsPhones);
        $contactsPhones = array_map('trim', $contactsPhones);

        if (count($contactsFullNames) == count($contactsPhones)) {
            foreach ($contactsFullNames as $key => $contactsFullName) {
                if (!isset($contactsPhones[$key])) continue;

                $contactRecord = ShopContact::query()->where('full_name', $contactsFullName)->withTrashed()->firstOrNew();
                $contactRecord->full_name = $contactsFullName;
                $contactRecord->phone = $contactsPhones[$key];
                $contactRecord->creator_id ??= $this->creatorId;
                $contactRecord->is_active ??= true;
                $contactRecord->touch();

                $shop->fillableRelations[$shop::RELATION_TYPE_MANY_MANY]['contacts'][$key] = $contactRecord->id;
            }
        }

        // Processing Shop

        $shop->location ??= [];
        $shop->is_active ??= true;
        $shop->fill((array)$this->record)->touch();
    }
}
