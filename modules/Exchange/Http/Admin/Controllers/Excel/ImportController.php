<?php

namespace Modules\Exchange\Http\Admin\Controllers\Excel;

use App\Http\Controllers\Controller;
use Modules\Exchange\Http\Admin\Requests\Excel\Import\ProductRequest;
use Modules\Exchange\Http\Admin\Requests\Excel\Import\ShopRequest;

class ImportController extends Controller
{
    public function product(ProductRequest $request)
    {
        $response = ['message' => __('information.exchange.import_started')];
        return response()->json($response, 200);
    }

    public function shop(ShopRequest $request)
    {
        $response = ['message' => __('information.exchange.import_started')];
        return response()->json($response, 200);
    }
}
