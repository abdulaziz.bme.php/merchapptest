<?php

namespace Modules\Exchange\Http\Admin\Controllers\Excel\Export\Shop;

use Modules\Exchange\Http\Admin\Controllers\Excel\ExportController;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Illuminate\Support\Facades\DB;

use Modules\Exchange\Http\Admin\Requests\Excel\Export\Shop\ListRequest;
use Modules\Exchange\Services\Excel\Export\Shop\ListService;

use Modules\Shop\Models\Shop;

class ExportShopListController extends ExportController
{
    public function process(ListRequest $request)
    {
        $shops = Shop::query()
            ->with([
                'agent',
                'company',
                'city',
                'city.region',
                'last_report',
            ])
            ->whereNull('deleted_at')
            ->whereHas('last_report', function ($query) use ($request) {
                $query->where('date_period_from', '>=', date('Y-m-d', strtotime($request->date_from)))
                    ->where('date_period_from', '<=', date('Y-m-d', strtotime($request->date_to)));
            })
            ->orderBy('shop.id', 'DESC')
            ->get();

        $spreadsheet = new Spreadsheet();

        $service = new ListService(
            spreadsheet: $spreadsheet,
            shops: $shops,
        );

        $service->process();

        $spreadsheet = $service->getSpreadsheet();

        foreach ($spreadsheet->getAllSheets() as $sheet) {
            $sheet->setSelectedCell('A1');
        }

        return $this->responseExport($spreadsheet);
    }
}
