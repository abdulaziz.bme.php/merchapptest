<?php

namespace Modules\Exchange\Http\Admin\Controllers\Excel\Export;

use Modules\Exchange\Http\Admin\Controllers\Excel\ExportController;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Illuminate\Support\Facades\DB;

use Modules\Exchange\Http\Admin\Requests\Excel\Export\ProductVariationRequest;
use Modules\Exchange\Services\Excel\Export\ProductVariationService;
use Modules\Report\Models\ReportProduct;

class ExportProductVariationController extends ExportController
{
    public function process(ProductVariationRequest $request)
    {
        $locale = app()->getLocale();

        $query = ReportProduct::query()
            ->joinRelation('report.shop.city.region')
            ->joinRelation('product.brand')
            ->joinRelation('supplier')
            ->leftJoinRelation('variation')
            ->whereHas('report', function ($q) use ($request) {
                $q->where('date_period_from', '>=', date('Y-m-d', strtotime($request->date_from)))
                    ->where('date_period_from', '<=', date('Y-m-d', strtotime($request->date_to)));
            })
            ->select([
                DB::raw('product_brand.name as brand_name'),
                DB::raw('product.id as product_id'),
                DB::raw('product.model_number as product_model_number'),
                DB::raw('product_variation.id as variation_id'),
                DB::raw('product_variation.name as variation_name'),
                DB::raw("region.name->>'$this->locale' as region_name"),
                DB::raw('shop_supplier.short_name as supplier_name'),
                DB::raw('SUM(report_product.quantity) as quantity'),
            ])
            ->groupBy([
                'region.id',
                'product_brand.id',
                'product.id',
                'shop_supplier.id',
                'product_variation.id',
            ])
            ->orderBy('product_variation.id')
            ->orderBy('shop_supplier.short_name')
            ->getQuery();

        if ($request->brand_id) {
            $query->where('product_brand.id', $request->brand_id);
        }

        if ($request->product_id) {
            $query->where('report_product.product_id', $request->product_id);
        }

        if ($request->variation_id) {
            $query->where('report_product.variation_id', $request->variation_id);
        }

        $variations = $query->get()->map(function ($value, $key) use ($locale) {
            $value->product_variation_id = $value->product_id;
            $value->name = "$value->brand_name $value->product_model_number";

            if ($value->variation_id) {
                $variationName = json_decode($value->variation_name, true);
                $variationName = data_get($variationName, "*.$locale");
                $variationName = implode(' ', $variationName);

                $value->product_variation_id .= "_$value->variation_id";
                $value->name .= " $variationName";
            }

            return (array)$value;
        });

        $spreadsheet = new Spreadsheet();

        $service = new ProductVariationService(
            spreadsheet: $spreadsheet,
            records: $variations->toArray(),
        );

        $service->process();

        $spreadsheet = $service->getSpreadsheet();

        foreach ($spreadsheet->getAllSheets() as $sheet) {
            $sheet->setSelectedCell('A1');
        }

        return $this->responseExport($spreadsheet);
    }
}
