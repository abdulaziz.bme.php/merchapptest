<?php

namespace Modules\Exchange\Http\Admin\Controllers\Excel\Export;

use Modules\Exchange\Http\Admin\Controllers\Excel\ExportController;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Illuminate\Support\Facades\DB;

use Modules\Exchange\Http\Admin\Requests\Excel\Export\ProductBrandRequest;
use Modules\Exchange\Services\Excel\Export\ProductBrandService;
use Modules\Report\Models\ReportProduct;

class ExportProductBrandController extends ExportController
{
    public function process(ProductBrandRequest $request)
    {
        $query = ReportProduct::query()
            ->joinRelation('product.brand')
            ->joinRelation('report.shop.city.region')
            ->joinRelation('supplier')
            ->whereHas('report', function ($q) use ($request) {
                $q->where('date_period_from', '>=', date('Y-m-d', strtotime($request->date_from)))
                    ->where('date_period_from', '<=', date('Y-m-d', strtotime($request->date_to)));
            })
            ->select([
                DB::raw('product_brand.name as name'),
                DB::raw("region.name->>'$this->locale' as region_name"),
                DB::raw('shop_supplier.short_name as supplier_name'),
                DB::raw('SUM(report_product.quantity) as quantity'),
            ])
            ->groupBy([
                'product_brand.id',
                'region.id',
                'shop_supplier.id',
            ])
            ->orderBy('product_brand.name')
            ->orderBy('region_name');

        if ($request->region_id) {
            $query->whereHas('report.shop.city', function ($q) use ($request) {
                $q->where('region_id', $request->region_id);
            });
        }

        $spreadsheet = new Spreadsheet();

        $service = new ProductBrandService(
            spreadsheet: $spreadsheet,
            records: $query->get()->toArray(),
        );

        $service->process();

        $spreadsheet = $service->getSpreadsheet();

        foreach ($spreadsheet->getAllSheets() as $sheet) {
            $sheet->setSelectedCell('A1');
        }

        return $this->responseExport($spreadsheet);
    }
}
