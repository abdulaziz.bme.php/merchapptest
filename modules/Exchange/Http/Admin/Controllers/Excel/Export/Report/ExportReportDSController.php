<?php

namespace Modules\Exchange\Http\Admin\Controllers\Excel\Export\Report;

use Modules\Exchange\Http\Admin\Controllers\Excel\ExportController;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Illuminate\Support\Facades\DB;

use Modules\Exchange\Http\Admin\Requests\Excel\Export\Report\ReportDSRequest;
use Modules\Exchange\Services\Excel\Export\Report\ReportDSService;

use Modules\Product\Models\ProductBrand;
use Modules\Region\Models\Region;
use Modules\Report\Models\Report;
use Modules\Report\Models\ReportProduct;
use Modules\Shop\Models\ShopSupplier;

class ExportReportDSController extends ExportController
{
    public function process(ReportDSRequest $request)
    {
        $regions = Region::query()->get(['id', DB::raw("name->'$this->locale' as name")]);
        $brands = ProductBrand::query()->orderBy('name')->get(['id', 'name']);

        $products = [];
        $shops = [];

        foreach ($regions as $region) {
            $productsQuery = ReportProduct::query()
                ->joinRelation('product')
                ->whereHas('report', function ($query) use ($request) {
                    $query->where('date_period_from', '>=', date('Y-m-d', strtotime($request->date_from)))
                        ->where('date_period_from', '<=', date('Y-m-d', strtotime($request->date_to)));
                })
                ->whereHas('report.shop.city', function ($query) use ($region) {
                    $query->where('region_id', $region->id);
                })
                ->select([
                    DB::raw('product.brand_id as brand_id'),
                    DB::raw('SUM(report_product.quantity) as quantity'),
                ])
                ->groupBy(['product.brand_id']);

            $products[$region->id] = [
                'all' => (clone ($productsQuery))
                    ->get()
                    ->keyBy('brand_id')
                    ->toArray(),
                'bm' => (clone ($productsQuery))
                    ->where('supplier_id', ShopSupplier::BM_ID)
                    ->get()
                    ->keyBy('brand_id')
                    ->toArray(),
            ];

            $shopsQuery = Report::query()
                ->where('date_period_from', '>=', date('Y-m-d', strtotime($request->date_from)))
                ->where('date_period_from', '<=', date('Y-m-d', strtotime($request->date_to)))
                ->whereHas('shop.city', function ($query) use ($region) {
                    $query->where('region_id', $region->id);
                })
                ->select([
                    DB::raw('COUNT(shop_id) as quantity'),
                ]);

            $shops[$region->id] = [
                'all' => (clone ($shopsQuery))
                    ->first()
                    ->toArray(),
                'bm' => (clone ($shopsQuery))
                    ->whereHas('products', function ($query) {
                        $query->where('supplier_id', ShopSupplier::BM_ID);
                    })
                    ->first()
                    ->toArray(),
            ];
        }

        $spreadsheet = new Spreadsheet();

        $service = new ReportDSService(
            spreadsheet: $spreadsheet,
            request: $request,
            regions: $regions,
            brands: $brands,
            products: $products,
            shops: $shops,
        );

        $service->process();

        return $this->responseExport($service->getSpreadsheet());
    }
}
