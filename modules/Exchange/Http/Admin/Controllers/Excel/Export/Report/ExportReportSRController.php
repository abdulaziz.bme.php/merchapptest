<?php

namespace Modules\Exchange\Http\Admin\Controllers\Excel\Export\Report;

use Modules\Exchange\Http\Admin\Controllers\Excel\ExportController;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Illuminate\Support\Facades\DB;

use Modules\Exchange\Http\Admin\Requests\Excel\Export\Report\ReportSRRequest;
use Modules\Exchange\Services\Excel\Export\Report\ReportSRService;
use Modules\Report\Models\Report;

class ExportReportSRController extends ExportController
{
    public function process(ReportSRRequest $request)
    {
        $report = Report::query()
            ->joinRelation('shop.city.region')
            ->joinRelation('creator as creator')
            ->with(['brands.brand'])
            ->where('report.type', 'sr')
            ->where('report.shop_id', $request->shop_id)
            ->where('report.updated_at', '>=', date('Y-m-d 00:00:00', strtotime($request->date)))
            ->where('report.updated_at', '<=', date('Y-m-d 23:59:59', strtotime($request->date)))
            ->select([
                DB::raw('report.id as id'),
                DB::raw('shop.number as shop_number'),
                DB::raw('shop.focus_code as shop_focus_code'),
                DB::raw('creator.username as creator_username'),
                DB::raw("region.name->>'$this->locale' as region_name"),
                DB::raw("region_city.name->>'$this->locale' as city_name"),
                DB::raw("TO_CHAR(report.updated_at, 'DD.MM.YYYY') as date"),
            ])
            ->first();

        if (!$report) abort(403, __('errors.report.not_found'));
        if ($report->brands->isEmpty()) abort(403, __('errors.report.no_brands'));

        $spreadsheet = new Spreadsheet();

        $service = new ReportSRService(
            spreadsheet: $spreadsheet,
            report: $report->toArray(),
        );

        $service->process();

        $spreadsheet = $service->getSpreadsheet();

        foreach ($spreadsheet->getAllSheets() as $sheet) {
            $sheet->setSelectedCell('A1');
        }

        return $this->responseExport($spreadsheet);
    }
}
