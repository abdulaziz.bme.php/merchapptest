<?php

namespace Modules\Exchange\Http\Admin\Requests\Excel\Export;

use App\Http\Requests\FormRequest;
use Illuminate\Validation\Rule;

class ProductBrandRequest extends FormRequest
{
    public function rules()
    {
        return [
            'region_id' => [
                Rule::exists('region', 'id'),
            ],
            'date_from' => 'required|date|date_format:d.m.Y',
            'date_to' => 'required|date|date_format:d.m.Y',
        ];
    }

    public function withValidator($validator)
    {
        if (!$validator->fails()) {
            $validator->after(function ($validator) {
                $datesDiff = date_diff(date_create($this->date_from), date_create($this->date_to));

                if ($datesDiff->invert) {
                    $validator->errors()->add('date_to', __('errors._common.gte.numeric', [
                        'attribute' => __('fields.date_to'),
                        'value' => $this->date_from,
                    ]));
                }

                if ($datesDiff->y > 0) {
                    $validator->errors()->add('date_to', __('errors.exchange.report_export_dates_max_diff'));
                }
            });
        }
    }
}
