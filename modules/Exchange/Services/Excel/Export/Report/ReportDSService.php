<?php

namespace Modules\Exchange\Services\Excel\Export\Report;

use App\Http\Requests\FormRequest;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

use Modules\Exchange\Services\Excel\Export\Report\DS\ReportDSServiceSheet1;
use Modules\Exchange\Services\Excel\Export\Report\DS\ReportDSServiceSheet2;
use Modules\Exchange\Services\Excel\Export\Report\DS\ReportDSServiceSheet3;

class ReportDSService
{
    public function __construct(
        private Spreadsheet $spreadsheet,
        private FormRequest $request,
        private mixed $regions,
        private mixed $brands,
        private mixed $products,
        private mixed $shops,
    ) {
    }

    public function getSpreadsheet(): Spreadsheet
    {
        return $this->spreadsheet;
    }

    public function process(): self
    {
        ReportDSServiceSheet1::process(
            spreadsheet: $this->spreadsheet,
            regions: $this->regions,
            brands: $this->brands,
            products: $this->products,
        );

        ReportDSServiceSheet2::process(
            spreadsheet: $this->spreadsheet,
            request: $this->request,
            regions: $this->regions,
            shops: $this->shops,
        );

        ReportDSServiceSheet3::process(
            spreadsheet: $this->spreadsheet,
            request: $this->request,
            regions: $this->regions,
            products: $this->products,
        );

        foreach ($this->spreadsheet->getAllSheets() as $sheet) {
            $sheet->setSelectedCell('A1');
        }

        $this->spreadsheet->setActiveSheetIndex(0);

        return $this;
    }
}
