<?php

namespace Modules\Exchange\Services\Excel\Export\Report\DS;

use Illuminate\Support\Arr;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class ReportDSServiceSheet1
{
    public static function process(
        Spreadsheet &$spreadsheet,
        mixed $regions,
        mixed $brands,
        mixed $products,
    ) {
        $sheet = $spreadsheet->getSheet(0);
        $sheet->setTitle('Portfolio');

        // Head for regions and brands

        $sheet->setCellValue('B2', 'Region');
        $sheet->setCellValue('C2', 'TTL QTY');
        $sheet->setCellValue('D2', 'TTL BM coverage');
        $sheet->setCellValue('E2', 'TTL BM coverage %');

        $column = 'F';

        foreach ($brands as $key => $brand) {
            $sheet->setCellValue($column . '2', "$brand->name TTL");
            $sheet->setCellValue(self::incrementColumn($column, 1) . '2', "BM $brand->name");
            $sheet->setCellValue(self::incrementColumn($column, 2) . '2', 'Coverage %');

            if ($key % 2) {
                $sheet->getStyle($column . '2:' . self::incrementColumn($column, 2) . '2')->getFill()->applyFromArray([
                    'fillType' => Fill::FILL_SOLID,
                    'color' => ['rgb' => 'dddddd'],
                ]);
            }

            $column = self::incrementColumn($column, 3);
        }

        // Data for regions and brands

        $row = 3;

        foreach ($regions as $region) {
            $quantity = [
                'all' => data_get($products, "$region->id.all.*.quantity"),
                'bm' => data_get($products, "$region->id.bm.*.quantity"),
            ];

            $sheet->setCellValue("B$row", $region->name);
            $sheet->setCellValue("C$row", array_sum($quantity['all']));
            $sheet->setCellValue("D$row", array_sum($quantity['bm']));
            $sheet->setCellValue("E$row", "=D$row/C$row");
            $sheet->getStyle("E$row")->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_PERCENTAGE);

            $column = 'F';

            foreach ($brands as $key => $brand) {
                $quantity = [
                    'all' => Arr::get($products, "$region->id.all.$brand->id.quantity", 0),
                    'bm' => Arr::get($products, "$region->id.bm.$brand->id.quantity", 0),
                ];

                $sheet->setCellValue($column . $row, $quantity['all']);
                $sheet->setCellValue(self::incrementColumn($column, 1) . $row, $quantity['bm']);
                $sheet->setCellValue(
                    self::incrementColumn($column, 2) . $row,
                    '=' . self::incrementColumn($column, 1) . $row . '/' . $column . $row
                );

                $sheet->getStyle(self::incrementColumn($column, 2) . $row)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_PERCENTAGE);

                $column = self::incrementColumn($column, 3);
            }

            $row++;
        }

        $sheet->setCellValue("B$row", 'Общий итог');
        $sheet->setCellValue("C$row", "=SUM(C3:C" . ($row - 1) . ")");
        $sheet->setCellValue("D$row", "=SUM(D3:D" . ($row - 1) . ")");
        $sheet->setCellValue("E$row", "=D$row/C$row");
        $sheet->getStyle("E$row")->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_PERCENTAGE);

        $column = 'F';

        foreach ($brands as $key => $brand) {
            $sheet->setCellValue($column . $row, "=SUM({$column}3:$column" . ($row - 1) . ")");
            $sheet->setCellValue(
                self::incrementColumn($column, 1) . $row,
                "=SUM(" . self::incrementColumn($column, 1) . "3:" . self::incrementColumn($column, 1) . ($row - 1) . ")"
            );

            $sheet->setCellValue(
                self::incrementColumn($column, 2) . $row,
                '=' . self::incrementColumn($column, 1) . $row . '/' . $column . $row
            );

            $sheet->getStyle(self::incrementColumn($column, 2) . $row)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_PERCENTAGE);

            $column = self::incrementColumn($column, 3);
        }

        // Data for brands

        $totalsRow = $row;
        $row += 3;
        $brandsStartRow = $row;

        $sheet->setCellValue("B$row", 'Brand');
        $sheet->setCellValue("C$row", 'QTY');
        $sheet->setCellValue("D$row", 'BM');
        $sheet->setCellValue("E$row", 'Coverage %');

        $row++;

        foreach ($brands as $key => $brand) {
            $linkedColumn = [
                'all' => self::incrementColumn('F', $key * 3),
                'bm' => self::incrementColumn('F', $key * 3 + 1),
                'percentage' => self::incrementColumn('F', $key * 3 + 2),
            ];

            $sheet->setCellValue("B$row", $brand->name);
            $sheet->setCellValue("C$row", '=' . $linkedColumn['all'] . $totalsRow);
            $sheet->setCellValue("D$row", '=' . $linkedColumn['bm'] . $totalsRow);
            $sheet->setCellValue("E$row", '=' . $linkedColumn['percentage'] . $totalsRow);
            $sheet->getStyle("E$row")->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_PERCENTAGE);

            $row++;
        }

        $sheet->setCellValue("B$row", 'TTL');
        $sheet->setCellValue("C$row", "=C$totalsRow");
        $sheet->setCellValue("D$row", "=D$totalsRow");
        $sheet->setCellValue("E$row", "=E$totalsRow");
        $sheet->getStyle("E$row")->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_PERCENTAGE);

        $brandsEndRow = $row;

        // Style

        $maxColumn = $sheet->getHighestDataColumn();
        $maxRow = $sheet->getHighestDataRow();

        foreach ($sheet->getColumnIterator() as $column) {
            $sheet->getColumnDimension($column->getColumnIndex())->setAutoSize(true);
        }

        $sheet->getStyle("A1:{$maxColumn}{$maxRow}")->getAlignment()->setHorizontal('center');
        $sheet->getStyle("A1:{$maxColumn}{$maxRow}")->getAlignment()->setVertical('center');

        // Style for regions and brands

        $sheet->getStyle("B2:{$maxColumn}2")->getFont()->setSize(12);
        $sheet->getStyle("B2:{$maxColumn}2")->getFont()->setBold(true);
        $sheet->getStyle("B{$totalsRow}:{$maxColumn}{$totalsRow}")->getFont()->setBold(true);

        $sheet->getStyle('B2:E2')->getFill()->applyFromArray([
            'fillType' => Fill::FILL_SOLID,
            'color' => ['rgb' => 'fff707'],
        ]);

        $sheet->getStyle("B2:{$maxColumn}{$totalsRow}")
            ->getBorders()
            ->getAllBorders()
            ->setBorderStyle(Border::BORDER_THIN)
            ->setColor(new Color('666666'));

        $borderCellGroups = [
            "B2:{$maxColumn}{$totalsRow}",
            "B2:{$maxColumn}2",
            "B{$totalsRow}:{$maxColumn}{$totalsRow}",
            "B2:B{$totalsRow}",
            "C2:E{$totalsRow}",
        ];

        for ($column = 'F'; $column <= $maxColumn; $column = chr(ord($column) + 3)) {
            $borderCellGroups[] = "{$column}2:" . chr(ord($column) + 2) . "$totalsRow";
        }

        foreach ($borderCellGroups as $cells) {
            $sheet->getStyle($cells)
                ->getBorders()
                ->getOutline()
                ->setBorderStyle(Border::BORDER_MEDIUM)
                ->setColor(new Color('222222'));
        }

        // Style for brands

        $sheet->getStyle("B$brandsStartRow:E$brandsStartRow")->getFont()->setBold(true);
        $sheet->getStyle("B$brandsEndRow:E$brandsEndRow")->getFont()->setBold(true);

        $sheet->getStyle("B$brandsStartRow:E$brandsStartRow")->getFill()->applyFromArray([
            'fillType' => Fill::FILL_SOLID,
            'color' => ['rgb' => 'fff707'],
        ]);

        $sheet->getStyle("B$brandsStartRow:E$brandsEndRow")
            ->getBorders()
            ->getAllBorders()
            ->setBorderStyle(Border::BORDER_THIN)
            ->setColor(new Color('666666'));

        $borderCellGroups = [
            "B$brandsStartRow:E$brandsEndRow",
            "B$brandsStartRow:E$brandsStartRow",
            "B$brandsEndRow:E$brandsEndRow",
            "B$brandsStartRow:B$brandsEndRow",
        ];

        foreach ($borderCellGroups as $cells) {
            $sheet->getStyle($cells)
                ->getBorders()
                ->getOutline()
                ->setBorderStyle(Border::BORDER_MEDIUM)
                ->setColor(new Color('222222'));
        }
    }

    private static function incrementColumn(string $column, int $step)
    {
        for ($i = 0; $i < abs($step); $i++) {
            $column++;
        }

        return $column;
    }
}
