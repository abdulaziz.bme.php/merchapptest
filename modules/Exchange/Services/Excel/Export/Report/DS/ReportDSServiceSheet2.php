<?php

namespace Modules\Exchange\Services\Excel\Export\Report\DS;

use Illuminate\Support\Arr;
use App\Http\Requests\FormRequest;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class ReportDSServiceSheet2
{
    public static function process(
        Spreadsheet &$spreadsheet,
        FormRequest $request,
        mixed $regions,
        mixed $shops,
    ) {
        $spreadsheet->createSheet(1);
        $sheet = $spreadsheet->getSheet(1);
        $sheet->setTitle('TTL Shop');

        // Data

        $sheet->setCellValue('B2', 'BM Coverage by IR shops');
        $sheet->mergeCells('B2:E2');

        $sheet->setCellValue('C3', "$request->date_from - $request->date_to");
        $sheet->mergeCells('C3:E3');

        $sheet->setCellValue('B4', 'Region');
        $sheet->setCellValue('C4', 'TTL Shop QTY');
        $sheet->setCellValue('D4', 'BM Coverage shop QTY');
        $sheet->setCellValue('E4', 'BM coverage %');

        $row = 5;

        foreach ($regions as $region) {
            $quantity = [
                'all' => data_get($shops, "$region->id.all.quantity"),
                'bm' => data_get($shops, "$region->id.bm.quantity"),
            ];

            $sheet->setCellValue("B$row", $region->name);
            $sheet->setCellValue("C$row", $quantity['all']);
            $sheet->setCellValue("D$row", $quantity['bm']);
            $sheet->setCellValue("E$row", "=D$row/C$row");
            $sheet->getStyle("E$row")->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_PERCENTAGE);

            $row++;
        }

        $sheet->setCellValue("B$row", 'Общий итог');
        $sheet->setCellValue("C$row", "=SUM(C5:C" . ($row - 1) . ")");
        $sheet->setCellValue("D$row", "=SUM(D5:D" . ($row - 1) . ")");
        $sheet->setCellValue("E$row", "=D$row/C$row");
        $sheet->getStyle("E$row")->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_PERCENTAGE);

        // Style

        foreach (range('A', 'E') as $column) {
            $sheet->getColumnDimension($column)->setAutoSize(true);
        }

        $sheet->getStyle("A1:E$row")->getAlignment()->setHorizontal('center');
        $sheet->getStyle("A1:E$row")->getAlignment()->setVertical('center');

        $sheet->getStyle("B2:E4")->getFont()->setBold(true);
        $sheet->getStyle("B$row:E$row")->getFont()->setBold(true);
        $sheet->getStyle("E5:E$row")->getFont()->setBold(true);

        $sheet->getStyle("B2:E3")->getFill()->applyFromArray([
            'fillType' => Fill::FILL_SOLID,
            'color' => ['rgb' => '4a00c3'],
        ]);

        $sheet->getStyle("B2:E3")->getFont()->setColor(new Color('ffffff'));

        $sheet->getStyle("B4:E4")->getFill()->applyFromArray([
            'fillType' => Fill::FILL_SOLID,
            'color' => ['rgb' => 'fff707'],
        ]);

        $sheet->getStyle("B2:E$row")
            ->getBorders()
            ->getAllBorders()
            ->setBorderStyle(Border::BORDER_THIN)
            ->setColor(new Color('666666'));

        $borderCellGroups = [
            "B2:E$row",
            "B2:E2",
            "B3:E3",
            "B4:E4",
            "B$row:E$row",
            "B3:B$row",
            "C3:E$row",
        ];

        foreach ($borderCellGroups as $cells) {
            $sheet->getStyle($cells)
                ->getBorders()
                ->getOutline()
                ->setBorderStyle(Border::BORDER_MEDIUM)
                ->setColor(new Color('222222'));
        }
    }
}
