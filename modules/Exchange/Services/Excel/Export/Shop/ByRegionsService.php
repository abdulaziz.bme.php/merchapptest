<?php

namespace Modules\Exchange\Services\Excel\Export\Shop;

use Illuminate\Support\Arr;
use App\Http\Requests\FormRequest;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class ByRegionsService
{
    public function __construct(
        private Spreadsheet $spreadsheet,
        private FormRequest $request,
        private array $records,
    ) {
    }

    public function getSpreadsheet(): Spreadsheet
    {
        return $this->spreadsheet;
    }

    public function process(): self
    {
        $sheet = $this->spreadsheet->getSheet(0);
        $sheet->setTitle('Shops');

        $sheet->setCellValue('A1', 'BM Coverage by IR shops');
        $sheet->mergeCells('A1:E1');

        $sheet->setCellValue('C2', $this->request->date_from . ' - ' . $this->request->date_to);
        $sheet->mergeCells('C2:E2');

        // Data

        $sheet->setCellValueByColumnAndRow(1, 3, 'Region');
        $sheet->setCellValueByColumnAndRow(2, 3, 'TTL region shop QTY');
        $sheet->setCellValueByColumnAndRow(3, 3, 'TTL period shop QTY');
        $sheet->setCellValueByColumnAndRow(4, 3, 'BM coverage shop QTY');
        $sheet->setCellValueByColumnAndRow(5, 3, 'BM coverage %');

        foreach ($this->records as $recordKey => $record) {
            $row = $recordKey + 4;

            $sheet->setCellValueByColumnAndRow(1, $row, $record['name']);
            $sheet->setCellValueByColumnAndRow(2, $row, $record['quantity']['total']);
            $sheet->setCellValueByColumnAndRow(3, $row, $record['quantity']['period_total']);
            $sheet->setCellValueByColumnAndRow(4, $row, $record['quantity']['period_bm']);
            
            $sheet->setCellValueByColumnAndRow(5, $row, "=D$row/C$row");
            $sheet->getStyleByColumnAndRow(5, $row)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_PERCENTAGE);
        }

        // Totals

        $totalsRow = 4 + count($this->records);

        $sheet->setCellValueByColumnAndRow(1, $totalsRow, 'Общий итог');
        $sheet->setCellValueByColumnAndRow(2, $totalsRow, "=SUM(B4:B" . ($totalsRow - 1) . ")");
        $sheet->setCellValueByColumnAndRow(3, $totalsRow, "=SUM(C4:C" . ($totalsRow - 1) . ")");
        $sheet->setCellValueByColumnAndRow(4, $totalsRow, "=SUM(D4:D" . ($totalsRow - 1) . ")");

        $sheet->setCellValueByColumnAndRow(5, $totalsRow, "=D$totalsRow/C$totalsRow");
        $sheet->getStyleByColumnAndRow(5, $totalsRow)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_PERCENTAGE);

        // Style

        $maxColumn = $sheet->getHighestDataColumn();
        $maxRow = $sheet->getHighestDataRow();

        foreach ($sheet->getColumnIterator() as $column) {
            $sheet->getColumnDimension($column->getColumnIndex())->setAutoSize(true);
        }

        $sheet->getDefaultRowDimension()->setRowHeight(-1);

        $sheet->getStyle('A1:' . $maxColumn . $maxRow)->getAlignment()->setHorizontal('center');
        $sheet->getStyle('A1:' . $maxColumn . $maxRow)->getAlignment()->setVertical('center');

        $sheet->getStyle("A1:{$maxColumn}3")->getFont()->setBold(true);
        $sheet->getStyle("A{$maxRow}:{$maxColumn}{$maxRow}")->getFont()->setBold(true);
        $sheet->getStyle("{$maxColumn}1:{$maxColumn}{$maxRow}")->getFont()->setBold(true);

        $sheet->getStyle('A1:E2')->getFill()->applyFromArray([
            'fillType' => Fill::FILL_SOLID,
            'color' => ['rgb' => '4a00c3'],
        ]);

        $sheet->getStyle('A1:E2')->getFont()->setColor(new Color('ffffff'));

        $sheet->getStyle('A3:E3')->getFill()->applyFromArray([
            'fillType' => Fill::FILL_SOLID,
            'color' => ['rgb' => 'ffff01'],
        ]);

        $sheet->getStyle("A1:{$maxColumn}{$maxRow}")
            ->getBorders()
            ->getAllBorders()
            ->setBorderStyle(Border::BORDER_THIN)
            ->setColor(new Color('666666'));

        return $this;
    }
}
