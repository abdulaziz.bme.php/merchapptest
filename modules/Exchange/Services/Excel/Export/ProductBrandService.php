<?php

namespace Modules\Exchange\Services\Excel\Export;

use Illuminate\Support\Arr;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class ProductBrandService
{
    public function __construct(
        private Spreadsheet $spreadsheet,
        private array $records,
    ) {
    }

    public function getSpreadsheet(): Spreadsheet
    {
        return $this->spreadsheet;
    }

    public function process(): self
    {
        $sheet = $this->spreadsheet->getSheet(0);
        $sheet->setTitle('Brands');

        // Data

        $sheet->setCellValueByColumnAndRow(1, 1, 'Brand');
        $sheet->setCellValueByColumnAndRow(2, 1, 'Area');
        $sheet->setCellValueByColumnAndRow(3, 1, 'TTL QTY');

        $suppliers = Arr::pluck($this->records, 'supplier_name');
        $suppliers = array_unique($suppliers);
        $suppliers = array_values($suppliers);

        foreach ($suppliers as $key => $supplier) {
            $startColumn = 4 + $key * 2;

            $sheet->setCellValueByColumnAndRow($startColumn, 1, "$supplier QTY");
            $sheet->setCellValueByColumnAndRow($startColumn + 1, 1, "Coverage $supplier");
        }

        $groupedRecords = [];

        foreach ($this->records as $value) {
            $key = $value['name'] . ' | ' . $value['region_name'];

            $groupedRecords[$key] ??= [
                'name' => $value['name'],
                'region_name' => $value['region_name'],
                'suppliers' => [],
            ];

            $supplier = $value['supplier_name'];
            $groupedRecords[$key]['suppliers'][$supplier] = $value['quantity'];
        }

        foreach (array_values($groupedRecords) as $recordKey => $record) {
            $row = $recordKey + 2;

            $sheet->setCellValueByColumnAndRow(1, $row, $record['name']);
            $sheet->setCellValueByColumnAndRow(2, $row, $record['region_name']);

            $totalQuantityCells = [];

            foreach ($suppliers as $supplierKey => $supplier) {
                $startColumn = 4 + $supplierKey * 2;

                $totalQuantityCells[] = $sheet->getCellByColumnAndRow($startColumn, $row)->getCoordinate();

                $sheet->setCellValueByColumnAndRow($startColumn, $row, Arr::get($record, "suppliers.$supplier", 0));

                $percentageCellFormula = '=' . end($totalQuantityCells) . "/C$row";
                $sheet->setCellValueByColumnAndRow($startColumn + 1, $row, $percentageCellFormula);
                $sheet->getStyleByColumnAndRow($startColumn + 1, $row)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_PERCENTAGE);
            }

            $totalQuantityFormula = '=' . implode('+', $totalQuantityCells);
            $sheet->setCellValueByColumnAndRow(3, $row, $totalQuantityFormula);
        }

        // Style

        $maxColumn = $sheet->getHighestDataColumn();
        $maxRow = $sheet->getHighestDataRow();

        foreach ($sheet->getColumnIterator() as $column) {
            $sheet->getColumnDimension($column->getColumnIndex())->setAutoSize(true);
        }

        $sheet->getStyle("A1:{$maxColumn}1")->getFont()->setBold(true);

        $sheet->getStyle('A1:' . $maxColumn . $maxRow)->getAlignment()->setHorizontal('center');
        $sheet->getStyle('A1:' . $maxColumn . $maxRow)->getAlignment()->setVertical('center');

        $sheet->getStyle("A1:{$maxColumn}{$maxRow}")
            ->getBorders()
            ->getAllBorders()
            ->setBorderStyle(Border::BORDER_THIN)
            ->setColor(new Color('666666'));

        $sheet->getStyle("B1:B$maxRow")->getFill()->applyFromArray([
            'fillType' => Fill::FILL_SOLID,
            'color' => ['rgb' => 'ffc100'],
        ]);

        $sheet->getStyle("C1:C$maxRow")->getFill()->applyFromArray([
            'fillType' => Fill::FILL_SOLID,
            'color' => ['rgb' => 'fe0000'],
        ]);

        foreach ($suppliers as $key => $supplier) {
            if ($key % 2) {
                $startColumn = 4 + $key * 2;

                $sheet->getStyleByColumnAndRow($startColumn, 1, $startColumn + 1, $maxRow)->getFill()->applyFromArray([
                    'fillType' => Fill::FILL_SOLID,
                    'color' => ['rgb' => 'fff707'],
                ]);
            }
        }

        return $this;
    }
}
