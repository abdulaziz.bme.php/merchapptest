<?php

namespace Modules\Exchange\Services\Excel\Export;

use Illuminate\Support\Arr;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class ShopSupplierService
{
    public function __construct(
        private Spreadsheet $spreadsheet,
        private array $records,
    ) {
    }

    public function getSpreadsheet(): Spreadsheet
    {
        return $this->spreadsheet;
    }

    public function process(): self
    {
        $sheet = $this->spreadsheet->getSheet(0);
        $sheet->setTitle('Suppliers');

        // Data

        $sheet->setCellValueByColumnAndRow(1, 1, 'Distributor');

        $regions = Arr::pluck($this->records, 'region_name');
        $regions = array_unique($regions);
        $regions = array_values($regions);

        $groupedRecords = [];

        foreach ($this->records as $value) {
            $key = $value['name'];

            $groupedRecords[$key] ??= [
                'name' => $value['name'],
                'regions' => [],
            ];

            $region = $value['region_name'];
            $groupedRecords[$key]['regions'][$region] = $value['quantity'];
        }

        $sheet->setCellValueByColumnAndRow(1, 2 + count($groupedRecords), 'TTL QTY');

        foreach ($regions as $key => $region) {
            $startColumn = 2 + $key * 2;

            $sheet->setCellValueByColumnAndRow($startColumn, 1, "$region QTY");
            $sheet->setCellValueByColumnAndRow($startColumn + 1, 1, "Coverage $region");

            $totalQuantityCellFrom = $sheet->getCellByColumnAndRow($startColumn, 2)->getCoordinate();
            $totalQuantityCellTo = $sheet->getCellByColumnAndRow($startColumn, count($groupedRecords) + 1)->getCoordinate();
            $totalQuantityCellFormula = "=SUM($totalQuantityCellFrom:$totalQuantityCellTo)";

            $sheet->setCellValueByColumnAndRow($startColumn, 2 + count($groupedRecords), $totalQuantityCellFormula);
            $sheet->mergeCellsByColumnAndRow($startColumn, 2 + count($groupedRecords), $startColumn + 1, 2 + count($groupedRecords));
        }

        foreach (array_values($groupedRecords) as $recordKey => $record) {
            $row = $recordKey + 2;

            $sheet->setCellValueByColumnAndRow(1, $row, $record['name']);

            foreach ($regions as $regionKey => $region) {
                $startColumn = 2 + $regionKey * 2;

                $sheet->setCellValueByColumnAndRow($startColumn, $row, Arr::get($record, "regions.$region", 0));

                $percentageCellFrom = $sheet->getCellByColumnAndRow($startColumn, $row)->getCoordinate();
                $percentageCellTo = $sheet->getCellByColumnAndRow($startColumn, 2 + count($groupedRecords))->getCoordinate();
                $percentageCellFormula = "=$percentageCellFrom/$percentageCellTo";

                $sheet->setCellValueByColumnAndRow($startColumn + 1, $row, $percentageCellFormula);
                $sheet->getStyleByColumnAndRow($startColumn + 1, $row)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_PERCENTAGE);
            }
        }

        // Style

        $maxColumn = $sheet->getHighestDataColumn();
        $maxRow = $sheet->getHighestDataRow();

        foreach ($sheet->getColumnIterator() as $column) {
            $sheet->getColumnDimension($column->getColumnIndex())->setAutoSize(true);
        }

        $sheet->getStyle("A1:{$maxColumn}1")->getFont()->setBold(true);
        $sheet->getStyle("A{$maxRow}:{$maxColumn}{$maxRow}")->getFont()->setBold(true);

        $sheet->getStyle('A1:' . $maxColumn . $maxRow)->getAlignment()->setHorizontal('center');
        $sheet->getStyle('A1:' . $maxColumn . $maxRow)->getAlignment()->setVertical('center');

        $sheet->getStyle("A1:{$maxColumn}{$maxRow}")
            ->getBorders()
            ->getAllBorders()
            ->setBorderStyle(Border::BORDER_THIN)
            ->setColor(new Color('666666'));

        $sheet->getStyle("B{$maxRow}:{$maxColumn}{$maxRow}")->getFill()->applyFromArray([
            'fillType' => Fill::FILL_SOLID,
            'color' => ['rgb' => '5ed595'],
        ]);

        foreach ($regions as $key => $region) {
            if ($key % 2) {
                $startColumn = 2 + $key * 2;

                $sheet->getStyleByColumnAndRow($startColumn, 1, $startColumn + 1, $maxRow - 1)->getFill()->applyFromArray([
                    'fillType' => Fill::FILL_SOLID,
                    'color' => ['rgb' => 'fff707'],
                ]);
            }
        }

        return $this;
    }
}
