<?php

namespace Modules\Exchange\Services\Excel\Export;

use PhpOffice\PhpSpreadsheet\Spreadsheet;

class ProductService
{
    public function __construct(
        private Spreadsheet $spreadsheet,
        private array $sheet,
        private mixed $products,
    ) {
    }

    public function getSpreadsheet(): Spreadsheet
    {
        return $this->spreadsheet;
    }

    public function process(): self
    {
        $this->spreadsheet->createSheet($this->sheet['index']);
        $sheet = $this->spreadsheet->getSheet($this->sheet['index']);
        $sheet->setTitle($this->sheet['name']);

        // Data

        $sheet->setCellValue('A1', __('fields.id'));
        $sheet->setCellValue('B1', __('fields.model_number'));
        $sheet->setCellValue('C1', __('fields.date_eol'));
        $sheet->setCellValue('D1', __('fields.brand_id'));

        foreach ($this->products as $key => $product) {
            $row = $key + 2;

            $sheet->setCellValue("A$row", $product->id);
            $sheet->setCellValue("B$row", $product->model_number);
            $sheet->setCellValue("C$row", date('d.m.Y', strtotime($product->date_eol)));
            $sheet->setCellValue("D$row", $product->brand_name);
        }

        // Style

        $maxColumn = $sheet->getHighestDataColumn();
        $maxRow = $sheet->getHighestDataRow();

        foreach ($sheet->getColumnIterator() as $column) {
            $sheet->getColumnDimension($column->getColumnIndex())->setAutoSize(true);
        }
        
        $sheet->getStyle("A1:{$maxColumn}1")->getFont()->setBold(true);

        $sheet->getStyle('A1:' . $maxColumn. $maxRow)->getAlignment()->setHorizontal('center');
        $sheet->getStyle('A1:' . $maxColumn. $maxRow)->getAlignment()->setVertical('center');

        return $this;
    }
}
