<?php

namespace Modules\User\Services;

use App\Services\ActiveService;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use Modules\User\Mail\UserResetPasswordMail;
use Illuminate\Support\Facades\Hash;
use Modules\Region\Models\Region;
use Modules\Region\Models\RegionCity;

class UserService extends ActiveService
{
    public function availableCities()
    {
        $regions = $this->model->regions;
        $cities = $this->model->cities->pluck('id')->toArray();

        if (!$regions && !$cities) {
            return RegionCity::query()->select(['id'])->get()->pluck('id')->toArray();
        }

        foreach ($regions as $region) {
            $regionAllCities = $region->cities->pluck('id')->toArray();

            if (!array_intersect($regionAllCities, $cities)) {
                $cities = array_merge($cities, $regionAllCities);
            }
        }

        return $cities;
    }

    public function sendResetPasswordCode()
    {
        $reset_password_code = rand(1000, 9999);

        $this->model->reset_password_code = $reset_password_code;
        $this->model->saveQuietly();

        Mail::to([$this->model->email])->send(new UserResetPasswordMail($reset_password_code));
    }

    public function resetPassword(string $password)
    {
        $this->model->password = Hash::make($password);
        $this->model->reset_password_code = null;
        $this->model->saveQuietly();
    }
}
