<?php

namespace Modules\User\Http\Public\Controllers;

use App\Http\Controllers\Controller;
use Modules\User\Resources\UserResource;

class UserProfileController extends Controller
{
    public function show()
    {
        $response = UserResource::make(auth()->user());
        return response()->json($response, 200);
    }
}
