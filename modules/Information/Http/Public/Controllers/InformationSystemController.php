<?php

namespace Modules\Information\Http\Public\Controllers;

use App\Http\Controllers\Controller;
use Modules\System\Models\SystemSettings;
use Modules\System\Resources\SystemSettingsResource;
use Modules\System\Resources\SystemLanguageResource;
use App\Services\LocalizationService;
use Illuminate\Support\Arr;

class InformationSystemController extends Controller
{
    public function index()
    {
        // Settings

        $settings = SystemSettings::query()->orderBy('name')->get()->keyBy('name');
        $settings = SystemSettingsResource::collection($settings);

        // Lanugages

        $locale = app()->getLocale();
        $localizationService = LocalizationService::getInstance();

        $languages = [
            'all' => SystemLanguageResource::collection($localizationService->allLanguages),
            'active' => SystemLanguageResource::collection($localizationService->activeLanguages),
        ];

        $languages['current'] = $locale;
        $languages['main'] = Arr::keyBy($languages['active'], 'is_main')[1];

        // Translations

        $translations = [];

        foreach ($languages['active'] as $language) {
            $path = lang_path($language->code);

            $translations[$language->code] = [
                'fields' => require("$path/fields.php"),
            ];
        }

        $response = [
            'settings' => $settings,
            'languages' => $languages,
            'translations' => $translations,
        ];

        return response()->json($response, 200);
    }

    public function settings()
    {
        $response = SystemSettings::query()->orderBy('name')->get()->keyBy('name');
        $response = SystemSettingsResource::collection($response);

        return response()->json($response, 200);
    }

    public function languages()
    {
        $localizationService = LocalizationService::getInstance();

        $response = [
            'all' => SystemLanguageResource::collection($localizationService->allLanguages),
            'active' => SystemLanguageResource::collection($localizationService->activeLanguages),
        ];

        $response['current'] = app()->getLocale();
        $response['main'] = Arr::keyBy($response['active'], 'is_main')[1];

        return response()->json($response, 200);
    }
}
