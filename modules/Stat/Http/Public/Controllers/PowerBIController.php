<?php

namespace Modules\Stat\Http\Public\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Modules\Stat\Http\Public\Requests\PowerBI\ProductRequest;
use Modules\Report\Models\ReportProduct;

class PowerBIController extends Controller
{
    public function product(ProductRequest $request)
    {
        $locale = app()->getLocale();

        // Preparing

        $dateFrom = date('Y-m-d', strtotime($request->date_from));
        $dateTo = date('Y-m-d', strtotime($request->date_to));

        // Getting reports products

        $products = ReportProduct::query()
            ->joinRelation('report.shop.city.region')
            ->joinRelation('product.brand')
            ->joinRelation('supplier')
            ->leftJoinRelation('variation')
            ->whereHas('report', function ($q) use ($dateFrom, $dateTo) {
                $q->where('updated_at', '>=', $dateFrom)
                    ->where('updated_at', '<=', $dateTo);
            })
            ->select([
                DB::raw("region.name->>'$locale' as region_name"),
                DB::raw("region_city.name->>'$locale' as city_name"),
                'shop.number as shop_number',
                'shop.focus_code as shop_focus_code',
                'product_brand.name as brand_name',
                'product.model_number as product_model_number',
                'product_variation.name as variation_name',
                'product_variation.sku as variation_sku',
                'report_product.variation_id as variation_id',
                'shop_supplier.full_name as supplier_full_name',
                'report_product.quantity as quantity',
                'report_product.quantity as quantity_diff',
                'report.comment as comment',
                'report.shop_is_closed as shop_is_closed',
                DB::raw("TO_CHAR(report.updated_at, 'DD.MM.YYYY') as date"),

                'report.id as report_id',
                DB::raw("CONCAT(report_product.product_id, '_', report_product.variation_id) as product_and_variation_id"),
            ])
            ->orderBy('report.updated_at')
            ->getQuery()
            ->get()
            ->map(function ($value, $key) use ($locale) {
                if ($value->variation_name) {
                    $variationName = json_decode($value->variation_name, true);
                    $variationName = data_get($variationName, "*.$locale");
                    $variationName = implode(' ', $variationName);

                    $value->variation_name = $variationName;
                }

                return $value;
            })
            ->groupBy([
                'shop_number',
                fn ($value) => "{$value->date}_{$value->report_id}",
            ])
            ->toArray();

        foreach ($products as &$productsByDateAndReportId) {
            $prevProductsByReport = [];

            foreach ($productsByDateAndReportId as &$productsByReport) {
                $productsByReport = Arr::keyBy($productsByReport, 'product_and_variation_id');
                $firstProductFromReport = reset($productsByReport);

                foreach ($productsByReport as $productAndVariationId => &$product) {
                    $productFromPrevReport = $prevProductsByReport[$productAndVariationId] ?? null;

                    if ($productFromPrevReport) {
                        $product->quantity_diff = $product->quantity - $productFromPrevReport['quantity'];
                    } else {
                        $product->quantity_diff = $product->quantity;
                    }
                }

                $newProducts = [];

                foreach ($prevProductsByReport as $productAndVariationId => $prevProduct) {
                    if (!isset($productsByReport[$productAndVariationId])) {
                        $newProducts[$productAndVariationId] = [
                            ...$prevProduct,
                            'quantity' => 0,
                            'quantity_diff' => -$prevProduct['quantity'],
                            'supplier_full_name' => $firstProductFromReport->supplier_full_name,
                            'comment' => $firstProductFromReport->comment,
                            'shop_is_closed' => $firstProductFromReport->shop_is_closed,
                            'date' => $firstProductFromReport->date,
                            'report_id' => $firstProductFromReport->report_id,
                        ];
                    }
                }

                $prevProductsByReport = array_map(fn ($value) => (array)$value, $productsByReport);

                $productsByReport = array_merge($productsByReport, $newProducts);
                $productsByReport = array_map(fn ($value) => (object)$value, $productsByReport);
            }
        }

        $products = Arr::flatten($products);

        return response()->json($products, 200);
    }
}
