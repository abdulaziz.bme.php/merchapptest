<?php

namespace Modules\Product\Resources;

use App\Resources\JsonResource;
use Modules\Product\Enums\ProductSpecificationEnums;
use Illuminate\Support\Arr;

class ProductSpecificationOptionResource extends JsonResource
{
    public function toArray($request)
    {
        if ($this->specification_id == 1) {
            $colors = ProductSpecificationEnums::colors();
            $colorName = Arr::get($this->name, app()->getLocale());
            $colorName = mb_strtolower($colorName);
            $color = Arr::get($colors, $colorName);
        }

        return [
            'id' => $this->id,
            'specification_id' => $this->specification_id,
            'name' => $this->name,
            'color' => $this->when($this->specification_id == 1, $color ?? null),
            'created_at' => $this->created_at->format('d.m.Y H:i:s'),
            'updated_at' => $this->updated_at->format('d.m.Y H:i:s'),
        ];
    }
}
