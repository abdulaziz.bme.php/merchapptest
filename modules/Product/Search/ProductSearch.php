<?php

namespace Modules\Product\Search;

use App\Search\Search;

class ProductSearch extends Search
{
    protected array $relations = [
        'creator', 'brand',
        'category', 'category.specifications.options',
        'variations', 'variations.options',
    ];

    protected array $filterable = [
        'id' => Search::FILTER_TYPE_IN,
        'creator_id' => Search::FILTER_TYPE_EQUAL,
        'category_id' => Search::FILTER_TYPE_EQUAL,
        'brand_id' => Search::FILTER_TYPE_EQUAL,
        'model_number' => Search::FILTER_TYPE_LIKE,
        'date_eol' => Search::FILTER_TYPE_DATE,
        'is_active' => Search::FILTER_TYPE_EQUAL,
    ];

    protected array $combinedFilterable = [
        'common' => [
            'type' => Search::COMBINED_TYPE_OR,
            'fields' => [
                'model_number' => Search::FILTER_TYPE_LIKE,
                'brand.name' => Search::FILTER_TYPE_LIKE,
            ],
        ],
    ];

    protected array $sortable = [
        'id' => Search::SORT_TYPE_SIMPLE,
        'creator_id' => Search::SORT_TYPE_SIMPLE,
        'category_id' => Search::SORT_TYPE_SIMPLE,
        'brand_id' => Search::SORT_TYPE_SIMPLE,
        'model_number' => Search::SORT_TYPE_SIMPLE,
        'date_eol' => Search::SORT_TYPE_SIMPLE,
        'is_active' => Search::SORT_TYPE_SIMPLE,
    ];
}
