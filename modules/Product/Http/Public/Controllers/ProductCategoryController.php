<?php

namespace Modules\Product\Http\Public\Controllers;

use App\Http\Controllers\ApiResourceController;

use Modules\Product\Models\ProductCategory;
use Modules\Product\Search\ProductCategorySearch;
use Modules\Product\Resources\ProductCategoryResource;

class ProductCategoryController extends ApiResourceController
{
    public function __construct()
    {
        return parent::__construct(
            model: new ProductCategory(),
            search: new ProductCategorySearch(),
            resourceClass: ProductCategoryResource::class
        );
    }
}
