<?php

use Illuminate\Support\Facades\Route;

use Modules\Product\Http\Public\Controllers\ProductBrandController;
use Modules\Product\Http\Public\Controllers\ProductCategoryController;
use Modules\Product\Http\Public\Controllers\ProductController;
use Modules\Product\Http\Public\Controllers\ProductVariationController;

use Modules\Product\Models\ProductBrand;
use Modules\Product\Models\ProductCategory;
use Modules\Product\Models\Product;
use Modules\Product\Models\ProductVariation;

Route::prefix('product')
    ->middleware('auth.basic.once', 'role:agent', 'api')
    ->where([
        'brand' => '[0-9]+',
        'category' => '[0-9]+',
        'product' => '[0-9]+',
        'variation' => '[0-9]+',
    ])
    ->group(function () {
        Route::model('brand', ProductBrand::class);
        Route::model('category', ProductCategory::class);
        Route::model('product', Product::class);
        Route::model('variation', ProductVariation::class);

        Route::apiResource('brand', ProductBrandController::class)->only(['index', 'show', 'store']);
        Route::apiResource('category', ProductCategoryController::class)->only(['index', 'show']);
        Route::apiResource('product', ProductController::class)->only(['index', 'show', 'store']);
        Route::apiResource('variation', ProductVariationController::class)->only(['index', 'show', 'store']);
    });
