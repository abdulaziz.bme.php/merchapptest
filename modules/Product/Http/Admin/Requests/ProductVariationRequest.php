<?php

namespace Modules\Product\Http\Admin\Requests;

use App\Http\Requests\ActiveFormRequest;
use Modules\Product\Models\ProductVariation;
use Modules\Product\Models\ProductSpecificationOption;
use Modules\Product\Models\Product;

use Illuminate\Validation\Rule;
use App\Rules\ExistsSoftDeleteRule;
use App\Helpers\FormRequestHelper;
use Illuminate\Support\Arr;

class ProductVariationRequest extends ActiveFormRequest
{
    protected array $ignoredModelFields = ['images_list'];
    protected bool $hasDateFolderForFiles = false;
    protected array $fileFields = [
        'images_list' => 'images',
    ];

    public function __construct()
    {
        return parent::__construct(
            model: new ProductVariation()
        );
    }

    public function rules()
    {
        return [
            'product_id' => [
                'required',
                new ExistsSoftDeleteRule($this->model, 'product'),
            ],
            'sku' => [
                'required',
                Rule::unique($this->model->getTable())->ignore($this->model->id)->where('product_id', $this->product_id),
            ],
            'images_list' => 'array',
            'images_list.*' => 'file|max:4096|mimes:jpg,png,webp',

            'options' => [
                'required',
                'array',
                'exists:product_specification_option,id',
            ],
        ];
    }

    public function withValidator($validator)
    {
        if (!$validator->fails()) {
            $validator->after(function ($validator) {
                $options = ProductSpecificationOption::query()->whereIn('id', $this->options)->get()->toArray();
                $specifications_ids = data_get($options, '*.specification_id');
                $specifications_ids = array_flip($specifications_ids);

                if (count($options) != count($specifications_ids)) {
                    $validator->errors()->add('options', __('rules.product_variation.options.specification_single_option'));
                }

                $variations = ProductVariation::query()
                    ->with('options')
                    ->where('product_id', $this->product_id)
                    ->whereNot('id', $this->model->id)
                    ->get()
                    ->toArray();

                foreach ($variations as $variation) {
                    $options = Arr::pluck($variation['options'], 'id');

                    if (array_values($options) == array_values($this->options)) {
                        $validator->errors()->add('options', __('rules.product_variation.options.unique_combinations_in_product'));
                    }
                }
            });
        }
    }

    protected  function passedValidation()
    {
        parent::passedValidation();

        $product = Product::query()->with(['brand'])->where('id', $this->product_id)->firstOrFail();
        $brandName = $product->brand->name;

        $this->fileFields['images_list'] = "../exchange/product/$brandName/$product->model_number/$this->sku";

        $this->model->fillableRelations = [
            $this->model::RELATION_TYPE_MANY_MANY => [
                'options' => $this->options,
            ],
        ];
    }
}
