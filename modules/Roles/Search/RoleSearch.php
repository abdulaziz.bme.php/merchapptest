<?php

namespace Modules\Roles\Search;

use App\Search\Search;
use Illuminate\Database\Eloquent\Builder;

class RoleSearch extends Search
{
    protected array $relations = [
        'permissions',
    ];

    protected array $filterable = [
        'id'         => Search::FILTER_TYPE_EQUAL,
        'name'       => Search::FILTER_TYPE_LIKE,
        'guard_name' => Search::FILTER_TYPE_LIKE,
    ];

    protected array $sortable = [
        'id'         => Search::SORT_TYPE_SIMPLE,
        'name'       => Search::SORT_TYPE_SIMPLE,
        'guard_name' => Search::SORT_TYPE_SIMPLE,
    ];
}
