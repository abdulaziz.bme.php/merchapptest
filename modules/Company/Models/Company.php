<?php

namespace Modules\Company\Models;

use App\Models\Model;
use Modules\User\Models\User;

class Company extends Model
{
    protected $table    = 'companies';
    protected $fillable = [
        'company_name',
        'is_active',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class, 'company_users');
    }
}
