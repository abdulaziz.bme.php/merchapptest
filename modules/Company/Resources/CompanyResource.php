<?php
/**
 *
 * MerchApp-Web
 * User: Bakhodir @iambakhodir
 * Date: 25/03/23
 * Time: 3:19 PM
 */

namespace Modules\Company\Resources;

use App\Resources\JsonResource;

class CompanyResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id'           => $this->id,
            'company_name' => $this->company_name,
            'is_active'    => $this->is_active,
            'created_at'   => $this->created_at->format("Y-m-d H:i"),
            'updated_at'   => $this->updated_at->format("Y-m-d H:i"),
        ];
    }
}
