<?php

namespace Modules\Shop\Search;

use App\Search\Search;
use Illuminate\Database\Eloquent\Builder;

class ShopMultipleSearch extends Search
{
    protected array $relations = [
        'agent', 'city', 'city.region', 'company',
        'last_report',
        'suppliers', 'contacts', 'brands',
    ];

    protected array $filterable = [
        'id'              => Search::FILTER_TYPE_IN,
        'agent_id'        => Search::FILTER_TYPE_EQUAL,
        'city_id'         => Search::FILTER_TYPE_EQUAL,
        'company_id'      => Search::FILTER_TYPE_EQUAL,
        'name'            => Search::FILTER_TYPE_LIKE,
        'address'         => Search::FILTER_TYPE_LIKE,
        'has_credit_line' => Search::FILTER_TYPE_EQUAL,
        'number'          => Search::FILTER_TYPE_LIKE,
        'focus_code'      => Search::FILTER_TYPE_EQUAL,
        'is_active'       => Search::FILTER_TYPE_EQUAL,
        'store_type'      => Search::FILTER_TYPE_LIKE,

        'city.region_id' => Search::FILTER_TYPE_EQUAL,
    ];

    protected array $combinedFilterable = [
        'common' => [
            'type'   => Search::COMBINED_TYPE_OR,
            'fields' => [
                'name'   => Search::FILTER_TYPE_LIKE,
                'number' => Search::FILTER_TYPE_LIKE,
            ],
        ],
    ];

    protected array $sortable = [
        'id'              => Search::SORT_TYPE_SIMPLE,
        'agent_id'        => Search::SORT_TYPE_SIMPLE,
        'city_id'         => Search::SORT_TYPE_SIMPLE,
        'company_id'      => Search::SORT_TYPE_SIMPLE,
        'name'            => Search::SORT_TYPE_SIMPLE,
        'address'         => Search::SORT_TYPE_SIMPLE,
        'has_credit_line' => Search::SORT_TYPE_SIMPLE,
        'number'          => Search::SORT_TYPE_SIMPLE,
        'is_active'       => Search::SORT_TYPE_SIMPLE,
    ];

    public function filter(array $params, string $combinedType, ?Builder $subQuery = null): self
    {

        parent::filter($params, $combinedType, $subQuery);

        if (isset($params['store_type'])) {
            if (!empty($params['store_type']) || $params['store_type'] !== "undefined") {
                $this->queryBuilder->where('store_type', $params['store_type']);
            }
        }
        return $this;
    }
}
