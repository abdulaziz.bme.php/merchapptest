<?php

namespace Modules\Shop\Search;

use App\Search\Search;

class ShopContactSearch extends Search
{
    protected array $relations = [
        'creator',
    ];

    protected array $filterable = [
        'id' => Search::FILTER_TYPE_IN,
        'creator_id' => Search::FILTER_TYPE_EQUAL,
        'full_name' => Search::FILTER_TYPE_LIKE,
        'position' => Search::FILTER_TYPE_LIKE,
        'phone' => Search::FILTER_TYPE_LIKE,
        'is_active' => Search::FILTER_TYPE_EQUAL,
    ];

    protected array $combinedFilterable = [
        'common' => [
            'type' => Search::COMBINED_TYPE_OR,
            'fields' => [
                'full_name' => Search::FILTER_TYPE_LIKE,
                'phone' => Search::FILTER_TYPE_LIKE,
            ],
        ],
        'common_admin' => [
            'type' => Search::COMBINED_TYPE_OR,
            'fields' => [
                'full_name' => Search::FILTER_TYPE_LIKE,
                'position' => Search::FILTER_TYPE_LIKE,
                'phone' => Search::FILTER_TYPE_LIKE,
            ],
        ],
    ];

    protected array $sortable = [
        'id' => Search::SORT_TYPE_SIMPLE,
        'creator_id' => Search::SORT_TYPE_SIMPLE,
        'full_name' => Search::SORT_TYPE_SIMPLE,
        'position' => Search::SORT_TYPE_SIMPLE,
        'phone' => Search::SORT_TYPE_SIMPLE,
        'is_active' => Search::SORT_TYPE_SIMPLE,
    ];
}
