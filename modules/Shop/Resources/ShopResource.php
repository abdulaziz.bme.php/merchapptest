<?php

namespace Modules\Shop\Resources;

use App\Resources\JsonResource;
use Modules\Product\Resources\ProductBrandResource;
use Modules\Region\Resources\RegionCityResource;
use Modules\Report\Resources\ReportResource;
use Modules\User\Resources\UserResource;

class ShopResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id'                 => $this->id,
            'agent_id'           => $this->agent_id,
            'region_id'          => $this->whenLoaded('city', fn() => $this->city->region_id),
            'city_id'            => $this->city_id,
            'company_id'         => $this->company_id,
            'name'               => $this->name,
            'address'            => $this->address,
            'has_credit_line'    => $this->has_credit_line,
            'location'           => $this->location,
            'number'             => $this->number,
            'focus_code'         => $this->focus_code,
            'is_active'          => $this->is_active,
            'created_at'         => $this->created_at->format('d.m.Y H:i:s'),
            'updated_at'         => $this->updated_at->format('d.m.Y H:i:s'),
            'is_deleted'         => (bool)$this->deleted_at,
            'store_type'         => $this->store_type,
            'samsung_store_name' => $this->samsung_store_name,
            'honor_store_name'   => $this->honor_store_name,
            'organized_retail'   => $this->organized_retail,
            'price_is_required'  => $this->price_is_required,

            'agent'       => UserResource::make($this->whenLoaded('agent')),
            'city'        => RegionCityResource::make($this->whenLoaded('city')),
            'company'     => ShopCompanyResource::make($this->whenLoaded('company')),
            'last_report' => ReportResource::make($this->whenLoaded('last_report')),
            'suppliers'   => ShopSupplierResource::collection($this->whenLoaded('suppliers')),
            'contacts'    => ShopContactResource::collection($this->whenLoaded('contacts')),
            'brands'      => ProductBrandResource::collection($this->whenLoaded('brands')),
        ];
    }
}
