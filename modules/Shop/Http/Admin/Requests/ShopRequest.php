<?php

namespace Modules\Shop\Http\Admin\Requests;

use App\Http\Requests\ActiveFormRequest;
use Modules\Shop\Models\Shop;

use Illuminate\Validation\Rule;
use App\Helpers\FormRequestHelper;
use App\Rules\ExistsSoftDeleteRule;

class ShopRequest extends ActiveFormRequest
{
    public function __construct()
    {
        return parent::__construct(
            model: new Shop()
        );
    }

    public function rules()
    {
        return [
            'agent_id' => [
                'required',
                new ExistsSoftDeleteRule($this->model, 'user', extraQuery: function ($query) {
                    $query->where('role', 'agent');
                }),
            ],
            'city_id' => [
                'required',
                new ExistsSoftDeleteRule($this->model, 'region_city'),
            ],
            'company_id' => [
                'nullable',
                new ExistsSoftDeleteRule($this->model, 'shop_company'),
            ],
            'name' => 'required|string|max:100',
            'store_type' => 'nullable|string|max:255',
            'samsung_store_name' => 'nullable|string|max:255',
            'honor_store_name' => 'nullable|string|max:255',
            'organized_retail' => 'nullable|string|max:255',
            'address' => 'required|string|max:1000',
            'has_credit_line' => 'required|boolean',

            'location' => 'array|size:2',
            'location.*' => 'nullable|numeric',

            'number' => [
                'required',
                Rule::unique($this->model->getTable())->ignore($this->model->id),
            ],
            'focus_code' => 'required|string|max:100',

            'suppliers' => [
                'array',
                new ExistsSoftDeleteRule($this->model, 'shop_supplier'),
            ],
            'contacts' => [
                'array',
                new ExistsSoftDeleteRule($this->model, 'shop_contact'),
            ],
            'brands' => [
                'array',
                new ExistsSoftDeleteRule($this->model, 'product_brand'),
            ],
        ];
    }

    protected function prepareForValidation()
    {
        parent::prepareForValidation();

        $this->merge([
            'location' => array_values((array)$this->location),
        ]);
    }

    protected  function passedValidation()
    {
        parent::passedValidation();

        $this->model->fillableRelations = [
            $this->model::RELATION_TYPE_MANY_MANY => [
                'suppliers' => $this->suppliers,
                'contacts' => $this->contacts,
                'brands' => $this->brands,
            ],
        ];
    }
}
