<?php

namespace Modules\Shop\Http\Admin\Requests;

use App\Http\Requests\ActiveFormRequest;
use Modules\Shop\Models\ShopContact;

use Illuminate\Validation\Rule;
use App\Helpers\FormRequestHelper;

class ShopContactRequest extends ActiveFormRequest
{
    public function __construct()
    {
        return parent::__construct(
            model: new ShopContact()
        );
    }

    public function rules()
    {
        return [
            'full_name' => [
                'required',
                'string',
                'max:100',
                Rule::unique($this->model->getTable())->ignore($this->model->id),
            ],
            'position' => 'nullable|string|max:100',
            'phone' => 'required|string|max:100',
        ];
    }
}
