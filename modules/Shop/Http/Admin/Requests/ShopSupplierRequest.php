<?php

namespace Modules\Shop\Http\Admin\Requests;

use App\Http\Requests\ActiveFormRequest;
use Modules\Shop\Models\ShopSupplier;

use App\Helpers\FormRequestHelper;
use App\Rules\ExistsSoftDeleteRule;
use App\Rules\UniqueRule;

class ShopSupplierRequest extends ActiveFormRequest
{
    public function __construct()
    {
        return parent::__construct(
            model: new ShopSupplier()
        );
    }

    public function rules()
    {
        return [
            'country_id' => [
                'nullable',
                new ExistsSoftDeleteRule($this->model, 'region_country'),
            ],
            'short_name' => [
                'required',
                'string',
                'max:100',
                new UniqueRule($this->model),
            ],
            'full_name' => 'nullable|string|max:100',
        ];
    }
}
