<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_brand', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('report_id')->unsigned()->index();
            $table->bigInteger('brand_id')->unsigned()->index();
            $table->integer('quantity');
            $table->smallInteger('sort_index')->unsigned()->index();
            $table->timestamp('created_at');
            $table->timestamp('updated_at')->index();

            $table->foreign('report_id')->references('id')->on('report')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('brand_id')->references('id')->on('product_brand')->onUpdate('cascade')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_brand');
    }
};
