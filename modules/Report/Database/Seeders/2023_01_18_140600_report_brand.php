<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

return new class extends Seeder
{
    public function run()
    {
        DB::table('report_brand')->insert([
            [
                'report_id' => 3,
                'brand_id' => 1,
                'quantity' => 3,
                'sort_index' => 0,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'report_id' => 3,
                'brand_id' => 2,
                'quantity' => 4,
                'sort_index' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
        ]);
    }
};
