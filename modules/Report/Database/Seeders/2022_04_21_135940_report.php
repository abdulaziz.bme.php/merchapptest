<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

return new class extends Seeder
{
    public function run()
    {
        DB::table('report')->insert([
            [
                'creator_id' => 2,
                'shop_id' => 1,
                'type' => 'ds',
                'date_period_type' => 'week',
                'date_period_from' => date('Y-m-d', strtotime('-2 week')),
                'date_period_to' => date('Y-m-d', strtotime('-1 week')),
                'location' => json_encode([
                    '41.344197',
                    '69.240367',
                ]),
                'images_list' => json_encode([
                    '/test_data/images/1.png',
                    '/test_data/images/2.png',
                    '/test_data/images/3.png',
                ]),
                'comment' => 'Comment 1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'creator_id' => 2,
                'shop_id' => 1,
                'type' => 'ds',
                'date_period_type' => 'week',
                'date_period_from' => date('Y-m-d', strtotime('-1 week')),
                'date_period_to' => date('Y-m-d'),
                'location' => json_encode([
                    '51.339584',
                    '12.346606',
                ]),
                'images_list' => json_encode([]),
                'comment' => null,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'creator_id' => 2,
                'shop_id' => 1,
                'type' => 'sr',
                'date_period_type' => 'month',
                'date_period_from' => date('Y-m-d', strtotime('-1 month')),
                'date_period_to' => date('Y-m-d'),
                'location' => json_encode([
                    '23.451568',
                    '47.798485',
                ]),
                'images_list' => json_encode([
                    '/test_data/images/2.png',
                ]),
                'comment' => 'Comment 3',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
        ]);
    }
};
