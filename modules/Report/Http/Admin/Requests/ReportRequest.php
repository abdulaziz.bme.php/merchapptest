<?php

namespace Modules\Report\Http\Admin\Requests;

use App\Http\Requests\ActiveFormRequest;
use Modules\Report\Models\Report;
use Modules\Report\Enums\ReportEnums;

use Illuminate\Validation\Rule;
use App\Helpers\FormRequestHelper;
use App\Rules\ExistsSoftDeleteRule;
use Illuminate\Support\Arr;

/**
 * @property array products
 * @property array brands
 */
class ReportRequest extends ActiveFormRequest
{
    protected array $ignoredModelFields = ['images_list'];
    protected array $fileFields = [
        'images_list' => 'images',
    ];

    public function __construct()
    {
        return parent::__construct(
            model: new Report()
        );
    }

    public function rules()
    {
        return [
            'creator_id' => [
                'required',
                new ExistsSoftDeleteRule($this->model, 'user'),
            ],
            'shop_id' => [
                'required',
                new ExistsSoftDeleteRule($this->model, 'shop'),
            ],

            'type' => [
                'required',
                Rule::in(array_keys(ReportEnums::types())),
            ],

            'date_period_type' => [
                'required',
                Rule::in(array_keys(ReportEnums::datePeriodTypes())),
            ],
            'date_period_from' => [
                'required',
                'date',
                'date_format:d.m.Y',
//                'before_or_equal:' . date('d.m.Y', strtotime('-6 days')),
            ],

            'location' => 'required|array|size:2',
            'location.*' => 'required|numeric',

            'images_list' => [
                Rule::requiredIf(!$this->model->exists && $this->type == 'ds'),
                'array',
            ],
            'images_list.*' => 'file|max:4096|mimes:jpg,png,webp',
            'comment' => $this->type == 'ds' ? 'required|string' : 'nullable|string',
            'shop_is_closed' => 'required|boolean',

            // DS

            'products' => !$this->shop_is_closed && $this->type == 'ds' ? 'required|array' : 'array',
            'products.*.id' => 'integer',
            'products.*.product_id' => [
                'required',
                new ExistsSoftDeleteRule($this->model, 'product'),
            ],
            'products.*.variation_id' => [
                'required',
                new ExistsSoftDeleteRule($this->model, 'product_variation'),
            ],
            'products.*.supplier_id' => [
                'required',
                new ExistsSoftDeleteRule($this->model, 'shop_supplier'),
            ],
            'products.*.quantity' => 'required|integer|min:1',

            // SR

            'brands' => !$this->shop_is_closed && $this->type == 'sr' ? 'required|array' : 'array',
            'brands.*.id' => 'integer',
            'brands.*.brand_id' => [
                'required',
                new ExistsSoftDeleteRule($this->model, 'product_brand'),
            ],
            'brands.*.quantity' => 'required|integer|min:1',
        ];
    }

    public function withValidator($validator)
    {
        if (!$validator->fails()) {
            $validator->after(function ($validator) {
                switch ($this->type) {
                    case 'ds':
                        $this->products ??= [];

                        $products = array_map(function ($value) {
                            $newValue = [
                                $value['product_id'],
                                $value['variation_id'] ?? null,
                                $value['supplier_id'],
                            ];

                            return implode('.', $newValue);
                        }, $this->products);

                        if (array_unique($products) != $products) {
                            $validator->errors()->add('products', __('rules.report.products.unique_combinations'));
                        }

                        break;

                    case 'sr':
                        $this->brands ??= [];

                        $brands = Arr::pluck($this->brands, 'brand_id');

                        if (array_unique($brands) != $brands) {
                            $validator->errors()->add('brands', __('rules.report.brands.unique_combinations'));
                        }

                        break;
                }
            });
        }
    }

    protected function prepareForValidation()
    {
        parent::prepareForValidation();

        $this->merge([
            'location' => array_values((array)$this->location),
        ]);
    }

    protected  function passedValidation()
    {
        parent::passedValidation();

        $this->model->fillableRelations = [
            $this->model::RELATION_TYPE_ONE_MANY => [
                'products' => $this->type == 'ds' ? $this->products : [],
                'brands' => $this->type == 'sr' ? $this->brands : [],
            ],
        ];
    }
}
