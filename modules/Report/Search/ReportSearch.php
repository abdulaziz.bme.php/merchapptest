<?php

namespace Modules\Report\Search;

use App\Search\Search;
use Illuminate\Database\Eloquent\Builder;

class ReportSearch extends Search
{
    protected array $relations = [
        'creator',
        'shop', 'shop.company', 'shop.city', 'shop.city.region',
        'task_report', 'task_report.task',
        'products', 'products.product', 'products.supplier',
        'brands', 'brands.brand',
        'products.product.brand', 'products.variation', 'products.variation.options',
    ];

    protected array $filterable = [
        'id' => Search::FILTER_TYPE_EQUAL,
        'creator_id' => Search::FILTER_TYPE_EQUAL,
        'shop_id' => Search::FILTER_TYPE_EQUAL,
        'task_report_id' => Search::FILTER_TYPE_EQUAL,
        'type' => Search::FILTER_TYPE_EQUAL,
        'date_period_type' => Search::FILTER_TYPE_EQUAL,
        'shop_is_closed' => Search::FILTER_TYPE_EQUAL,

        'shop.name' => Search::FILTER_TYPE_LIKE,
        'shop.number' => Search::FILTER_TYPE_EQUAL,
        'shop.company_id' => Search::FILTER_TYPE_EQUAL,
        'shop.city_id' => Search::FILTER_TYPE_EQUAL,
        'shop.city.region_id' => Search::FILTER_TYPE_EQUAL,
    ];

    protected array $sortable = [
        'id' => Search::SORT_TYPE_SIMPLE,
        'creator_id' => Search::SORT_TYPE_SIMPLE,
        'shop_id' => Search::SORT_TYPE_SIMPLE,
        'task_report_id' => Search::SORT_TYPE_SIMPLE,
        'type' => Search::SORT_TYPE_SIMPLE,
        'date_period_from' => Search::SORT_TYPE_SIMPLE,
        'date_period_to' => Search::SORT_TYPE_SIMPLE,

        'shop.number' => Search::SORT_TYPE_SIMPLE,
    ];

    public function filter(array $params, string $combinedType, ?Builder $subQuery = null): self
    {
        parent::filter($params, $combinedType, $subQuery);

        if (isset($params['year'])) {
            $this->queryBuilder->where(function ($query) use ($params) {
                $query->whereYear('date_period_from', $params['year'])
                    ->orWhereYear('date_period_to', $params['year']);
            });
        }

        if (isset($params['common'])) {
            $this->queryBuilder->where(function ($query) use ($params) {
                $query->where('id', (int)$params['common'])
                    ->orWhereHas('shop', function ($q) use ($params) {
                        $q->where('name', 'ILIKE', '%' . $params['common'] . '%');
                    });
            });
        }

        if (isset($params['date_period_min'])) {
            $this->queryBuilder->where(function ($query) use ($params) {
                $query->where(
                    'date_period_from',
                    '>=',
                    date('Y-m-d', strtotime($params['date_period_min']))
                )->orWhere(
                    'date_period_to',
                    '>=',
                    date('Y-m-d', strtotime($params['date_period_min']))
                );
            });
        }

        if (isset($params['date_period_max'])) {
            $this->queryBuilder->where(function ($query) use ($params) {
                $query->where(
                    'date_period_from',
                    '<=',
                    date('Y-m-d', strtotime($params['date_period_max']))
                )->orWhere(
                    'date_period_to',
                    '<=',
                    date('Y-m-d', strtotime($params['date_period_max']))
                );
            });
        }

        return $this;
    }
}
