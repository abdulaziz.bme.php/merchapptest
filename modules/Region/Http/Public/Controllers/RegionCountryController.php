<?php

namespace Modules\Region\Http\Public\Controllers;

use App\Http\Controllers\ApiResourceController;

use Modules\Region\Models\RegionCountry;
use Modules\Region\Search\RegionCountrySearch;
use Modules\Region\Resources\RegionCountryResource;

class RegionCountryController extends ApiResourceController
{
    public function __construct()
    {
        return parent::__construct(
            model: new RegionCountry(),
            search: new RegionCountrySearch(),
            resourceClass: RegionCountryResource::class
        );
    }
}
