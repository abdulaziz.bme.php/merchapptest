<?php

namespace Modules\Region\Http\Public\Controllers;

use App\Http\Controllers\ApiResourceController;

use Modules\Region\Models\RegionCity;
use Modules\Region\Search\RegionCitySearch;
use Modules\Region\Resources\RegionCityResource;
use Modules\User\Services\UserService;
use Illuminate\Support\Arr;

class RegionCityController extends ApiResourceController
{
    public function __construct()
    {
        parent::__construct(
            model: new RegionCity(),
            search: new RegionCitySearch(),
            resourceClass: RegionCityResource::class
        );

        $this->middleware(function ($request, $next) {
            $showParams = (array)request()->get('show');
            $showParams = Arr::flatten($showParams);

            if (in_array('only-my', $showParams)) {
                $cities = (new UserService(auth()->user()))->availableCities();
                $this->search->queryBuilder->whereIn('id', $cities);
            }

            return $next($request);
        });
    }
}
