<?php

namespace Modules\External\Http\Public\Middleware;

use Illuminate\Http\Request;

class ExternalAuthenticateMiddleware
{
    public function handle(Request $request, \Closure $next)
    {
        $username = env('AUTHENTICATION_EXTERNAL_USERNAME');
        $password = env('AUTHENTICATION_EXTERNAL_PASSWORD');
        $authKey = 'Basic ' . base64_encode("$username:$password");

        if (request()->header('Authorization') !== $authKey) {
            abort(401, 'Invalid credentials.');
        }

        return $next($request);
    }
}
