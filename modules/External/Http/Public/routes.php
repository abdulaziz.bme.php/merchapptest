<?php

use Illuminate\Support\Facades\Route;

use Modules\External\Http\Public\Middleware\ExternalAuthenticateMiddleware;

use Modules\External\Http\Public\Controllers\SqlController;

Route::prefix('external')
    ->middleware(ExternalAuthenticateMiddleware::class)
    ->group(function () {
        Route::prefix('sql')
            ->group(function () {
                Route::get('execute', [SqlController::class, 'execute']);
            });
    });
