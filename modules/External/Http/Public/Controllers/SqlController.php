<?php

namespace Modules\External\Http\Public\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class SqlController extends Controller
{
    public function execute()
    {
        $sql = (string)request()->get('sql');

        if (!$sql) abort(400, 'No SQL to execute');

        $result = DB::connection('pgsql_only_read')->select($sql);

        return response()->json($result, 200);
    }
}
