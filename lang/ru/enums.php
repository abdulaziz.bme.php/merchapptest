<?php

return [
    'report.type.ds.description' => 'Описание Display Share',
    'report.type.sr.description' => 'Описание Sales Report',
    
    'report.date_period_type.week' => 'Неделя',
    'report.date_period_type.month' => 'Месяц',
    
    'task.agent_status.opened' => 'Открыто',
    'task.agent_status.in_progress' => 'В процессе',
    'task.agent_status.completed' => 'Завершено',
    'task.agent_status.overdue' => 'Просрочено',
    
    'task.admin_status.unchecked' => 'Не проверено',
    'task.admin_status.moderating' => 'На рассмотрении',
    'task.admin_status.accepted' => 'Принято',
    'task.admin_status.declined' => 'Отклонено',
    
    'user.role.admin' => 'Администратор',
    'user.role.agent' => 'Агент',
];
