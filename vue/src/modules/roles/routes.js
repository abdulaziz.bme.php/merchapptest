export default [
  {
    path: 'report',
    component: () => import("@/app/views/EmptyDecorator.vue"),
    children: [
      {
        path: 'report',
        component: () => import("@/app/views/EmptyDecorator.vue"),
        children: [
          {
            path: '',
            component: () => import("./views/report/Index.vue"),
          },
          {
            path: ':id/show',
            component: () => import("./views/report/Show.vue"),
          },
          {
            path: 'create',
            component: () => import("./views/report/Create.vue"),
          },
          {
            path: ':id/update',
            component: () => import("./views/report/Update.vue"),
          },
        ],
      },
    ],
  },
];
