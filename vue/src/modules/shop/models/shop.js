import {CrudModel} from '@/app/core/models/model';

import * as crudEnums from '@/app/enums/crud';
import * as inputEnums from '@/app/enums/input';

export default new CrudModel({
  showDeleted: true,

  list: {
    id: {
      value: 'id',
      type: crudEnums.valueTypes.text,
    },
    name: {
      value: 'name',
      type: crudEnums.valueTypes.text,
    },
    number: {
      value: 'number',
      type: crudEnums.valueTypes.text,
    },
    focus_code: {
      value: 'focus_code',
      type: crudEnums.valueTypes.text,
    },
    agent_id: {
      value: 'agent.username',
      type: crudEnums.valueTypes.text,
    },
    region_id: {
      value: 'city.region.name.:locale',
      type: crudEnums.valueTypes.text,
    },
    city_id: {
      value: 'city.name.:locale',
      type: crudEnums.valueTypes.text,
    },
    store_type: {
      value: 'store_type',
      type: crudEnums.valueTypes.text,
    },
    samsung_store_name: {
      value: 'samsung_store_name',
      type: crudEnums.valueTypes.text,
    },
    honor_store_name: {
      value: 'honor_store_name',
      type: crudEnums.valueTypes.text,
    },
    organized_retail: {
      value: 'organized_retail',
      type: crudEnums.valueTypes.text,
    },
    has_credit_line: {
      value: 'has_credit_line',
      type: crudEnums.valueTypes.boolean,
    },
    is_active: {
      value: 'is_active',
      type: crudEnums.valueTypes.httpSwitcher,
      options: {
        path: 'shop/shop/:id/set-active/:value',
      },
    },
    price_is_required: {
      value: 'price_is_required',
      type: crudEnums.valueTypes.httpSwitcher,
      options: {
        path: 'shop/shop/:id/set-price-required/:value',
      },
    },
    last_report_date_period_interval: {
      value: 'last_report.date_period_interval',
      type: crudEnums.valueTypes.text,
    },
    updated_at: {
      value: 'updated_at',
      type: crudEnums.valueTypes.text,
    },
  },

  filters: {
    common: {
      label: 'Общий поиск',
      hint: (context) => {
        let fields = ['name', 'number'];

        return context.__('Поиск по полям: :fields', {
          fields: fields.map((value) => context.__('fields->' + value)).join(' | '),
        });
      },
      type: inputEnums.types.text,
      wrapperSize: inputEnums.wrapperSizes.xl,
    },
    id: {
      type: inputEnums.types.number,
    },
    focus_code: {
      type: inputEnums.types.text,
    },
    agent_id: {
      type: inputEnums.types.select2Ajax,
      options: {
        path: 'user/user',
        query: {
          'filter[role]': 'agent',
        },
        field: 'username',
      },
    },
    'city.region_id': {
      type: inputEnums.types.select2Ajax,
      options: {
        path: 'region/region',
        field: 'name:locale',
      },
    },
    city_id: {
      type: inputEnums.types.select2Ajax,
      options: {
        path: 'region/city',
        query: () => {
          let regionId = $('[name="filter[city.region_id]"]').val();
          return regionId ? {'filter[region_id]': regionId} : {};
        },
        field: 'name:locale',
      },
    },
    has_credit_line: {
      type: inputEnums.types.select,
      options: {
        items: (context) => {
          return {
            0: context.__('Нет'),
            1: context.__('Да'),
          };
        },
        withPrompt: true,
      },
    },
    is_active: {
      type: inputEnums.types.select,
      options: {
        items: (context) => {
          return {
            0: context.__('Нет'),
            1: context.__('Да'),
          };
        },
        withPrompt: true,
      },
    },
  },

  sortings: ['id', 'name', 'number'],

  show: {
    id: {
      value: 'id',
      type: crudEnums.valueTypes.text,
    },
    name: {
      value: 'name',
      type: crudEnums.valueTypes.text,
    },
    number: {
      value: 'number',
      type: crudEnums.valueTypes.text,
    },
    focus_code: {
      value: 'focus_code',
      type: crudEnums.valueTypes.text,
    },
    address: {
      value: 'address',
      type: crudEnums.valueTypes.text,
    },
    location: {
      value: 'location',
      type: crudEnums.valueTypes.component,
      options: {
        resolve: (context, item) => {
          if (item.value[0] === null || item.value[1] === null) {
            return {
              component: 'div',
              params: [],
            };
          }

          // let link = 'https://yandex.uz/maps/?ll=65.104751%2C42.543778&mode=whatshere&whatshere%5Bpoint%5D=';

          let mapCoors = [parseFloat(item.value[0]) + 0.045, parseFloat(item.value[1]) + 0.009];

          let link = 'https://yandex.uz/maps/?ll=';

          link += mapCoors[1] + '%2C' + mapCoors[0];
          link += '&mode=whatshere&whatshere%5Bpoint%5D=';
          link += item.value[1] + '%2C' + item.value[0];
          link += '&whatshere%5Bzoom%5D=15.4&z=12.73';

          return {
            component: 'a',
            params: [
              {
                href: link,
                target: '_blank',
                innerHTML: context.__('Ссылка'),
              },
            ],
          };
        },
      },
    },
    agent_id: {
      value: 'agent.username',
      type: crudEnums.valueTypes.text,
    },
    company_id: {
      value: 'company.name',
      type: crudEnums.valueTypes.text,
    },
    region_id: {
      value: 'city.region.name.:locale',
      type: crudEnums.valueTypes.text,
    },
    city_id: {
      value: 'city.name.:locale',
      type: crudEnums.valueTypes.text,
    },
    store_type: {
      value: 'store_type',
      type: crudEnums.valueTypes.text,
    },
    samsung_store_name: {
      value: 'samsung_store_name',
      type: crudEnums.valueTypes.text,
    },
    honor_store_name: {
      value: 'honor_store_name',
      type: crudEnums.valueTypes.text,
    },
    organized_retail: {
      value: 'organized_retail',
      type: crudEnums.valueTypes.text,
    },
    suppliers: {
      value: 'suppliers',
      type: crudEnums.valueTypes.array,
      options: {
        path: 'short_name',
      },
    },
    contacts: {
      value: 'contacts',
      type: crudEnums.valueTypes.array,
      options: {
        path: 'full_name',
      },
    },
    brands: {
      value: 'brands',
      type: crudEnums.valueTypes.array,
      options: {
        path: 'name',
      },
    },
    has_credit_line: {
      value: 'has_credit_line',
      type: crudEnums.valueTypes.boolean,
    },
    is_active: {
      value: 'is_active',
      type: crudEnums.valueTypes.boolean,
    },
    last_report_date_period_interval: {
      value: 'last_report.date_period_interval',
      type: crudEnums.valueTypes.text,
    },
    last_report_comment: {
      value: 'last_report.comment',
      type: crudEnums.valueTypes.text,
    },
    created_at: {
      value: 'created_at',
      type: crudEnums.valueTypes.text,
    },
    updated_at: {
      value: 'updated_at',
      type: crudEnums.valueTypes.text,
    },
  },

  form: {
    'Информация': {
      name: {
        type: inputEnums.types.text,
        wrapperSize: inputEnums.wrapperSizes.xl,
      },
      number: {
        type: inputEnums.types.text,
      },
      focus_code: {
        type: inputEnums.types.text,
        initValue: '0',
      },
      address: {
        type: inputEnums.types.textarea,
        wrapperSize: inputEnums.wrapperSizes.xl,
      },
      'location.0': {
        type: inputEnums.types.text,
      },
      'location.1': {
        type: inputEnums.types.text,
      },
      agent_id: {
        type: inputEnums.types.select2Ajax,
        select2Value: 'agent.username',
        options: {
          path: 'user/user',
          query: {
            'filter[role]': 'agent',
          },
          field: 'username',
        },
      },
      company_id: {
        type: inputEnums.types.select2Ajax,
        select2Value: 'company.name',
        options: {
          path: 'shop/company',
          field: 'name',
        },
      },
      region_id: {
        type: inputEnums.types.select2Ajax,
        select2Value: 'city.region.name.:locale',
        options: {
          path: 'region/region',
          field: 'name:locale',
          onChange: ($el) => {
            $('[name*="city_id"]').attr('disabled', $el.val() === '');
          },
        },
      },
      city_id: {
        type: inputEnums.types.select2Ajax,
        select2Value: 'city.name.:locale',
        options: {
          path: 'region/city',
          query: () => {
            let id = $('[name="region_id"]').val();
            return {'filter[region_id]': id};
          },
          field: 'name:locale',
        },
      },
      suppliers: {
        value: (value) => Object.values(value).map((value) => value.id),
        select2Value: 'suppliers',
        type: inputEnums.types.select2Ajax,
        options: {
          path: 'shop/supplier',
          field: 'short_name',
          isMultiple: true,
          select2SubValue: 'short_name',
        },
      },
      contacts: {
        value: (value) => Object.values(value).map((value) => value.id),
        select2Value: 'contacts',
        type: inputEnums.types.select2Ajax,
        options: {
          path: 'shop/contact',
          field: 'full_name',
          isMultiple: true,
          select2SubValue: 'full_name',
        },
      },
      brands: {
        value: (value) => Object.values(value).map((value) => value.id),
        select2Value: 'brands',
        type: inputEnums.types.select2Ajax,
        options: {
          path: 'product/brand',
          field: 'name',
          isMultiple: true,
          select2SubValue: 'name',
        },
      },
      has_credit_line: {
        type: inputEnums.types.switcher,
      },
      store_type: {
        type: inputEnums.types.text,
      },
      samsung_store_name: {
        type: inputEnums.types.text,
      },
      honor_store_name: {
        type: inputEnums.types.text,
      },
      organized_retail: {
        type: inputEnums.types.text,
      },
    },
  },
});
