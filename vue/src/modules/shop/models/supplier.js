import { CrudModel } from '@/app/core/models/model';

import * as crudEnums from '@/app/enums/crud';
import * as inputEnums from '@/app/enums/input';

export default new CrudModel({
    showDeleted: true,

    list: {
        id: {
            value: 'id',
            type: crudEnums.valueTypes.text,
        },
        creator_id: {
            value: 'creator.username',
            type: crudEnums.valueTypes.text,
        },
        short_name: {
            value: 'short_name',
            type: crudEnums.valueTypes.text,
        },
        full_name: {
            value: 'full_name',
            type: crudEnums.valueTypes.text,
        },
        country_id: {
            value: 'country.name.:locale',
            type: crudEnums.valueTypes.text,
        },
        is_active: {
            value: 'is_active',
            type: crudEnums.valueTypes.httpSwitcher,
            options: {
                path: 'shop/supplier/:id/set-active/:value',
            },
        },
        created_at: {
            value: 'created_at',
            type: crudEnums.valueTypes.text,
        },
        updated_at: {
            value: 'updated_at',
            type: crudEnums.valueTypes.text,
        },
    },

    filters: {
        common: {
            label: 'Общий поиск',
            hint: (context) => {
                let fields = ['short_name', 'full_name'];

                return context.__('Поиск по полям: :fields', {
                    fields: fields.map((value) => context.__('fields->' + value)).join(' | '),
                });
            },
            type: inputEnums.types.text,
            wrapperSize: inputEnums.wrapperSizes.xl,
        },
        id: {
            type: inputEnums.types.number,
        },
        creator_id: {
            type: inputEnums.types.select2Ajax,
            options: {
                path: 'user/user',
                field: 'username',
            },
        },
        country_id: {
            type: inputEnums.types.select2Ajax,
            options: {
                path: 'region/country',
                field: 'name:locale',
            },
        },
        is_active: {
            type: inputEnums.types.select,
            options: {
                items: (context) => {
                    return {
                        0: context.__('Нет'),
                        1: context.__('Да'),
                    };
                },
                withPrompt: true,
            },
        },
    },

    sortings: [
        'id',
        'short_name',
        'full_name',
    ],

    show: {
        id: {
            value: 'id',
            type: crudEnums.valueTypes.text,
        },
        creator_id: {
            value: 'creator.username',
            type: crudEnums.valueTypes.text,
        },
        short_name: {
            value: 'short_name',
            type: crudEnums.valueTypes.text,
        },
        full_name: {
            value: 'full_name',
            type: crudEnums.valueTypes.text,
        },
        country_id: {
            value: 'country.name.:locale',
            type: crudEnums.valueTypes.text,
        },
        is_active: {
            value: 'is_active',
            type: crudEnums.valueTypes.boolean,
        },
        created_at: {
            value: 'created_at',
            type: crudEnums.valueTypes.text,
        },
        updated_at: {
            value: 'updated_at',
            type: crudEnums.valueTypes.text,
        },
    },

    form: {
        'Информация': {
            short_name: {
                type: inputEnums.types.text,
            },
            full_name: {
                type: inputEnums.types.text,
            },
            country_id: {
                type: inputEnums.types.select2Ajax,
                select2Value: 'country.name.:locale',
                options: {
                    path: 'region/country',
                    field: 'name:locale',
                },
            },
        },
    },
});