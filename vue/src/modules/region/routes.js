export default [
    {
        path: 'region',
        component: () => import("@/app/views/EmptyDecorator.vue"),
        children: [
            {
                path: 'country',
                component: () => import("@/app/views/EmptyDecorator.vue"),
                children: [
                    {
                        path: '',
                        component: () => import("./views/country/Index.vue"),
                    },
                    {
                        path: ':id/show',
                        component: () => import("./views/country/Show.vue"),
                    },
                    {
                        path: 'create',
                        component: () => import("./views/country/Create.vue"),
                    },
                    {
                        path: ':id/update',
                        component: () => import("./views/country/Update.vue"),
                    },
                ],
            },
            {
                path: 'region',
                component: () => import("@/app/views/EmptyDecorator.vue"),
                children: [
                    {
                        path: '',
                        component: () => import("./views/region/Index.vue"),
                    },
                    {
                        path: ':id/show',
                        component: () => import("./views/region/Show.vue"),
                    },
                    {
                        path: 'create',
                        component: () => import("./views/region/Create.vue"),
                    },
                    {
                        path: ':id/update',
                        component: () => import("./views/region/Update.vue"),
                    },
                ],
            },
            {
                path: 'city',
                component: () => import("@/app/views/EmptyDecorator.vue"),
                children: [
                    {
                        path: '',
                        component: () => import("./views/city/Index.vue"),
                    },
                    {
                        path: ':id/show',
                        component: () => import("./views/city/Show.vue"),
                    },
                    {
                        path: 'create',
                        component: () => import("./views/city/Create.vue"),
                    },
                    {
                        path: ':id/update',
                        component: () => import("./views/city/Update.vue"),
                    },
                ],
            },
        ],
    },
];
