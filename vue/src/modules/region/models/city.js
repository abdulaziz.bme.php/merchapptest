import { CrudModel } from '@/app/core/models/model';

import * as crudEnums from '@/app/enums/crud';
import * as inputEnums from '@/app/enums/input';

export default new CrudModel({
    showDeleted: true,

    list: {
        id: {
            value: 'id',
            type: crudEnums.valueTypes.text,
        },
        name: {
            value: 'name.:locale',
            type: crudEnums.valueTypes.text,
        },
        region_id: {
            value: 'region.name.:locale',
            type: crudEnums.valueTypes.text,
        },
        created_at: {
            value: 'created_at',
            type: crudEnums.valueTypes.text,
        },
        updated_at: {
            value: 'updated_at',
            type: crudEnums.valueTypes.text,
        },
    },

    filters: {
        id: {
            type: inputEnums.types.number,
        },
        name: {
            type: inputEnums.types.text,
        },
        region_id: {
            type: inputEnums.types.select2Ajax,
            options: {
                path: 'region/region',
                field: 'name:locale',
            },
        },
    },

    sortings: [
        'id',
        'name',
    ],

    show: {
        id: {
            value: 'id',
            type: crudEnums.valueTypes.text,
        },
        name: {
            value: 'name.:locale',
            type: crudEnums.valueTypes.text,
        },
        region_id: {
            value: 'region.name.:locale',
            type: crudEnums.valueTypes.text,
        },
        created_at: {
            value: 'created_at',
            type: crudEnums.valueTypes.text,
        },
        updated_at: {
            value: 'updated_at',
            type: crudEnums.valueTypes.text,
        },
    },

    form: {
        'Информация': {
            name: {
                type: inputEnums.types.text,
                options: {
                    isLocalized: true,
                },
                wrapperSize: inputEnums.wrapperSizes.xl,
            },
            region_id: {
                type: inputEnums.types.select2Ajax,
                select2Value: 'region.name.:locale',
                options: {
                    path: 'region/region',
                    field: 'name:locale',
                },
            },
        },
    },
});