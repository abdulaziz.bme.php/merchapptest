import { CrudModel } from '@/app/core/models/model';

import * as crudEnums from '@/app/enums/crud';
import * as inputEnums from '@/app/enums/input';

export default new CrudModel({
    form: {
        'Информация': {
            username: {
                type: inputEnums.types.text,
            },
            email: {
                type: inputEnums.types.text,
            },
            full_name: {
                label: 'user.full_name',
                type: inputEnums.types.text,
            },
            phone: {
                type: inputEnums.types.phone,
            },
            new_password: {
                type: inputEnums.types.password,
            },
        },
    },
});