export default [
    {
        path: 'exchange',
        component: () => import("@/app/views/EmptyDecorator.vue"),
        children: [
            {
                path: 'excel',
                component: () => import("@/app/views/EmptyDecorator.vue"),
                children: [
                    {
                        path: 'import',
                        component: () => import("./views/excel/Import.vue"),
                    },
                    {
                        path: 'export',
                        component: () => import("./views/excel/Export.vue"),
                    },
                ],
            },
        ],
    },
];
