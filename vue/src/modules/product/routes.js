export default [
    {
        path: 'product',
        component: () => import("@/app/views/EmptyDecorator.vue"),
        children: [
            {
                path: 'product',
                component: () => import("@/app/views/EmptyDecorator.vue"),
                children: [
                    {
                        path: '',
                        component: () => import("./views/product/Index.vue"),
                    },
                    {
                        path: ':id/show',
                        component: () => import("./views/product/Show.vue"),
                    },
                    {
                        path: 'create',
                        component: () => import("./views/product/Create.vue"),
                    },
                    {
                        path: ':id/update',
                        component: () => import("./views/product/Update.vue"),
                    },
                ],
            },
            {
                path: 'category',
                component: () => import("@/app/views/EmptyDecorator.vue"),
                children: [
                    {
                        path: '',
                        component: () => import("./views/category/Index.vue"),
                    },
                    {
                        path: ':id/show',
                        component: () => import("./views/category/Show.vue"),
                    },
                    {
                        path: 'create',
                        component: () => import("./views/category/Create.vue"),
                    },
                    {
                        path: ':id/update',
                        component: () => import("./views/category/Update.vue"),
                    },
                ],
            },
            {
                path: 'brand',
                component: () => import("@/app/views/EmptyDecorator.vue"),
                children: [
                    {
                        path: '',
                        component: () => import("./views/brand/Index.vue"),
                    },
                    {
                        path: ':id/show',
                        component: () => import("./views/brand/Show.vue"),
                    },
                    {
                        path: 'create',
                        component: () => import("./views/brand/Create.vue"),
                    },
                    {
                        path: ':id/update',
                        component: () => import("./views/brand/Update.vue"),
                    },
                ],
            },
            {
                path: 'specification',
                component: () => import("@/app/views/EmptyDecorator.vue"),
                children: [
                    {
                        path: '',
                        component: () => import("./views/specification/Index.vue"),
                    },
                    {
                        path: ':id/show',
                        component: () => import("./views/specification/Show.vue"),
                    },
                    {
                        path: 'create',
                        component: () => import("./views/specification/Create.vue"),
                    },
                    {
                        path: ':id/update',
                        component: () => import("./views/specification/Update.vue"),
                    },
                ],
            },
            {
                path: 'variation/:product_id',
                component: () => import("./views/variation/_ProductDecorator.vue"),
                children: [
                    {
                        path: '',
                        component: () => import("./views/variation/Index.vue"),
                    },
                    {
                        path: ':id/show',
                        component: () => import("./views/variation/Show.vue"),
                    },
                    {
                        path: 'create',
                        component: () => import("./views/variation/Create.vue"),
                    },
                    {
                        path: ':id/update',
                        component: () => import("./views/variation/Update.vue"),
                    },
                ],
            },
        ],
    },
];
