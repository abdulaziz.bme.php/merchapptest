import {CrudModel} from '@/app/core/models/model';

import * as crudEnums from '@/app/enums/crud';
import * as inputEnums from '@/app/enums/input';

export default new CrudModel({
  showDeleted: true,

  list: {
    images_list: {
      value: 'images_list.0.w_100',
      type: crudEnums.valueTypes.image,
      options: {
        hideLabel: true,
      },
    },
    id: {
      value: 'id',
      type: crudEnums.valueTypes.text,
    },
    creator_id: {
      value: 'creator.username',
      type: crudEnums.valueTypes.text,
    },
    model_number: {
      value: 'model_number',
      type: crudEnums.valueTypes.text,
    },
    date_eol: {
      value: 'date_eol',
      type: crudEnums.valueTypes.text,
    },
    extra_id: {
      value: 'extra_id',
      type: crudEnums.valueTypes.text,
    },
    category_id: {
      value: 'category.name.:locale',
      type: crudEnums.valueTypes.text,
    },
    brand_id: {
      value: 'brand.name',
      type: crudEnums.valueTypes.text,
    },
    is_active: {
      value: 'is_active',
      type: crudEnums.valueTypes.httpSwitcher,
      options: {
        path: 'product/product/:id/set-active/:value',
      },
    },
    updated_at: {
      value: 'updated_at',
      type: crudEnums.valueTypes.text,
    },
  },

  filters: {
    common: {
      label: 'Общий поиск',
      hint: (context) => {
        let fields = ['model_number', 'brand_id'];

        return context.__('Поиск по полям: :fields', {
          fields: fields.map((value) => context.__('fields->' + value)).join(' | '),
        });
      },
      type: inputEnums.types.text,
      wrapperSize: inputEnums.wrapperSizes.xl,
    },
    id: {
      type: inputEnums.types.number,
    },
    creator_id: {
      type: inputEnums.types.select2Ajax,
      options: {
        path: 'user/user',
        field: 'username',
      },
    },
    date_eol: {
      type: inputEnums.types.date,
    },
    category_id: {
      type: inputEnums.types.select2Ajax,
      options: {
        path: 'product/category',
        field: 'name:locale',
      },
    },
    brand_id: {
      type: inputEnums.types.select2Ajax,
      options: {
        path: 'product/brand',
        field: 'name',
      },
    },
    is_active: {
      type: inputEnums.types.select,
      options: {
        items: (context) => {
          return {
            0: context.__('Нет'),
            1: context.__('Да'),
          };
        },
        withPrompt: true,
      },
    },
  },

  sortings: [
    'id',
    'model_number',
    'date_eol',
  ],

  show: {
    id: {
      value: 'id',
      type: crudEnums.valueTypes.text,
    },
    creator_id: {
      value: 'creator.username',
      type: crudEnums.valueTypes.text,
    },
    model_number: {
      value: 'model_number',
      type: crudEnums.valueTypes.text,
    },
    date_eol: {
      value: 'date_eol',
      type: crudEnums.valueTypes.text,
    },
    category_id: {
      value: 'category.name.:locale',
      type: crudEnums.valueTypes.text,
    },
    brand_id: {
      value: 'brand.name',
      type: crudEnums.valueTypes.text,
    },
    extra_id: {
      value: 'extra_id',
      type: crudEnums.valueTypes.text,
    },
    is_active: {
      value: 'is_active',
      type: crudEnums.valueTypes.boolean,
    },
    created_at: {
      value: 'created_at',
      type: crudEnums.valueTypes.text,
    },
    updated_at: {
      value: 'updated_at',
      type: crudEnums.valueTypes.text,
    },
    images_list: {
      value: 'images_list',
      type: crudEnums.valueTypes.image,
      options: {
        isMultiple: true,
        path: 'w_500',
      },
    },
  },

  form: {
    'Информация': {
      model_number: {
        type: inputEnums.types.text,
      },
      extra_id: {
        type: inputEnums.types.text,
      },
      date_eol: {
        type: inputEnums.types.date,
      },
      category_id: {
        type: inputEnums.types.select2Ajax,
        select2Value: 'category.name.:locale',
        options: {
          path: 'product/category',
          field: 'name:locale',
        },
      },
      brand_id: {
        type: inputEnums.types.select2Ajax,
        select2Value: 'brand.name',
        options: {
          path: 'product/brand',
          field: 'name',
        },
      },
      images_list: {
        type: inputEnums.types.file,
        options: {
          previewPath: 'w_500',
          downloadPath: 'original',
          deletePath: 'product/product/:id/delete-file/images_list/:index',
          isMultiple: true,
        },
        wrapperSize: inputEnums.wrapperSizes.xl,
      },
    },
  },
});
