import { CrudModel } from '@/app/core/models/model';

import * as crudEnums from '@/app/enums/crud';
import * as inputEnums from '@/app/enums/input';

export default new CrudModel({
    showDeleted: true,

    list: {
        id: {
            value: 'id',
            type: crudEnums.valueTypes.text,
        },
        name: {
            value: 'name.:locale',
            type: crudEnums.valueTypes.text,
        },
        specifications: {
            value: 'specifications',
            type: crudEnums.valueTypes.array,
            options: {
                path: 'name.:locale',
            },
        },
        created_at: {
            value: 'created_at',
            type: crudEnums.valueTypes.text,
        },
        updated_at: {
            value: 'updated_at',
            type: crudEnums.valueTypes.text,
        },
    },

    filters: {
        id: {
            type: inputEnums.types.number,
        },
        name: {
            type: inputEnums.types.text,
        },
    },

    sortings: [
        'id',
        'name',
    ],

    show: {
        id: {
            value: 'id',
            type: crudEnums.valueTypes.text,
        },
        name: {
            value: 'name.:locale',
            type: crudEnums.valueTypes.text,
        },
        specifications: {
            value: 'specifications',
            type: crudEnums.valueTypes.array,
            options: {
                path: 'name.:locale',
            },
        },
        created_at: {
            value: 'created_at',
            type: crudEnums.valueTypes.text,
        },
        updated_at: {
            value: 'updated_at',
            type: crudEnums.valueTypes.text,
        },
    },

    form: {
        'Информация': {
            name: {
                type: inputEnums.types.text,
                options: {
                    isLocalized: true,
                },
                wrapperSize: inputEnums.wrapperSizes.xl,
            },
            specifications: {
                value: (value) => Object.values(value).map((value) => value.id),
                select2Value: 'specifications',
                type: inputEnums.types.select2Ajax,
                options: {
                    path: 'product/specification',
                    field: 'name:locale',
                    isMultiple: true,
                    select2SubValue: 'name.:locale',
                },
            },
        },
    },
});