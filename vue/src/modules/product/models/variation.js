import { CrudModel } from '@/app/core/models/model';

import * as crudEnums from '@/app/enums/crud';
import * as inputEnums from '@/app/enums/input';

import VariationOptions from '@/modules/product/components/VariationOptions.vue';

export default new CrudModel({
    showDeleted: true,

    list: {
        images_list: {
            value: 'images_list.0.w_100',
            type: crudEnums.valueTypes.image,
            options: {
                hideLabel: true,
            },
        },
        id: {
            value: 'id',
            type: crudEnums.valueTypes.text,
        },
        name: {
            value: 'name',
            type: crudEnums.valueTypes.text,
        },
        sku: {
            value: 'sku',
            type: crudEnums.valueTypes.text,
        },
        created_at: {
            value: 'created_at',
            type: crudEnums.valueTypes.text,
        },
        updated_at: {
            value: 'updated_at',
            type: crudEnums.valueTypes.text,
        },
    },

    filters: {
        common: {
            label: 'Общий поиск',
            hint: (context) => {
                let fields = ['options.name', 'sku'];

                return context.__('Поиск по полям: :fields', {
                    fields: fields.map((value) => context.__('fields->' + value)).join(' | '),
                });
            },
            type: inputEnums.types.text,
            wrapperSize: inputEnums.wrapperSizes.xl,
        },
        id: {
            type: inputEnums.types.number,
        },
    },

    sortings: [
        'id',
    ],

    show: {
        id: {
            value: 'id',
            type: crudEnums.valueTypes.text,
        },
        name: {
            value: 'name',
            type: crudEnums.valueTypes.text,
        },
        sku: {
            value: 'sku',
            type: crudEnums.valueTypes.text,
        },
        created_at: {
            value: 'created_at',
            type: crudEnums.valueTypes.text,
        },
        updated_at: {
            value: 'updated_at',
            type: crudEnums.valueTypes.text,
        },
        images_list: {
            value: 'images_list',
            type: crudEnums.valueTypes.image,
            options: {
                isMultiple: true,
                path: 'w_500',
            },
        },
    },

    form: {
        'Информация': {
            product_id: {
                initValue: (context) => context.$route.params.product_id,
                type: inputEnums.types.hidden,
            },
            sku: {
                type: inputEnums.types.text,
            },
            images_list: {
                type: inputEnums.types.file,
                options: {
                    previewPath: 'w_500',
                    downloadPath: 'original',
                    deletePath: 'product/variation/:id/delete-file/images_list/:index',
                    isMultiple: true,
                },
                wrapperSize: inputEnums.wrapperSizes.xl,
            },
        },
        'Опции': {
            options: {
                value: (value) => Object.values(value).map((value) => value.id),
                type: inputEnums.types.component,
                options: {
                    resolve: (context, item) => {
                        return {
                            component: VariationOptions,
                            params: [
                                {
                                    value: item.value,
                                }
                            ],
                        };
                    },
                },
            },
        },
    },
});