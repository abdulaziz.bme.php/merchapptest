import { CrudModel } from '@/app/core/models/model';

import * as crudEnums from '@/app/enums/crud';
import * as inputEnums from '@/app/enums/input';

export default new CrudModel({
    showDeleted: true,

    list: {
        id: {
            value: 'id',
            type: crudEnums.valueTypes.text,
        },
        name: {
            value: 'name.:locale',
            type: crudEnums.valueTypes.text,
        },
        options: {
            value: 'options',
            type: crudEnums.valueTypes.array,
            options: {
                path: 'name.:locale',
            },
        },
        created_at: {
            value: 'created_at',
            type: crudEnums.valueTypes.text,
        },
        updated_at: {
            value: 'updated_at',
            type: crudEnums.valueTypes.text,
        },
    },

    filters: {
        id: {
            type: inputEnums.types.number,
        },
        name: {
            type: inputEnums.types.text,
        },
    },

    sortings: [
        'id',
        'name',
    ],

    show: {
        id: {
            value: 'id',
            type: crudEnums.valueTypes.text,
        },
        name: {
            value: 'name.:locale',
            type: crudEnums.valueTypes.text,
        },
        options: {
            value: 'options',
            type: crudEnums.valueTypes.relations,
            options: {
                fields: {
                    name: {
                        value: 'name.:locale',
                        type: crudEnums.valueTypes.text,
                    },
                },
            },
        },
        created_at: {
            value: 'created_at',
            type: crudEnums.valueTypes.text,
        },
        updated_at: {
            value: 'updated_at',
            type: crudEnums.valueTypes.text,
        },
    },

    form: {
        'Информация': {
            name: {
                type: inputEnums.types.text,
                options: {
                    isLocalized: true,
                },
                wrapperSize: inputEnums.wrapperSizes.xl,
            },
        },
        'Опции': {
            options: {
                type: inputEnums.types.relations,
                options: {
                    fields: {
                        name: {
                            type: inputEnums.types.text,
                            options: {
                                isLocalized: true,
                            },
                            wrapperSize: inputEnums.wrapperSizes.xl,
                        },
                    },
                },
            },
        },
    },
});