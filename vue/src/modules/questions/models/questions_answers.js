import {CrudModel} from '@/app/core/models/model';

import * as crudEnums from '@/app/enums/crud';
import * as inputEnums from '@/app/enums/input';
import RouterLink from "@/app/components/vue/RouterLink.vue";

export default new CrudModel({
  showDeleted: true,

  list: {
    id: {
      value: 'id',
      type: crudEnums.valueTypes.text,
    },
    shop_id: {
      value: 'shop',
      type: crudEnums.valueTypes.component,
      options: {
        resolve: (context, item) => {
          return {
            component: RouterLink,
            params: [
              {
                to: 'shop/shop/' + item.value.id + '/show',
                target: '_blank',
              },
              () => item.value.name + ' / ' + item.value.number,
            ],
          };
        },
      },
    },
    created_at: {
      value: 'created_at',
      type: crudEnums.valueTypes.text,
    },
  },

  filters: {
    id: {
      type: inputEnums.types.number,
    },
    title: {
      type: inputEnums.types.text,
    },
    shop_id: {
      type: inputEnums.types.select2Ajax,
      options: {
        path: 'shop/shop',
        field: 'number',
      },
    },
    is_active: {
      type: inputEnums.types.select,
      options: {
        items: (context) => {
          return {
            0: context.__('Нет'),
            1: context.__('Да'),
          };
        },
        withPrompt: true,
      },
    },
  },

  sortings: ['id', 'title', 'is_active'],

  show: {
    id: {
      value: 'id',
      type: crudEnums.valueTypes.text,
    },
    shop_id: {
      value: 'shop',
      type: crudEnums.valueTypes.component,
      options: {
        resolve: (context, item) => {
          return {
            component: RouterLink,
            params: [
              {
                to: 'shop/shop/' + item.value.id + '/show',
                target: '_blank',
              },
              () => item.value.name + ' / ' + item.value.number,
            ],
          };
        },
      },
    },
    answers: {
      value: 'answers',
      type: crudEnums.valueTypes.relations,
      options: {
        fields: {
          id: {
            value: 'id',
            type: crudEnums.valueTypes.text,
          },
          question: {
            value: 'question',
            type: crudEnums.valueTypes.text,
          },
          answer: {
            value: 'answer',
            type: crudEnums.valueTypes.text,
            options: {
              onComplete: (context, value) => {
                if (value === 'yes' || value === 'no')
                  return value === 'yes' ? context.__("Да") : context.__("Нет")
                return value;
              },
            },
          },
        },
      },
    },
  }
});
