import {CrudModel} from '@/app/core/models/model';

import * as crudEnums from '@/app/enums/crud';
import * as inputEnums from '@/app/enums/input';

export default new CrudModel({
  showDeleted: true,

  list: {
    id: {
      value: 'id',
      type: crudEnums.valueTypes.text,
    },
    title: {
      value: 'title',
      type: crudEnums.valueTypes.text,
    },
    title_en: {
      value: 'title_en',
      type: crudEnums.valueTypes.text,
    },
    brand_id: {
      value: 'brand.name',
      type: crudEnums.valueTypes.text,
    },
    is_active: {
      value: 'is_active',
      type: crudEnums.valueTypes.boolean,
    },
    created_at: {
      value: 'created_at',
      type: crudEnums.valueTypes.text,
    },
  },

  filters: {
    id: {
      type: inputEnums.types.number,
    },
    title: {
      type: inputEnums.types.text,
    },
    title_en: {
      type: inputEnums.types.text,
    },
    brand_id: {
      type: inputEnums.types.select2Ajax,
      options: {
        path: 'product/brand',
        field: 'name',
      },
    },
    is_active: {
      type: inputEnums.types.select,
      options: {
        items: (context) => {
          return {
            0: context.__('Нет'),
            1: context.__('Да'),
          };
        },
        withPrompt: true,
      },
    },
  },

  sortings: ['id', 'title', 'title_en', 'is_active'],

  show: {
    id: {
      value: 'id',
      type: crudEnums.valueTypes.text,
    },
    title: {
      value: 'title',
      type: crudEnums.valueTypes.text,
    },
    title_en: {
      value: 'title_en',
      type: crudEnums.valueTypes.text,
    },
    brand_id: {
      value: 'brand.name',
      type: crudEnums.valueTypes.text,
    },
    questions: {
      value: 'questions',
      type: crudEnums.valueTypes.relations,
      options: {
        fields: {
          id: {
            value: 'id',
            type: crudEnums.valueTypes.text,
          },
          question: {
            value: 'question',
            type: crudEnums.valueTypes.text,
          },
        },
      },
    },
  },

  form: {
    'Информация': {
      title: {
        type: inputEnums.types.text,
      },
      title_en: {
        type: inputEnums.types.text,
      },
      brand_id: {
        type: inputEnums.types.select2Ajax,
        select2Value: 'brand.name',
        options: {
          path: 'product/brand',
          field: 'name',
        },
      },
      is_active: {
        type: inputEnums.types.switcher,
        wrapperSize: inputEnums.wrapperSizes.xs,
      },
      reusable: {
        wrapperSize: inputEnums.wrapperSizes.xs,
        type: inputEnums.types.switcher,
      },
      for_selected_shops: {
        type: inputEnums.types.switcher,
        wrapperSize: inputEnums.wrapperSizes.xs,
      },
      shop_type: {
        type: inputEnums.types.select,
        options: {
          required: true,
          items: (context) => {
            return {
              'Organized Retail': context.__('Организованный ритейл'),
              'Independent Retail': context.__('Независимый ритейл'),
            };
          },
        },
      },
      shops: {
        value: (value) => Object.values(value).map((value) => value.id),
        type: inputEnums.types.select2Ajax,
        select2Value: 'shop.id',
        options: {
          query: () => {
            let storeType = $('[name="shop_type"]').val();
            return storeType ? {'filter[shop_type]': storeType} : {};
          },
          isMultiple: true,
          path: 'shop/search',
          field: 'number',
        },
      },
    },
    'Вопросы': {
      questions: {
        type: inputEnums.types.relations,
        options: {
          fields: {
            question: {
              type: inputEnums.types.text,
            },
            question_en: {
              type: inputEnums.types.text,
            },
            variants: {
              type: inputEnums.types.relations,
              options: {
                fields: {
                  title: {
                    type: inputEnums.types.text,
                  },
                  title_en: {
                    type: inputEnums.types.text,
                  },
                  variant_type: {
                    type: inputEnums.types.select,
                    options: {
                      required: true,
                      items: (context) => {
                        return {
                          'N': context.__('Нет'),
                          'Y': context.__('Да'),
                        };
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    },
  },
  formInit: () => {
    if ($('#crud-form input').length === 0) return;

    let $selectedShops = $('#crud-form [name="for_selected_shops"][type="checkbox"]'),
      $storeType = $('#crud-form [name="shop_type"]'),
      $shop_id = $('#crud-form [name="shops[]"]');

    function toggleVisible(state) {
      if (state) {
        $storeType.closest('.input-wrapper').removeClass('d-none');
        $shop_id.closest('.input-wrapper').removeClass('d-none');
      } else {
        $storeType.closest('.input-wrapper').addClass('d-none');
        $shop_id.closest('.input-wrapper').addClass('d-none');
      }
    }

    $selectedShops.on('switchChange.bootstrapSwitch', function (event, state) {
      toggleVisible(state);
    });
    toggleVisible($selectedShops.bootstrapSwitch('state'));

    $selectedShops.trigger('change.bootstrapSwitch');
  },
});
