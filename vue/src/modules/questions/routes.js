export default [
  {
    path: 'questions',
    component: () => import("@/app/views/EmptyDecorator.vue"),
    children: [
      {
        path: 'groups',
        component: () => import("@/app/views/EmptyDecorator.vue"),
        children: [
          {
            path: '',
            component: () => import("./views/questions/Index.vue"),
          },
          {
            path: ':id/show',
            component: () => import("./views/questions/Show.vue"),
          },
          {
            path: 'create',
            component: () => import("./views/questions/Create.vue"),
          },
          {
            path: ':id/update',
            component: () => import("./views/questions/Update.vue"),
          },
        ],
      }, {
        path: 'answers',
        component: () => import("@/app/views/EmptyDecorator.vue"),
        children: [
          {
            path: '',
            component: () => import("./views/question_answers/Index.vue"),
          },
          {
            path: ':id/show',
            component: () => import("./views/question_answers/Show.vue"),
          },
          {
            path: 'create',
            component: () => import("./views/question_answers/Create.vue"),
          },
          {
            path: ':id/update',
            component: () => import("./views/question_answers/Update.vue"),
          },
        ],
      },
    ],
  },
];
