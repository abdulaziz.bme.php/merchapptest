export default [
  {
    path: 'companies',
    component: () => import("@/app/views/EmptyDecorator.vue"),
    children: [
      {
        path: '',
        component: () => import("./views/company/Index.vue"),
      },
      {
        path: ':id/show',
        component: () => import("./views/company/Show.vue"),
      },
      {
        path: 'create',
        component: () => import("./views/company/Create.vue"),
      },
      {
        path: ':id/update',
        component: () => import("./views/company/Update.vue"),
      },
    ],
  },
];
