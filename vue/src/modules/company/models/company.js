import {CrudModel} from '@/app/core/models/model';

import * as crudEnums from '@/app/enums/crud';
import * as inputEnums from '@/app/enums/input';

export default new CrudModel({
  showDeleted: false,

  list: {
    id: {
      value: 'id',
      type: crudEnums.valueTypes.text,
    },
    company_name: {
      value: 'company_name',
      type: crudEnums.valueTypes.text,
    },
    is_active: {
      value: 'is_active',
      type: crudEnums.valueTypes.boolean,
    },
    created_at: {
      value: 'created_at',
      type: crudEnums.valueTypes.text,
    },
  },

  filters: {
    id: {
      type: inputEnums.types.number,
    },
    company_name: {
      type: inputEnums.types.text,
    },
    is_active: {
      type: inputEnums.types.select,
      options: {
        items: (context) => {
          return {
            0: context.__('Нет'),
            1: context.__('Да'),
          };
        },
        withPrompt: true,
      },
    },
  },

  sortings: ['id', 'company_name', 'is_active'],

  show: {
    id: {
      value: 'id',
      type: crudEnums.valueTypes.text,
    },
    company_name: {
      value: 'company_name',
      type: crudEnums.valueTypes.text,
    },
    is_active: {
      value: 'is_active',
      type: crudEnums.valueTypes.boolean,
    },
  },

  form: {
    'Информация': {
      company_name: {
        type: inputEnums.types.text,
      },
      is_active: {
        type: inputEnums.types.switcher,
      },
    },
  }
});
