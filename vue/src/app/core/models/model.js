export class CrudModel {
    showDeleted;
    list;
    filters;
    sortings;
    show;
    form;
    formInit;

    constructor({
        showDeleted,
        list,
        filters,
        sortings,
        show,
        form,
        formInit,
    }) {
        this.showDeleted = showDeleted ?? false;
        this.list = list ?? {};
        this.filters = filters ?? {};
        this.sortings = sortings ?? [];
        this.show = show ?? {};
        this.form = form ?? {};
        this.formInit = formInit ?? null;
    }
}