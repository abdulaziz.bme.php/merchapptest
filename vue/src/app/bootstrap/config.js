let authUsername = localStorage.getItem('auth_username') ?? '',
    authPassword = localStorage.getItem('auth_password') ?? '';

export default {
    http: {
        url: {
            common: window.__VUE_OPTIONS_API__ ? 'http://bm-electronics.loc/api' : 'http://46.101.97.100:81/api',
            main: null,
        },
        headers: {
            'Accept': 'application/json',
            'Authorization': 'Basic ' + window.btoa(authUsername + ':' + authPassword),
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': true,
        },
    },
};
