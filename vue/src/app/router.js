import { createRouter, createWebHistory } from "vue-router";
import Wrapper from "@/app/views/_Wrapper.vue";
import NoWrapper from "@/app/views/_NoWrapper.vue";
import Home from "@/app/views/Home.vue";
import Error from "@/app/views/Error.vue";

import authRoutes from "@/modules/auth/routes";
import exchangeRoutes from "@/modules/exchange/routes";
import productRoutes from "@/modules/product/routes";
import regionRoutes from "@/modules/region/routes";
import reportRoutes from "@/modules/report/routes";
import shopRoutes from "@/modules/shop/routes";
import systemRoutes from "@/modules/system/routes";
import taskRoutes from "@/modules/task/routes";
import userRoutes from "@/modules/user/routes";
import questionRoutes from "@/modules/questions/routes";
import companyRoutes from "@/modules/company/routes";

let wrappedRoutes = [
    {
        path: "",
        component: Home,
    },
];

wrappedRoutes = wrappedRoutes.concat(exchangeRoutes);
wrappedRoutes = wrappedRoutes.concat(productRoutes);
wrappedRoutes = wrappedRoutes.concat(regionRoutes);
wrappedRoutes = wrappedRoutes.concat(reportRoutes);
wrappedRoutes = wrappedRoutes.concat(shopRoutes);
wrappedRoutes = wrappedRoutes.concat(systemRoutes);
wrappedRoutes = wrappedRoutes.concat(taskRoutes);
wrappedRoutes = wrappedRoutes.concat(userRoutes);
wrappedRoutes = wrappedRoutes.concat(questionRoutes);
wrappedRoutes = wrappedRoutes.concat(companyRoutes);

let nonWrappedRoutes = authRoutes;

export default createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: "/:locale",
            component: Wrapper,
            children: wrappedRoutes,
        },
        {
            path: "/",
            component: Wrapper,
            children: wrappedRoutes,
        },
        {
            path: "/",
            component: NoWrapper,
            children: nonWrappedRoutes.concat([
                {
                    path: "/:pathMatch(.*)*",
                    component: Error,
                },
            ]),
        },
        {
            path: "/:locale",
            component: NoWrapper,
            children: nonWrappedRoutes,
        },
    ],
});
