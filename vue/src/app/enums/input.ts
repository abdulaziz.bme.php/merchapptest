export enum types {
    text, textarea, password, number,
    date, datetime, time,
    phone,
    hidden,
    select, select2Ajax, httpSelect,
    switcher,
    file,
    relations,
    component,
}

export enum wrapperSizes {
    xs = 'col-sm-6 col-md-4 col-lg-3 col-xl-2',
    sm = 'col-md-6 col-lg-4 col-xl-3',
    md = 'col-lg-6 col-xl-4',
    lg = 'col-xl-6',
    xl = 'col-12',
}
