--
-- PostgreSQL database dump
--

-- Dumped from database version 14.5 (Ubuntu 14.5-1.pgdg20.04+1)
-- Dumped by pg_dump version 14.5 (Ubuntu 14.5-1.pgdg20.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: failed_jobs; Type: TABLE; Schema: public; Owner: pgadmin
--

CREATE TABLE public.failed_jobs (
    id bigint NOT NULL,
    uuid character varying(100) NOT NULL,
    connection text NOT NULL,
    queue text NOT NULL,
    payload text NOT NULL,
    exception text NOT NULL,
    failed_at timestamp(0) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);




--
-- Name: failed_jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: pgadmin
--

CREATE SEQUENCE public.failed_jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;




--
-- Name: failed_jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pgadmin
--

ALTER SEQUENCE public.failed_jobs_id_seq OWNED BY public.failed_jobs.id;


--
-- Name: jobs; Type: TABLE; Schema: public; Owner: pgadmin
--

CREATE TABLE public.jobs (
    id bigint NOT NULL,
    queue character varying(100) NOT NULL,
    payload text NOT NULL,
    attempts smallint NOT NULL,
    reserved_at integer,
    available_at integer NOT NULL,
    created_at integer NOT NULL
);




--
-- Name: jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: pgadmin
--

CREATE SEQUENCE public.jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;




--
-- Name: jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pgadmin
--

ALTER SEQUENCE public.jobs_id_seq OWNED BY public.jobs.id;


--
-- Name: migrations; Type: TABLE; Schema: public; Owner: pgadmin
--

CREATE TABLE public.migrations (
    id integer NOT NULL,
    migration character varying(100) NOT NULL,
    batch integer NOT NULL
);




--
-- Name: migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: pgadmin
--

CREATE SEQUENCE public.migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;




--
-- Name: migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pgadmin
--

ALTER SEQUENCE public.migrations_id_seq OWNED BY public.migrations.id;


--
-- Name: product; Type: TABLE; Schema: public; Owner: pgadmin
--

CREATE TABLE public.product (
    id bigint NOT NULL,
    creator_id bigint NOT NULL,
    category_id bigint NOT NULL,
    brand_id bigint NOT NULL,
    model_number character varying(100) NOT NULL,
    date_eol date,
    images_list json DEFAULT '[]'::json NOT NULL,
    is_active boolean DEFAULT false NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL,
    deleted_at timestamp(0) without time zone
);




--
-- Name: product_brand; Type: TABLE; Schema: public; Owner: pgadmin
--

CREATE TABLE public.product_brand (
    id bigint NOT NULL,
    creator_id bigint NOT NULL,
    name character varying(100) NOT NULL,
    is_active boolean DEFAULT false NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL,
    deleted_at timestamp(0) without time zone
);




--
-- Name: product_brand_id_seq; Type: SEQUENCE; Schema: public; Owner: pgadmin
--

CREATE SEQUENCE public.product_brand_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;




--
-- Name: product_brand_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pgadmin
--

ALTER SEQUENCE public.product_brand_id_seq OWNED BY public.product_brand.id;


--
-- Name: product_category; Type: TABLE; Schema: public; Owner: pgadmin
--

CREATE TABLE public.product_category (
    id bigint NOT NULL,
    name json NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL,
    deleted_at timestamp(0) without time zone
);




--
-- Name: product_category_id_seq; Type: SEQUENCE; Schema: public; Owner: pgadmin
--

CREATE SEQUENCE public.product_category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;




--
-- Name: product_category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pgadmin
--

ALTER SEQUENCE public.product_category_id_seq OWNED BY public.product_category.id;


--
-- Name: product_category_specification_ref; Type: TABLE; Schema: public; Owner: pgadmin
--

CREATE TABLE public.product_category_specification_ref (
    category_id bigint NOT NULL,
    specification_id bigint NOT NULL
);




--
-- Name: product_id_seq; Type: SEQUENCE; Schema: public; Owner: pgadmin
--

CREATE SEQUENCE public.product_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;




--
-- Name: product_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pgadmin
--

ALTER SEQUENCE public.product_id_seq OWNED BY public.product.id;


--
-- Name: product_specification; Type: TABLE; Schema: public; Owner: pgadmin
--

CREATE TABLE public.product_specification (
    id bigint NOT NULL,
    name json NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL,
    deleted_at timestamp(0) without time zone
);




--
-- Name: product_specification_id_seq; Type: SEQUENCE; Schema: public; Owner: pgadmin
--

CREATE SEQUENCE public.product_specification_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;




--
-- Name: product_specification_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pgadmin
--

ALTER SEQUENCE public.product_specification_id_seq OWNED BY public.product_specification.id;


--
-- Name: product_specification_option; Type: TABLE; Schema: public; Owner: pgadmin
--

CREATE TABLE public.product_specification_option (
    id bigint NOT NULL,
    specification_id bigint NOT NULL,
    name json NOT NULL,
    sort_index smallint NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL
);




--
-- Name: product_specification_option_id_seq; Type: SEQUENCE; Schema: public; Owner: pgadmin
--

CREATE SEQUENCE public.product_specification_option_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;




--
-- Name: product_specification_option_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pgadmin
--

ALTER SEQUENCE public.product_specification_option_id_seq OWNED BY public.product_specification_option.id;


--
-- Name: product_variation; Type: TABLE; Schema: public; Owner: pgadmin
--

CREATE TABLE public.product_variation (
    id bigint NOT NULL,
    product_id bigint NOT NULL,
    sku character varying(100) NOT NULL,
    images_list json DEFAULT '[]'::json NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL,
    deleted_at timestamp(0) without time zone
);




--
-- Name: product_variation_id_seq; Type: SEQUENCE; Schema: public; Owner: pgadmin
--

CREATE SEQUENCE public.product_variation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;




--
-- Name: product_variation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pgadmin
--

ALTER SEQUENCE public.product_variation_id_seq OWNED BY public.product_variation.id;


--
-- Name: product_variation_option_ref; Type: TABLE; Schema: public; Owner: pgadmin
--

CREATE TABLE public.product_variation_option_ref (
    variation_id bigint NOT NULL,
    option_id bigint NOT NULL
);




--
-- Name: region; Type: TABLE; Schema: public; Owner: pgadmin
--

CREATE TABLE public.region (
    id bigint NOT NULL,
    name json NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL,
    deleted_at timestamp(0) without time zone
);




--
-- Name: region_city; Type: TABLE; Schema: public; Owner: pgadmin
--

CREATE TABLE public.region_city (
    id bigint NOT NULL,
    region_id bigint NOT NULL,
    name json NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL,
    deleted_at timestamp(0) without time zone
);




--
-- Name: region_city_id_seq; Type: SEQUENCE; Schema: public; Owner: pgadmin
--

CREATE SEQUENCE public.region_city_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;




--
-- Name: region_city_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pgadmin
--

ALTER SEQUENCE public.region_city_id_seq OWNED BY public.region_city.id;


--
-- Name: region_country; Type: TABLE; Schema: public; Owner: pgadmin
--

CREATE TABLE public.region_country (
    id bigint NOT NULL,
    name json NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL,
    deleted_at timestamp(0) without time zone
);




--
-- Name: region_country_id_seq; Type: SEQUENCE; Schema: public; Owner: pgadmin
--

CREATE SEQUENCE public.region_country_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;




--
-- Name: region_country_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pgadmin
--

ALTER SEQUENCE public.region_country_id_seq OWNED BY public.region_country.id;


--
-- Name: region_id_seq; Type: SEQUENCE; Schema: public; Owner: pgadmin
--

CREATE SEQUENCE public.region_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;




--
-- Name: region_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pgadmin
--

ALTER SEQUENCE public.region_id_seq OWNED BY public.region.id;


--
-- Name: report; Type: TABLE; Schema: public; Owner: pgadmin
--

CREATE TABLE public.report (
    id bigint NOT NULL,
    creator_id bigint NOT NULL,
    shop_id bigint NOT NULL,
    task_report_id bigint,
    type character varying(255) NOT NULL,
    date_period_type character varying(255) NOT NULL,
    date_period_from date NOT NULL,
    date_period_to date NOT NULL,
    location json NOT NULL,
    images_list json DEFAULT '[]'::json NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL,
    deleted_at timestamp(0) without time zone,
    CONSTRAINT report_date_period_type_check CHECK (((date_period_type)::text = ANY ((ARRAY['week'::character varying, 'month'::character varying])::text[]))),
    CONSTRAINT report_type_check CHECK (((type)::text = ANY ((ARRAY['ds'::character varying, 'sr'::character varying])::text[])))
);




--
-- Name: report_id_seq; Type: SEQUENCE; Schema: public; Owner: pgadmin
--

CREATE SEQUENCE public.report_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;




--
-- Name: report_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pgadmin
--

ALTER SEQUENCE public.report_id_seq OWNED BY public.report.id;


--
-- Name: report_product; Type: TABLE; Schema: public; Owner: pgadmin
--

CREATE TABLE public.report_product (
    id bigint NOT NULL,
    report_id bigint NOT NULL,
    product_id bigint NOT NULL,
    variation_id bigint,
    supplier_id bigint NOT NULL,
    quantity integer NOT NULL,
    sort_index smallint NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL
);




--
-- Name: report_product_id_seq; Type: SEQUENCE; Schema: public; Owner: pgadmin
--

CREATE SEQUENCE public.report_product_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;




--
-- Name: report_product_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pgadmin
--

ALTER SEQUENCE public.report_product_id_seq OWNED BY public.report_product.id;


--
-- Name: shop; Type: TABLE; Schema: public; Owner: pgadmin
--

CREATE TABLE public.shop (
    id bigint NOT NULL,
    agent_id bigint NOT NULL,
    city_id bigint NOT NULL,
    company_id bigint,
    name character varying(100) NOT NULL,
    address character varying(1000) NOT NULL,
    has_credit_line boolean DEFAULT false NOT NULL,
    location json DEFAULT '[]'::json NOT NULL,
    number character varying(100) NOT NULL,
    store_type      varchar(100),
    is_active boolean DEFAULT false NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL,
    deleted_at timestamp(0) without time zone
);




--
-- Name: shop_brand_ref; Type: TABLE; Schema: public; Owner: pgadmin
--

CREATE TABLE public.shop_brand_ref (
    shop_id bigint NOT NULL,
    brand_id bigint NOT NULL
);




--
-- Name: shop_company; Type: TABLE; Schema: public; Owner: pgadmin
--

CREATE TABLE public.shop_company (
    id bigint NOT NULL,
    name character varying(100) NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL,
    deleted_at timestamp(0) without time zone
);




--
-- Name: shop_company_id_seq; Type: SEQUENCE; Schema: public; Owner: pgadmin
--

CREATE SEQUENCE public.shop_company_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;




--
-- Name: shop_company_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pgadmin
--

ALTER SEQUENCE public.shop_company_id_seq OWNED BY public.shop_company.id;


--
-- Name: shop_contact; Type: TABLE; Schema: public; Owner: pgadmin
--

CREATE TABLE public.shop_contact (
    id bigint NOT NULL,
    creator_id bigint NOT NULL,
    first_name character varying(100) NOT NULL,
    second_name character varying(100),
    last_name character varying(100),
    "position" character varying(100),
    phone character varying(100) NOT NULL,
    is_active boolean DEFAULT false NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL,
    deleted_at timestamp(0) without time zone
);




--
-- Name: shop_contact_id_seq; Type: SEQUENCE; Schema: public; Owner: pgadmin
--

CREATE SEQUENCE public.shop_contact_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;




--
-- Name: shop_contact_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pgadmin
--

ALTER SEQUENCE public.shop_contact_id_seq OWNED BY public.shop_contact.id;


--
-- Name: shop_contact_ref; Type: TABLE; Schema: public; Owner: pgadmin
--

CREATE TABLE public.shop_contact_ref (
    shop_id bigint NOT NULL,
    contact_id bigint NOT NULL
);




--
-- Name: shop_id_seq; Type: SEQUENCE; Schema: public; Owner: pgadmin
--

CREATE SEQUENCE public.shop_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;




--
-- Name: shop_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pgadmin
--

ALTER SEQUENCE public.shop_id_seq OWNED BY public.shop.id;


--
-- Name: shop_supplier; Type: TABLE; Schema: public; Owner: pgadmin
--

CREATE TABLE public.shop_supplier (
    id bigint NOT NULL,
    creator_id bigint NOT NULL,
    country_id bigint,
    short_name character varying(100) NOT NULL,
    full_name character varying(100),
    is_active boolean DEFAULT false NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL,
    deleted_at timestamp(0) without time zone
);




--
-- Name: shop_supplier_id_seq; Type: SEQUENCE; Schema: public; Owner: pgadmin
--

CREATE SEQUENCE public.shop_supplier_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;




--
-- Name: shop_supplier_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pgadmin
--

ALTER SEQUENCE public.shop_supplier_id_seq OWNED BY public.shop_supplier.id;


--
-- Name: shop_supplier_ref; Type: TABLE; Schema: public; Owner: pgadmin
--

CREATE TABLE public.shop_supplier_ref (
    shop_id bigint NOT NULL,
    supplier_id bigint NOT NULL
);




--
-- Name: system_language; Type: TABLE; Schema: public; Owner: pgadmin
--

CREATE TABLE public.system_language (
    id bigint NOT NULL,
    name character varying(100) NOT NULL,
    code character varying(10) NOT NULL,
    image character varying(100) NOT NULL,
    is_active boolean DEFAULT true NOT NULL,
    is_main boolean DEFAULT false NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL
);




--
-- Name: system_language_id_seq; Type: SEQUENCE; Schema: public; Owner: pgadmin
--

CREATE SEQUENCE public.system_language_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;




--
-- Name: system_language_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pgadmin
--

ALTER SEQUENCE public.system_language_id_seq OWNED BY public.system_language.id;


--
-- Name: system_settings; Type: TABLE; Schema: public; Owner: pgadmin
--

CREATE TABLE public.system_settings (
    name character varying(100) NOT NULL,
    value character varying(100) NOT NULL
);




--
-- Name: task; Type: TABLE; Schema: public; Owner: pgadmin
--

CREATE TABLE public.task (
    id bigint NOT NULL,
    agent_id bigint NOT NULL,
    type character varying(255) NOT NULL,
    name character varying(100) NOT NULL,
    description character varying(4096),
    execution_comment character varying(4096),
    deadline date NOT NULL,
    agent_status character varying(255) DEFAULT 'opened'::character varying NOT NULL,
    admin_status character varying(255) DEFAULT 'unchecked'::character varying NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL,
    deleted_at timestamp(0) without time zone,
    CONSTRAINT task_admin_status_check CHECK (((admin_status)::text = ANY ((ARRAY['unchecked'::character varying, 'moderating'::character varying, 'accepted'::character varying, 'declined'::character varying])::text[]))),
    CONSTRAINT task_agent_status_check CHECK (((agent_status)::text = ANY ((ARRAY['opened'::character varying, 'in_progress'::character varying, 'completed'::character varying, 'overdue'::character varying])::text[]))),
    CONSTRAINT task_type_check CHECK (((type)::text = ANY ((ARRAY['v'::character varying, 's'::character varying])::text[])))
);




--
-- Name: task_id_seq; Type: SEQUENCE; Schema: public; Owner: pgadmin
--

CREATE SEQUENCE public.task_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;




--
-- Name: task_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pgadmin
--

ALTER SEQUENCE public.task_id_seq OWNED BY public.task.id;


--
-- Name: task_report; Type: TABLE; Schema: public; Owner: pgadmin
--

CREATE TABLE public.task_report (
    id bigint NOT NULL,
    task_id bigint NOT NULL,
    shop_id bigint NOT NULL,
    type character varying(255) NOT NULL,
    date_period_type character varying(255) NOT NULL,
    sort_index smallint NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL,
    CONSTRAINT task_report_date_period_type_check CHECK (((date_period_type)::text = ANY ((ARRAY['week'::character varying, 'month'::character varying])::text[]))),
    CONSTRAINT task_report_type_check CHECK (((type)::text = ANY ((ARRAY['ds'::character varying, 'sr'::character varying])::text[])))
);




--
-- Name: task_report_id_seq; Type: SEQUENCE; Schema: public; Owner: pgadmin
--

CREATE SEQUENCE public.task_report_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;




--
-- Name: task_report_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pgadmin
--

ALTER SEQUENCE public.task_report_id_seq OWNED BY public.task_report.id;


--
-- Name: user; Type: TABLE; Schema: public; Owner: pgadmin
--

CREATE TABLE public."user" (
    id bigint NOT NULL,
    username character varying(100) NOT NULL,
    email character varying(100) NOT NULL,
    email_verified_at timestamp(0) without time zone,
    password character varying(100) NOT NULL,
    remember_token character varying(100),
    reset_password_code character varying(100),
    full_name character varying(100) NOT NULL,
    phone character varying(9),
    role character varying(255) NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL,
    deleted_at timestamp(0) without time zone,
    CONSTRAINT user_role_check CHECK (((role)::text = ANY ((ARRAY['admin'::character varying, 'agent'::character varying])::text[])))
);




--
-- Name: user_city_ref; Type: TABLE; Schema: public; Owner: pgadmin
--

CREATE TABLE public.user_city_ref (
    user_id bigint NOT NULL,
    city_id bigint NOT NULL
);




--
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: pgadmin
--

CREATE SEQUENCE public.user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;




--
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pgadmin
--

ALTER SEQUENCE public.user_id_seq OWNED BY public."user".id;


--
-- Name: user_message; Type: TABLE; Schema: public; Owner: pgadmin
--

CREATE TABLE public.user_message (
    id bigint NOT NULL,
    user_id bigint NOT NULL,
    text character varying(4096),
    files_list json DEFAULT '[]'::json NOT NULL,
    is_sent_by_admin boolean DEFAULT false NOT NULL,
    is_seen boolean DEFAULT false NOT NULL,
    created_at timestamp(0) without time zone NOT NULL
);




--
-- Name: user_message_id_seq; Type: SEQUENCE; Schema: public; Owner: pgadmin
--

CREATE SEQUENCE public.user_message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;




--
-- Name: user_message_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pgadmin
--

ALTER SEQUENCE public.user_message_id_seq OWNED BY public.user_message.id;


--
-- Name: user_region_ref; Type: TABLE; Schema: public; Owner: pgadmin
--

CREATE TABLE public.user_region_ref (
    user_id bigint NOT NULL,
    region_id bigint NOT NULL
);




--
-- Name: failed_jobs id; Type: DEFAULT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.failed_jobs ALTER COLUMN id SET DEFAULT nextval('public.failed_jobs_id_seq'::regclass);


--
-- Name: jobs id; Type: DEFAULT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.jobs ALTER COLUMN id SET DEFAULT nextval('public.jobs_id_seq'::regclass);


--
-- Name: migrations id; Type: DEFAULT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.migrations ALTER COLUMN id SET DEFAULT nextval('public.migrations_id_seq'::regclass);


--
-- Name: product id; Type: DEFAULT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.product ALTER COLUMN id SET DEFAULT nextval('public.product_id_seq'::regclass);


--
-- Name: product_brand id; Type: DEFAULT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.product_brand ALTER COLUMN id SET DEFAULT nextval('public.product_brand_id_seq'::regclass);


--
-- Name: product_category id; Type: DEFAULT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.product_category ALTER COLUMN id SET DEFAULT nextval('public.product_category_id_seq'::regclass);


--
-- Name: product_specification id; Type: DEFAULT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.product_specification ALTER COLUMN id SET DEFAULT nextval('public.product_specification_id_seq'::regclass);


--
-- Name: product_specification_option id; Type: DEFAULT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.product_specification_option ALTER COLUMN id SET DEFAULT nextval('public.product_specification_option_id_seq'::regclass);


--
-- Name: product_variation id; Type: DEFAULT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.product_variation ALTER COLUMN id SET DEFAULT nextval('public.product_variation_id_seq'::regclass);


--
-- Name: region id; Type: DEFAULT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.region ALTER COLUMN id SET DEFAULT nextval('public.region_id_seq'::regclass);


--
-- Name: region_city id; Type: DEFAULT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.region_city ALTER COLUMN id SET DEFAULT nextval('public.region_city_id_seq'::regclass);


--
-- Name: region_country id; Type: DEFAULT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.region_country ALTER COLUMN id SET DEFAULT nextval('public.region_country_id_seq'::regclass);


--
-- Name: report id; Type: DEFAULT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.report ALTER COLUMN id SET DEFAULT nextval('public.report_id_seq'::regclass);


--
-- Name: report_product id; Type: DEFAULT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.report_product ALTER COLUMN id SET DEFAULT nextval('public.report_product_id_seq'::regclass);


--
-- Name: shop id; Type: DEFAULT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.shop ALTER COLUMN id SET DEFAULT nextval('public.shop_id_seq'::regclass);


--
-- Name: shop_company id; Type: DEFAULT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.shop_company ALTER COLUMN id SET DEFAULT nextval('public.shop_company_id_seq'::regclass);


--
-- Name: shop_contact id; Type: DEFAULT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.shop_contact ALTER COLUMN id SET DEFAULT nextval('public.shop_contact_id_seq'::regclass);


--
-- Name: shop_supplier id; Type: DEFAULT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.shop_supplier ALTER COLUMN id SET DEFAULT nextval('public.shop_supplier_id_seq'::regclass);


--
-- Name: system_language id; Type: DEFAULT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.system_language ALTER COLUMN id SET DEFAULT nextval('public.system_language_id_seq'::regclass);


--
-- Name: task id; Type: DEFAULT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.task ALTER COLUMN id SET DEFAULT nextval('public.task_id_seq'::regclass);


--
-- Name: task_report id; Type: DEFAULT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.task_report ALTER COLUMN id SET DEFAULT nextval('public.task_report_id_seq'::regclass);


--
-- Name: user id; Type: DEFAULT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public."user" ALTER COLUMN id SET DEFAULT nextval('public.user_id_seq'::regclass);


--
-- Name: user_message id; Type: DEFAULT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.user_message ALTER COLUMN id SET DEFAULT nextval('public.user_message_id_seq'::regclass);


--
-- Data for Name: failed_jobs; Type: TABLE DATA; Schema: public; Owner: pgadmin
--

COPY public.failed_jobs (id, uuid, connection, queue, payload, exception, failed_at) FROM stdin;
\.


--
-- Data for Name: jobs; Type: TABLE DATA; Schema: public; Owner: pgadmin
--

COPY public.jobs (id, queue, payload, attempts, reserved_at, available_at, created_at) FROM stdin;
\.


--
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: pgadmin
--

COPY public.migrations (id, migration, batch) FROM stdin;
1	2019_08_19_000000_failed_jobs_create	1
2	2022_03_21_084545_user_create	1
3	2022_04_21_013709_region_country_create	1
4	2022_04_21_015619_shop_company_create	1
5	2022_04_21_015620_shop_supplier_create	1
6	2022_04_21_082124_region_create	1
7	2022_04_21_082252_region_city_create	1
8	2022_04_21_083225_system_settings_create	1
9	2022_04_21_083242_system_language_create	1
10	2022_04_21_085721_user_region_ref_create	1
11	2022_04_21_085738_user_city_ref_create	1
12	2022_04_21_091043_user_message_create	1
13	2022_04_21_093907_product_brand_create	1
14	2022_04_21_093922_product_category_create	1
15	2022_04_21_093923_product_specification_create	1
16	2022_04_21_093924_product_specification_option_create	1
17	2022_04_21_093925_product_category_specification_ref_create	1
18	2022_04_21_095304_product_create	1
19	2022_04_21_100227_product_variation_create	1
20	2022_04_21_102340_shop_contact_create	1
21	2022_04_21_104957_shop_create	1
22	2022_04_21_105526_shop_supplier_ref_create	1
23	2022_04_21_105527_shop_contact_ref_create	1
24	2022_04_21_105540_shop_brand_ref_create	1
25	2022_04_21_133252_product_variation_option_ref_create	1
26	2022_04_21_135940_report_create	1
27	2022_04_21_140600_report_product_create	1
28	2022_04_21_143056_task_create	1
29	2022_04_21_144619_task_report_create	1
30	2022_07_18_173205_create_jobs_table	1
\.


--
-- Data for Name: product; Type: TABLE DATA; Schema: public; Owner: pgadmin
--

COPY public.product (id, creator_id, category_id, brand_id, model_number, date_eol, images_list, is_active, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- Data for Name: product_brand; Type: TABLE DATA; Schema: public; Owner: pgadmin
--

COPY public.product_brand (id, creator_id, name, is_active, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- Data for Name: product_category; Type: TABLE DATA; Schema: public; Owner: pgadmin
--

COPY public.product_category (id, name, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- Data for Name: product_category_specification_ref; Type: TABLE DATA; Schema: public; Owner: pgadmin
--

COPY public.product_category_specification_ref (category_id, specification_id) FROM stdin;
\.


--
-- Data for Name: product_specification; Type: TABLE DATA; Schema: public; Owner: pgadmin
--

COPY public.product_specification (id, name, created_at, updated_at, deleted_at) FROM stdin;
1	{"ru":"Color ru","uz":"Color uz"}	2022-08-18 05:30:00	2022-08-18 05:30:00	\N
\.


--
-- Data for Name: product_specification_option; Type: TABLE DATA; Schema: public; Owner: pgadmin
--

COPY public.product_specification_option (id, specification_id, name, sort_index, created_at, updated_at) FROM stdin;
6	1	{"ru":"White","uz":"White"}	0	2022-08-18 05:30:00	2022-08-18 05:30:00
7	1	{"ru":"Red","uz":"Red"}	1	2022-08-18 05:30:00	2022-08-18 05:30:00
8	1	{"ru":"Black","uz":"Black"}	2	2022-08-18 05:30:00	2022-08-18 05:30:00
\.


--
-- Data for Name: product_variation; Type: TABLE DATA; Schema: public; Owner: pgadmin
--

COPY public.product_variation (id, product_id, sku, images_list, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- Data for Name: product_variation_option_ref; Type: TABLE DATA; Schema: public; Owner: pgadmin
--

COPY public.product_variation_option_ref (variation_id, option_id) FROM stdin;
\.


--
-- Data for Name: region; Type: TABLE DATA; Schema: public; Owner: pgadmin
--

COPY public.region (id, name, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- Data for Name: region_city; Type: TABLE DATA; Schema: public; Owner: pgadmin
--

COPY public.region_city (id, region_id, name, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- Data for Name: region_country; Type: TABLE DATA; Schema: public; Owner: pgadmin
--

COPY public.region_country (id, name, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- Data for Name: report; Type: TABLE DATA; Schema: public; Owner: pgadmin
--

COPY public.report (id, creator_id, shop_id, task_report_id, type, date_period_type, date_period_from, date_period_to, location, images_list, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- Data for Name: report_product; Type: TABLE DATA; Schema: public; Owner: pgadmin
--

COPY public.report_product (id, report_id, product_id, variation_id, supplier_id, quantity, sort_index, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: shop; Type: TABLE DATA; Schema: public; Owner: pgadmin
--

COPY public.shop (id, agent_id, city_id, company_id, name, address, has_credit_line, location, number, is_active, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- Data for Name: shop_brand_ref; Type: TABLE DATA; Schema: public; Owner: pgadmin
--

COPY public.shop_brand_ref (shop_id, brand_id) FROM stdin;
\.


--
-- Data for Name: shop_company; Type: TABLE DATA; Schema: public; Owner: pgadmin
--

COPY public.shop_company (id, name, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- Data for Name: shop_contact; Type: TABLE DATA; Schema: public; Owner: pgadmin
--

COPY public.shop_contact (id, creator_id, first_name, second_name, last_name, "position", phone, is_active, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- Data for Name: shop_contact_ref; Type: TABLE DATA; Schema: public; Owner: pgadmin
--

COPY public.shop_contact_ref (shop_id, contact_id) FROM stdin;
\.


--
-- Data for Name: shop_supplier; Type: TABLE DATA; Schema: public; Owner: pgadmin
--

COPY public.shop_supplier (id, creator_id, country_id, short_name, full_name, is_active, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- Data for Name: shop_supplier_ref; Type: TABLE DATA; Schema: public; Owner: pgadmin
--

COPY public.shop_supplier_ref (shop_id, supplier_id) FROM stdin;
\.


--
-- Data for Name: system_language; Type: TABLE DATA; Schema: public; Owner: pgadmin
--

COPY public.system_language (id, name, code, image, is_active, is_main, created_at, updated_at) FROM stdin;
1	Русский	ru	/test_data/flags/ru.png	t	t	2022-08-18 05:19:14	2022-08-18 05:19:14
2	O'zbek	uz	/test_data/flags/uz.png	t	f	2022-08-18 05:19:14	2022-08-18 05:19:14
\.


--
-- Data for Name: system_settings; Type: TABLE DATA; Schema: public; Owner: pgadmin
--

COPY public.system_settings (name, value) FROM stdin;
admin_email	info@local.host
favicon	/test_data/logo.png
logo	/test_data/logo.png
project_name	BM Electronics
\.


--
-- Data for Name: task; Type: TABLE DATA; Schema: public; Owner: pgadmin
--

COPY public.task (id, agent_id, type, name, description, execution_comment, deadline, agent_status, admin_status, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- Data for Name: task_report; Type: TABLE DATA; Schema: public; Owner: pgadmin
--

COPY public.task_report (id, task_id, shop_id, type, date_period_type, sort_index, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: pgadmin
--

COPY public."user" (id, username, email, email_verified_at, password, remember_token, reset_password_code, full_name, phone, role, created_at, updated_at, deleted_at) FROM stdin;
1	admin	admin@local.host	2022-08-18 05:19:14	$2y$10$bOuPJ27OmJxTszZm5Juy/eRcYxEBtb5.nZvxhKL..9piiD8erjIdu	\N	\N	Administrator	001111111	admin	2022-08-18 05:19:14	2022-08-18 05:19:14	\N
2	agent_1	agent_1@local.host	2022-08-18 05:19:14	$2y$10$GWS/heXVGpsJwT5EnslAceXVL4VEMuGGxFZ8p6mDb3ouKQONdCpOm	\N	\N	Bahodir Mirzaahmedov	\N	agent	2022-08-18 05:19:14	2022-08-18 05:19:14	\N
\.


--
-- Data for Name: user_city_ref; Type: TABLE DATA; Schema: public; Owner: pgadmin
--

COPY public.user_city_ref (user_id, city_id) FROM stdin;
\.


--
-- Data for Name: user_message; Type: TABLE DATA; Schema: public; Owner: pgadmin
--

COPY public.user_message (id, user_id, text, files_list, is_sent_by_admin, is_seen, created_at) FROM stdin;
\.


--
-- Data for Name: user_region_ref; Type: TABLE DATA; Schema: public; Owner: pgadmin
--

COPY public.user_region_ref (user_id, region_id) FROM stdin;
\.


--
-- Name: failed_jobs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pgadmin
--

SELECT pg_catalog.setval('public.failed_jobs_id_seq', 1, false);


--
-- Name: jobs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pgadmin
--

SELECT pg_catalog.setval('public.jobs_id_seq', 1, false);


--
-- Name: migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pgadmin
--

SELECT pg_catalog.setval('public.migrations_id_seq', 30, true);


--
-- Name: product_brand_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pgadmin
--

SELECT pg_catalog.setval('public.product_brand_id_seq', 2, true);


--
-- Name: product_category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pgadmin
--

SELECT pg_catalog.setval('public.product_category_id_seq', 2, true);


--
-- Name: product_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pgadmin
--

SELECT pg_catalog.setval('public.product_id_seq', 4, true);


--
-- Name: product_specification_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pgadmin
--

SELECT pg_catalog.setval('public.product_specification_id_seq', 4, true);


--
-- Name: product_specification_option_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pgadmin
--

SELECT pg_catalog.setval('public.product_specification_option_id_seq', 8, true);


--
-- Name: product_variation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pgadmin
--

SELECT pg_catalog.setval('public.product_variation_id_seq', 3, true);


--
-- Name: region_city_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pgadmin
--

SELECT pg_catalog.setval('public.region_city_id_seq', 2, true);


--
-- Name: region_country_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pgadmin
--

SELECT pg_catalog.setval('public.region_country_id_seq', 2, true);


--
-- Name: region_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pgadmin
--

SELECT pg_catalog.setval('public.region_id_seq', 2, true);


--
-- Name: report_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pgadmin
--

SELECT pg_catalog.setval('public.report_id_seq', 2, true);


--
-- Name: report_product_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pgadmin
--

SELECT pg_catalog.setval('public.report_product_id_seq', 2, true);


--
-- Name: shop_company_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pgadmin
--

SELECT pg_catalog.setval('public.shop_company_id_seq', 2, true);


--
-- Name: shop_contact_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pgadmin
--

SELECT pg_catalog.setval('public.shop_contact_id_seq', 2, true);


--
-- Name: shop_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pgadmin
--

SELECT pg_catalog.setval('public.shop_id_seq', 2, true);


--
-- Name: shop_supplier_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pgadmin
--

SELECT pg_catalog.setval('public.shop_supplier_id_seq', 2, true);


--
-- Name: system_language_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pgadmin
--

SELECT pg_catalog.setval('public.system_language_id_seq', 2, true);


--
-- Name: task_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pgadmin
--

SELECT pg_catalog.setval('public.task_id_seq', 2, true);


--
-- Name: task_report_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pgadmin
--

SELECT pg_catalog.setval('public.task_report_id_seq', 3, true);


--
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pgadmin
--

SELECT pg_catalog.setval('public.user_id_seq', 2, true);


--
-- Name: user_message_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pgadmin
--

SELECT pg_catalog.setval('public.user_message_id_seq', 2, true);


--
-- Name: failed_jobs failed_jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.failed_jobs
    ADD CONSTRAINT failed_jobs_pkey PRIMARY KEY (id);


--
-- Name: failed_jobs failed_jobs_uuid_unique; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.failed_jobs
    ADD CONSTRAINT failed_jobs_uuid_unique UNIQUE (uuid);


--
-- Name: jobs jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.jobs
    ADD CONSTRAINT jobs_pkey PRIMARY KEY (id);


--
-- Name: migrations migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);


--
-- Name: product_brand product_brand_pkey; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.product_brand
    ADD CONSTRAINT product_brand_pkey PRIMARY KEY (id);


--
-- Name: product_category product_category_pkey; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.product_category
    ADD CONSTRAINT product_category_pkey PRIMARY KEY (id);


--
-- Name: product_category_specification_ref product_category_specification_ref_pkey; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.product_category_specification_ref
    ADD CONSTRAINT product_category_specification_ref_pkey PRIMARY KEY (category_id, specification_id);


--
-- Name: product product_pkey; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.product
    ADD CONSTRAINT product_pkey PRIMARY KEY (id);


--
-- Name: product_specification_option product_specification_option_pkey; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.product_specification_option
    ADD CONSTRAINT product_specification_option_pkey PRIMARY KEY (id);


--
-- Name: product_specification product_specification_pkey; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.product_specification
    ADD CONSTRAINT product_specification_pkey PRIMARY KEY (id);


--
-- Name: product_variation_option_ref product_variation_option_ref_pkey; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.product_variation_option_ref
    ADD CONSTRAINT product_variation_option_ref_pkey PRIMARY KEY (variation_id, option_id);


--
-- Name: product_variation product_variation_pkey; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.product_variation
    ADD CONSTRAINT product_variation_pkey PRIMARY KEY (id);


--
-- Name: region_city region_city_pkey; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.region_city
    ADD CONSTRAINT region_city_pkey PRIMARY KEY (id);


--
-- Name: region_country region_country_pkey; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.region_country
    ADD CONSTRAINT region_country_pkey PRIMARY KEY (id);


--
-- Name: region region_pkey; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.region
    ADD CONSTRAINT region_pkey PRIMARY KEY (id);


--
-- Name: report report_pkey; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.report
    ADD CONSTRAINT report_pkey PRIMARY KEY (id);


--
-- Name: report_product report_product_pkey; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.report_product
    ADD CONSTRAINT report_product_pkey PRIMARY KEY (id);


--
-- Name: shop_brand_ref shop_brand_ref_pkey; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.shop_brand_ref
    ADD CONSTRAINT shop_brand_ref_pkey PRIMARY KEY (shop_id, brand_id);


--
-- Name: shop_company shop_company_pkey; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.shop_company
    ADD CONSTRAINT shop_company_pkey PRIMARY KEY (id);


--
-- Name: shop_contact shop_contact_pkey; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.shop_contact
    ADD CONSTRAINT shop_contact_pkey PRIMARY KEY (id);


--
-- Name: shop_contact_ref shop_contact_ref_pkey; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.shop_contact_ref
    ADD CONSTRAINT shop_contact_ref_pkey PRIMARY KEY (shop_id, contact_id);


--
-- Name: shop shop_number_unique; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.shop
    ADD CONSTRAINT shop_number_unique UNIQUE (number);


--
-- Name: shop shop_pkey; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.shop
    ADD CONSTRAINT shop_pkey PRIMARY KEY (id);


--
-- Name: shop_supplier shop_supplier_pkey; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.shop_supplier
    ADD CONSTRAINT shop_supplier_pkey PRIMARY KEY (id);


--
-- Name: shop_supplier_ref shop_supplier_ref_pkey; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.shop_supplier_ref
    ADD CONSTRAINT shop_supplier_ref_pkey PRIMARY KEY (shop_id, supplier_id);


--
-- Name: system_language system_language_code_unique; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.system_language
    ADD CONSTRAINT system_language_code_unique UNIQUE (code);


--
-- Name: system_language system_language_pkey; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.system_language
    ADD CONSTRAINT system_language_pkey PRIMARY KEY (id);


--
-- Name: system_settings system_settings_pkey; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.system_settings
    ADD CONSTRAINT system_settings_pkey PRIMARY KEY (name);


--
-- Name: task task_pkey; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.task
    ADD CONSTRAINT task_pkey PRIMARY KEY (id);


--
-- Name: task_report task_report_pkey; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.task_report
    ADD CONSTRAINT task_report_pkey PRIMARY KEY (id);


--
-- Name: user_city_ref user_city_ref_pkey; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.user_city_ref
    ADD CONSTRAINT user_city_ref_pkey PRIMARY KEY (user_id, city_id);


--
-- Name: user user_email_unique; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_email_unique UNIQUE (email);


--
-- Name: user_message user_message_pkey; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.user_message
    ADD CONSTRAINT user_message_pkey PRIMARY KEY (id);


--
-- Name: user user_pkey; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- Name: user_region_ref user_region_ref_pkey; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.user_region_ref
    ADD CONSTRAINT user_region_ref_pkey PRIMARY KEY (user_id, region_id);


--
-- Name: user user_username_unique; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_username_unique UNIQUE (username);


--
-- Name: jobs_queue_index; Type: INDEX; Schema: public; Owner: pgadmin
--

CREATE INDEX jobs_queue_index ON public.jobs USING btree (queue);


--
-- Name: product_brand_creator_id_index; Type: INDEX; Schema: public; Owner: pgadmin
--

CREATE INDEX product_brand_creator_id_index ON public.product_brand USING btree (creator_id);


--
-- Name: product_brand_id_index; Type: INDEX; Schema: public; Owner: pgadmin
--

CREATE INDEX product_brand_id_index ON public.product USING btree (brand_id);


--
-- Name: product_brand_is_active_index; Type: INDEX; Schema: public; Owner: pgadmin
--

CREATE INDEX product_brand_is_active_index ON public.product_brand USING btree (is_active);


--
-- Name: product_category_id_index; Type: INDEX; Schema: public; Owner: pgadmin
--

CREATE INDEX product_category_id_index ON public.product USING btree (category_id);


--
-- Name: product_creator_id_index; Type: INDEX; Schema: public; Owner: pgadmin
--

CREATE INDEX product_creator_id_index ON public.product USING btree (creator_id);


--
-- Name: product_is_active_index; Type: INDEX; Schema: public; Owner: pgadmin
--

CREATE INDEX product_is_active_index ON public.product USING btree (is_active);


--
-- Name: product_model_number_index; Type: INDEX; Schema: public; Owner: pgadmin
--

CREATE INDEX product_model_number_index ON public.product USING btree (model_number);


--
-- Name: product_specification_option_sort_index_index; Type: INDEX; Schema: public; Owner: pgadmin
--

CREATE INDEX product_specification_option_sort_index_index ON public.product_specification_option USING btree (sort_index);


--
-- Name: product_specification_option_specification_id_index; Type: INDEX; Schema: public; Owner: pgadmin
--

CREATE INDEX product_specification_option_specification_id_index ON public.product_specification_option USING btree (specification_id);


--
-- Name: product_variation_product_id_index; Type: INDEX; Schema: public; Owner: pgadmin
--

CREATE INDEX product_variation_product_id_index ON public.product_variation USING btree (product_id);


--
-- Name: product_variation_sku_index; Type: INDEX; Schema: public; Owner: pgadmin
--

CREATE INDEX product_variation_sku_index ON public.product_variation USING btree (sku);


--
-- Name: region_city_region_id_index; Type: INDEX; Schema: public; Owner: pgadmin
--

CREATE INDEX region_city_region_id_index ON public.region_city USING btree (region_id);


--
-- Name: report_creator_id_index; Type: INDEX; Schema: public; Owner: pgadmin
--

CREATE INDEX report_creator_id_index ON public.report USING btree (creator_id);


--
-- Name: report_product_product_id_index; Type: INDEX; Schema: public; Owner: pgadmin
--

CREATE INDEX report_product_product_id_index ON public.report_product USING btree (product_id);


--
-- Name: report_product_report_id_index; Type: INDEX; Schema: public; Owner: pgadmin
--

CREATE INDEX report_product_report_id_index ON public.report_product USING btree (report_id);


--
-- Name: report_product_sort_index_index; Type: INDEX; Schema: public; Owner: pgadmin
--

CREATE INDEX report_product_sort_index_index ON public.report_product USING btree (sort_index);


--
-- Name: report_product_supplier_id_index; Type: INDEX; Schema: public; Owner: pgadmin
--

CREATE INDEX report_product_supplier_id_index ON public.report_product USING btree (supplier_id);


--
-- Name: report_product_variation_id_index; Type: INDEX; Schema: public; Owner: pgadmin
--

CREATE INDEX report_product_variation_id_index ON public.report_product USING btree (variation_id);


--
-- Name: report_shop_id_index; Type: INDEX; Schema: public; Owner: pgadmin
--

CREATE INDEX report_shop_id_index ON public.report USING btree (shop_id);


--
-- Name: report_task_report_id_index; Type: INDEX; Schema: public; Owner: pgadmin
--

CREATE INDEX report_task_report_id_index ON public.report USING btree (task_report_id);


--
-- Name: shop_agent_id_index; Type: INDEX; Schema: public; Owner: pgadmin
--

CREATE INDEX shop_agent_id_index ON public.shop USING btree (agent_id);


--
-- Name: shop_city_id_index; Type: INDEX; Schema: public; Owner: pgadmin
--

CREATE INDEX shop_city_id_index ON public.shop USING btree (city_id);


--
-- Name: shop_company_id_index; Type: INDEX; Schema: public; Owner: pgadmin
--

CREATE INDEX shop_company_id_index ON public.shop USING btree (company_id);


--
-- Name: shop_contact_creator_id_index; Type: INDEX; Schema: public; Owner: pgadmin
--

CREATE INDEX shop_contact_creator_id_index ON public.shop_contact USING btree (creator_id);


--
-- Name: shop_contact_is_active_index; Type: INDEX; Schema: public; Owner: pgadmin
--

CREATE INDEX shop_contact_is_active_index ON public.shop_contact USING btree (is_active);


--
-- Name: shop_is_active_index; Type: INDEX; Schema: public; Owner: pgadmin
--

CREATE INDEX shop_is_active_index ON public.shop USING btree (is_active);


--
-- Name: shop_supplier_country_id_index; Type: INDEX; Schema: public; Owner: pgadmin
--

CREATE INDEX shop_supplier_country_id_index ON public.shop_supplier USING btree (country_id);


--
-- Name: shop_supplier_creator_id_index; Type: INDEX; Schema: public; Owner: pgadmin
--

CREATE INDEX shop_supplier_creator_id_index ON public.shop_supplier USING btree (creator_id);


--
-- Name: shop_supplier_is_active_index; Type: INDEX; Schema: public; Owner: pgadmin
--

CREATE INDEX shop_supplier_is_active_index ON public.shop_supplier USING btree (is_active);


--
-- Name: task_agent_id_index; Type: INDEX; Schema: public; Owner: pgadmin
--

CREATE INDEX task_agent_id_index ON public.task USING btree (agent_id);


--
-- Name: task_report_shop_id_index; Type: INDEX; Schema: public; Owner: pgadmin
--

CREATE INDEX task_report_shop_id_index ON public.task_report USING btree (shop_id);


--
-- Name: task_report_sort_index_index; Type: INDEX; Schema: public; Owner: pgadmin
--

CREATE INDEX task_report_sort_index_index ON public.task_report USING btree (sort_index);


--
-- Name: task_report_task_id_index; Type: INDEX; Schema: public; Owner: pgadmin
--

CREATE INDEX task_report_task_id_index ON public.task_report USING btree (task_id);


--
-- Name: user_message_user_id_index; Type: INDEX; Schema: public; Owner: pgadmin
--

CREATE INDEX user_message_user_id_index ON public.user_message USING btree (user_id);


--
-- Name: product_brand product_brand_creator_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.product_brand
    ADD CONSTRAINT product_brand_creator_id_foreign FOREIGN KEY (creator_id) REFERENCES public."user"(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: product product_brand_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.product
    ADD CONSTRAINT product_brand_id_foreign FOREIGN KEY (brand_id) REFERENCES public.product_brand(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: product product_category_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.product
    ADD CONSTRAINT product_category_id_foreign FOREIGN KEY (category_id) REFERENCES public.product_category(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: product_category_specification_ref product_category_specification_ref_category_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.product_category_specification_ref
    ADD CONSTRAINT product_category_specification_ref_category_id_foreign FOREIGN KEY (category_id) REFERENCES public.product_category(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: product_category_specification_ref product_category_specification_ref_specification_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.product_category_specification_ref
    ADD CONSTRAINT product_category_specification_ref_specification_id_foreign FOREIGN KEY (specification_id) REFERENCES public.product_specification(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: product product_creator_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.product
    ADD CONSTRAINT product_creator_id_foreign FOREIGN KEY (creator_id) REFERENCES public."user"(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: product_specification_option product_specification_option_specification_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.product_specification_option
    ADD CONSTRAINT product_specification_option_specification_id_foreign FOREIGN KEY (specification_id) REFERENCES public.product_specification(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: product_variation_option_ref product_variation_option_ref_option_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.product_variation_option_ref
    ADD CONSTRAINT product_variation_option_ref_option_id_foreign FOREIGN KEY (option_id) REFERENCES public.product_specification_option(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: product_variation_option_ref product_variation_option_ref_variation_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.product_variation_option_ref
    ADD CONSTRAINT product_variation_option_ref_variation_id_foreign FOREIGN KEY (variation_id) REFERENCES public.product_variation(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: product_variation product_variation_product_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.product_variation
    ADD CONSTRAINT product_variation_product_id_foreign FOREIGN KEY (product_id) REFERENCES public.product(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: region_city region_city_region_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.region_city
    ADD CONSTRAINT region_city_region_id_foreign FOREIGN KEY (region_id) REFERENCES public.region(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: report report_creator_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.report
    ADD CONSTRAINT report_creator_id_foreign FOREIGN KEY (creator_id) REFERENCES public."user"(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: report_product report_product_product_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.report_product
    ADD CONSTRAINT report_product_product_id_foreign FOREIGN KEY (product_id) REFERENCES public.product(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: report_product report_product_report_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.report_product
    ADD CONSTRAINT report_product_report_id_foreign FOREIGN KEY (report_id) REFERENCES public.report(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: report_product report_product_supplier_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.report_product
    ADD CONSTRAINT report_product_supplier_id_foreign FOREIGN KEY (supplier_id) REFERENCES public.shop_supplier(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: report_product report_product_variation_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.report_product
    ADD CONSTRAINT report_product_variation_id_foreign FOREIGN KEY (variation_id) REFERENCES public.product_variation(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: report report_shop_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.report
    ADD CONSTRAINT report_shop_id_foreign FOREIGN KEY (shop_id) REFERENCES public.shop(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: report report_task_report_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.report
    ADD CONSTRAINT report_task_report_id_foreign FOREIGN KEY (task_report_id) REFERENCES public.report(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: shop shop_agent_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.shop
    ADD CONSTRAINT shop_agent_id_foreign FOREIGN KEY (agent_id) REFERENCES public."user"(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: shop_brand_ref shop_brand_ref_brand_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.shop_brand_ref
    ADD CONSTRAINT shop_brand_ref_brand_id_foreign FOREIGN KEY (brand_id) REFERENCES public.product_brand(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: shop_brand_ref shop_brand_ref_shop_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.shop_brand_ref
    ADD CONSTRAINT shop_brand_ref_shop_id_foreign FOREIGN KEY (shop_id) REFERENCES public.shop(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: shop shop_city_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.shop
    ADD CONSTRAINT shop_city_id_foreign FOREIGN KEY (city_id) REFERENCES public.region_city(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: shop shop_company_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.shop
    ADD CONSTRAINT shop_company_id_foreign FOREIGN KEY (company_id) REFERENCES public.shop_company(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: shop_contact shop_contact_creator_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.shop_contact
    ADD CONSTRAINT shop_contact_creator_id_foreign FOREIGN KEY (creator_id) REFERENCES public."user"(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: shop_contact_ref shop_contact_ref_contact_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.shop_contact_ref
    ADD CONSTRAINT shop_contact_ref_contact_id_foreign FOREIGN KEY (contact_id) REFERENCES public.shop_contact(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: shop_contact_ref shop_contact_ref_shop_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.shop_contact_ref
    ADD CONSTRAINT shop_contact_ref_shop_id_foreign FOREIGN KEY (shop_id) REFERENCES public.shop(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: shop_supplier shop_supplier_country_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.shop_supplier
    ADD CONSTRAINT shop_supplier_country_id_foreign FOREIGN KEY (country_id) REFERENCES public.region_country(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: shop_supplier shop_supplier_creator_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.shop_supplier
    ADD CONSTRAINT shop_supplier_creator_id_foreign FOREIGN KEY (creator_id) REFERENCES public."user"(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: shop_supplier_ref shop_supplier_ref_shop_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.shop_supplier_ref
    ADD CONSTRAINT shop_supplier_ref_shop_id_foreign FOREIGN KEY (shop_id) REFERENCES public.shop(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: shop_supplier_ref shop_supplier_ref_supplier_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.shop_supplier_ref
    ADD CONSTRAINT shop_supplier_ref_supplier_id_foreign FOREIGN KEY (supplier_id) REFERENCES public.shop_supplier(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: task task_agent_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.task
    ADD CONSTRAINT task_agent_id_foreign FOREIGN KEY (agent_id) REFERENCES public."user"(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: task_report task_report_shop_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.task_report
    ADD CONSTRAINT task_report_shop_id_foreign FOREIGN KEY (shop_id) REFERENCES public.shop(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: task_report task_report_task_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.task_report
    ADD CONSTRAINT task_report_task_id_foreign FOREIGN KEY (task_id) REFERENCES public.task(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: user_city_ref user_city_ref_city_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.user_city_ref
    ADD CONSTRAINT user_city_ref_city_id_foreign FOREIGN KEY (city_id) REFERENCES public.region_city(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: user_city_ref user_city_ref_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.user_city_ref
    ADD CONSTRAINT user_city_ref_user_id_foreign FOREIGN KEY (user_id) REFERENCES public."user"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: user_message user_message_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.user_message
    ADD CONSTRAINT user_message_user_id_foreign FOREIGN KEY (user_id) REFERENCES public."user"(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: user_region_ref user_region_ref_region_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.user_region_ref
    ADD CONSTRAINT user_region_ref_region_id_foreign FOREIGN KEY (region_id) REFERENCES public.region(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: user_region_ref user_region_ref_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.user_region_ref
    ADD CONSTRAINT user_region_ref_user_id_foreign FOREIGN KEY (user_id) REFERENCES public."user"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

