<?php

namespace Tests\Feature\Admin\Report\Report;

use Illuminate\Http\UploadedFile;

class CreateTest extends _TestCase
{
    protected string $requestMethod = self::REQUEST_METHOD_POST;

    public function test_DS()
    {
        $this->requestBody = [
            'creator_id' => '2',
            'shop_id' => '1',
            'type' => 'ds',
            'date_period_type' => 'week',
            'date_period_from' => date('d.m.Y', strtotime('-1 week')),
            'location' => [
                '22.56',
                '33.28',
            ],
            'comment' => 'Comment 3',
            'shop_is_closed' => 0,

            'products' => [
                [
                    'product_id' => '1',
                    'supplier_id' => '1',
                    'quantity' => '1',
                ],
                [
                    'product_id' => '1',
                    'variation_id' => '1',
                    'supplier_id' => '1',
                    'quantity' => '2',
                ],
                [
                    'product_id' => '1',
                    'variation_id' => '2',
                    'supplier_id' => '1',
                    'quantity' => '5',
                ],
            ],
        ];

        $this->requestFiles = [
            'images_list' => [
                UploadedFile::fake()->create('image_1.jpg', 100, 'image/jpeg'),
                UploadedFile::fake()->create('image_2.jpg', 100, 'image/jpeg'),
            ],
        ];

        $this->response = $this->sendRequest();
        $this->response->assertStatus(201);
    }

    public function test_SR()
    {
        $this->requestBody = [
            'creator_id' => '2',
            'shop_id' => '1',
            'type' => 'sr',
            'date_period_type' => 'month',
            'date_period_from' => date('d.m.Y', strtotime('-1 month')),
            'location' => [
                '22.56',
                '33.28',
            ],
            'comment' => 'Comment 3',
            'shop_is_closed' => 0,

            'brands' => [
                [
                    'brand_id' => '1',
                    'quantity' => '3',
                ],
                [
                    'brand_id' => '2',
                    'quantity' => '4',
                ],
            ],
        ];

        $this->requestFiles = [
            'images_list' => [
                UploadedFile::fake()->create('image_1.jpg', 100, 'image/jpeg'),
                UploadedFile::fake()->create('image_2.jpg', 100, 'image/jpeg'),
            ],
        ];

        $this->response = $this->sendRequest();
        $this->response->assertStatus(201);
    }
}
