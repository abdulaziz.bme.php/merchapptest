<?php

namespace Tests\Feature\Admin\Report\Report;

use Illuminate\Http\UploadedFile;

class UpdateTest extends _TestCase
{
    protected string $requestMethod = self::REQUEST_METHOD_POST;

    protected array $requestQuery = [
        '_method' => self::REQUEST_METHOD_PATCH,
    ];

    public function test_DS()
    {
        $this->requestUrl .= '/1';

        $this->requestBody = [
            'creator_id' => '2',
            'shop_id' => '1',
            'type' => 'ds',
            'date_period_type' => 'week',
            'date_period_from' => date('d.m.Y', strtotime('-2 weeks')),
            'location' => [
                '22.56',
                '33.28',
            ],
            'comment' => 'Comment 1',
            'shop_is_closed' => 0,

            'products' => [
                [
                    'id' => '1',
                    'product_id' => '1',
                    'supplier_id' => '1',
                    'quantity' => '2',
                ],
                [
                    'id' => '2',
                    'product_id' => '1',
                    'variation_id' => '1',
                    'supplier_id' => '1',
                    'quantity' => '5',
                ],
            ],
        ];

        $this->requestFiles = [
            'images_list' => [
                UploadedFile::fake()->create('image_1.jpg', 100, 'image/jpeg'),
                UploadedFile::fake()->create('image_2.jpg', 100, 'image/jpeg'),
            ],
        ];

        $this->response = $this->sendRequest();
        $this->response->assertStatus(200);
    }

    public function test_SR()
    {
        $this->requestUrl .= '/3';

        $this->requestBody = [
            'creator_id' => '2',
            'shop_id' => '1',
            'type' => 'sr',
            'date_period_type' => 'month',
            'date_period_from' => date('d.m.Y', strtotime('-2 months')),
            'location' => [
                '22.56',
                '33.28',
            ],
            'comment' => 'Comment 1',
            'shop_is_closed' => 0,

            'brands' => [
                [
                    'id' => '1',
                    'brand_id' => '1',
                    'quantity' => '3',
                ],
                [
                    'id' => '2',
                    'brand_id' => '2',
                    'quantity' => '4',
                ],
            ],
        ];

        $this->requestFiles = [
            'images_list' => [
                UploadedFile::fake()->create('image_1.jpg', 100, 'image/jpeg'),
                UploadedFile::fake()->create('image_2.jpg', 100, 'image/jpeg'),
            ],
        ];

        $this->response = $this->sendRequest();
        $this->response->assertStatus(200);
    }
}
