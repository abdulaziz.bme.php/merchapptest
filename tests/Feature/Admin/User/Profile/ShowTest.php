<?php

namespace Tests\Feature\Admin\User\Profile;

class ShowTest extends _TestCase
{
    public function test_success()
    {
        $this->requestMethod = self::REQUEST_METHOD_GET;

        $this->response = $this->sendRequest();
        $this->response->assertStatus(200);
    }
}
