<?php

namespace Tests\Feature\Admin\Shop\Contact;

use Illuminate\Http\UploadedFile;

class CreateTest extends _TestCase
{
    protected string $requestMethod = self::REQUEST_METHOD_POST;

    public function test_success()
    {
        $this->requestBody = [
            'full_name' => 'Full name 3',
            'position' => 'Position 3',
            'phone' => 'Phone 3',
        ];

        $this->response = $this->sendRequest();
        $this->response->assertStatus(201);
    }
}
