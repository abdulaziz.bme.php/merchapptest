<?php

namespace Tests\Feature\Admin\Region\City;

use App\Tests\Feature\Traits\Delete\DeleteTrait;
use App\Tests\Feature\Traits\Delete\RestoreTrait;

class DeleteTest extends _TestCase
{
    use DeleteTrait;
    use RestoreTrait;
}
